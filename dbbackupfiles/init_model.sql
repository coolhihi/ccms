create table ccms_customparam(
    id int not null auto_increment,
    paramname varchar(50) not null,
    paramvalue varchar(200) not null,
    paramtitle varchar(200) not null,
    manager_id int not null,
    enable int not null,
    systemparam int not null,
    primary key(id),
    foreign key(manager_id) references ccms_manager(id)
    on update cascade on delete restrict,
    unique(paramname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_template(
    id int not null auto_increment,
    title varchar(200) not null,
    categoryfile varchar(200),
    contentfile varchar(200),
    perpage int,
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_protemplate(
    id int not null auto_increment,
    title varchar(200) not null,
    categoryfile varchar(200),
    contentfile varchar(200),
    perpage int,
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_category(
    id int not null auto_increment,
    parent_id int not null,
    categoryname varchar(200) not null,
    kind int not null,
    outsideurl varchar(200),
    joinnav int not null,
    weight int not null,
    image varchar(200),
    template_id int not null,
    introduction text,
    primary key(id),
    index(parent_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_content(
    id int not null auto_increment,
    category_id int not null,
    weight int not null,
    title varchar(200) not null,
    author varchar(200),
    origin varchar(200),
    attachment text,
    image varchar(200),
    flags varchar(200),
    allowcomment int not null,
    summary text,
    content text,
    tags varchar(200),
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    enable int not null,
    primary key(id),
    foreign key(category_id) references ccms_category(id)
    on update cascade on delete restrict,
    index(category_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_procategory(
    id int not null auto_increment,
    parent_id int not null,
    categoryname varchar(200) not null,
    weight int not null,
    image varchar(200),
    protemplate_id int not null,
    introduction text,
    primary key(id),
    index(parent_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_product(
    id int not null auto_increment,
    procategory_id int not null,
    weight int not null,
    title varchar(200) not null,
    image varchar(200),
    flags varchar(200),
    content text,
    tags varchar(200),
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    enable int not null,
    param1 text,
    param2 text,
    primary key(id),
    foreign key(procategory_id) references ccms_procategory(id)
    on update cascade on delete restrict,
    index(procategory_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_piccategory(
    id int not null auto_increment,
    categoryname varchar(200) not null,
    weight int not null,
    renderfile varchar(200),
    picturerenderfile varchar(200),
    image varchar(200),
    introduction text,
    primary key(id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_picture(
    id int not null auto_increment,
    piccategory_id int not null,
    weight int not null,
    title varchar(200) not null,
    author varchar(200),
    origin varchar(200),
    image varchar(200),
    introduction text,
    width int,
    height int,
    flags varchar(200),
    enable int not null,
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    primary key(id),
    foreign key(piccategory_id) references ccms_piccategory(id)
    on update cascade on delete cascade,
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_partner(
    id int not null auto_increment,
    title varchar(200) not null,
    image varchar(200),
    url varchar(200),
    description text,
    weight int not null,
    create_time int not null,
    primary key(id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;
