/* Demo Note:  This demo uses a FileProgress class that handles the UI for displaying the file name and percent complete.
The FileProgress class is not part of SWFUpload.
*/


/* **********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
  
function FileProgress(file, targetID) {
	this.fileProgressID = file.id;

	this.opacity = 100;
	this.height = 0;
	
	this.fileProgressWrapper = document.getElementById(this.fileProgressID);
	if (!this.fileProgressWrapper) {
		this.fileProgressWrapper = document.createElement("div");
		this.fileProgressWrapper.className = "progressWrapper";
		this.fileProgressWrapper.id = this.fileProgressID;

		this.fileProgressElement = document.createElement("div");
		this.fileProgressElement.className = "progressContainer";

		var progressCancel = document.createElement("a");
		progressCancel.className = "progressCancel";
		progressCancel.href = "#";
		progressCancel.style.visibility = "hidden";
		progressCancel.appendChild(document.createTextNode(" "));

		var progressText = document.createElement("div");
		progressText.className = "progressName";
		//progressText.appendChild(document.createTextNode(file.name));
		//progressText.appendChild(document.createTextNode(file.name));

		var progressBar = document.createElement("div");
		progressBar.className = "progressBarInProgress";

		var progressStatus = document.createElement("div");
		progressStatus.className = "progressBarStatus";
		progressStatus.innerHTML = "&nbsp;";

		this.fileProgressElement.appendChild(progressCancel);
		this.fileProgressElement.appendChild(progressText);
		this.fileProgressElement.appendChild(progressStatus);
		this.fileProgressElement.appendChild(progressBar);

		this.fileProgressWrapper.appendChild(this.fileProgressElement);

		document.getElementById(targetID).appendChild(this.fileProgressWrapper);
	} else {
		this.fileProgressElement = this.fileProgressWrapper.firstChild;
		this.reset();
	}

	this.height = this.fileProgressWrapper.offsetHeight;
	this.setTimer(null);


}

FileProgress.prototype.setTimer = function (timer) {
	this.fileProgressElement["FP_TIMER"] = timer;
};
FileProgress.prototype.getTimer = function (timer) {
	return this.fileProgressElement["FP_TIMER"] || null;
};

FileProgress.prototype.reset = function () {
	this.fileProgressElement.className = "progressContainer";

	this.fileProgressElement.childNodes[2].innerHTML = "&nbsp;";
	this.fileProgressElement.childNodes[2].className = "progressBarStatus";
	
	this.fileProgressElement.childNodes[3].className = "progressBarInProgress";
	this.fileProgressElement.childNodes[3].style.width = "0%";
	
	this.appear();	
};

FileProgress.prototype.setProgress = function (percentage) {
	this.fileProgressElement.className = "progressContainer green";
	this.fileProgressElement.childNodes[3].className = "progressBarInProgress";
	this.fileProgressElement.childNodes[3].style.width = percentage + "%";

	this.appear();	
};
FileProgress.prototype.setComplete = function () {
	this.fileProgressElement.className = "progressContainer blue";
	this.fileProgressElement.childNodes[3].className = "progressBarComplete";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 10000));
};
FileProgress.prototype.setError = function () {
	this.fileProgressElement.className = "progressContainer red";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 5000));
};
FileProgress.prototype.setCancelled = function () {
	this.fileProgressElement.className = "progressContainer";
	this.fileProgressElement.childNodes[3].className = "progressBarError";
	this.fileProgressElement.childNodes[3].style.width = "";

	var oSelf = this;
	this.setTimer(setTimeout(function () {
		oSelf.disappear();
	}, 2000));
};
FileProgress.prototype.setStatus = function (status) {
	this.fileProgressElement.childNodes[2].innerHTML = status;
};

// Show/Hide the cancel button
FileProgress.prototype.toggleCancel = function (show, swfUploadInstance) {
	this.fileProgressElement.childNodes[0].style.visibility = show ? "visible" : "hidden";
	if (swfUploadInstance) {
		var fileID = this.fileProgressID;
		this.fileProgressElement.childNodes[0].onclick = function () {
			swfUploadInstance.cancelUpload(fileID);
			return false;
		};
	}
};

FileProgress.prototype.appear = function () {
	if (this.getTimer() !== null) {
		clearTimeout(this.getTimer());
		this.setTimer(null);
	}
	
	if (this.fileProgressWrapper.filters) {
		try {
			this.fileProgressWrapper.filters.item("DXImageTransform.Microsoft.Alpha").opacity = 100;
		} catch (e) {
			// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
			this.fileProgressWrapper.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=100)";
		}
	} else {
		this.fileProgressWrapper.style.opacity = 1;
	}
		
	this.fileProgressWrapper.style.height = "";
	
	this.height = this.fileProgressWrapper.offsetHeight;
	this.opacity = 100;
	this.fileProgressWrapper.style.display = "";
	
};

// Fades out and clips away the FileProgress box.
FileProgress.prototype.disappear = function () {

	var reduceOpacityBy = 15;
	var reduceHeightBy = 4;
	var rate = 30;	// 15 fps

	if (this.opacity > 0) {
		this.opacity -= reduceOpacityBy;
		if (this.opacity < 0) {
			this.opacity = 0;
		}

		if (this.fileProgressWrapper.filters) {
			try {
				this.fileProgressWrapper.filters.item("DXImageTransform.Microsoft.Alpha").opacity = this.opacity;
			} catch (e) {
				// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
				this.fileProgressWrapper.style.filter = "progid:DXImageTransform.Microsoft.Alpha(opacity=" + this.opacity + ")";
			}
		} else {
			this.fileProgressWrapper.style.opacity = this.opacity / 100;
		}
	}

	if (this.height > 0) {
		this.height -= reduceHeightBy;
		if (this.height < 0) {
			this.height = 0;
		}

		this.fileProgressWrapper.style.height = this.height + "px";
	}

	if (this.height > 0 || this.opacity > 0) {
		var oSelf = this;
		this.setTimer(setTimeout(function () {
			oSelf.disappear();
		}, rate));
	} else {
		this.fileProgressWrapper.style.display = "none";
		this.setTimer(null);
	}
};

function preLoad() {
	if (!this.support.loading) {
		alert("You need the Flash Player 9.028 or above to use SWFUpload.");
		return false;
	}
}
function loadFailed() {
	alert("Something went wrong while loading SWFUpload. If this were a real application we'd clean up and then give you an alternative");
}

function fileQueued(file) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("即将上传...");
		progress.toggleCancel(true, this);

	} catch (ex) {
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
			return;
		}

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			progress.setStatus("File is too big.");
			this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			progress.setStatus("Cannot upload Zero Byte files.");
			this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			progress.setStatus("Invalid File Type.");
			this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		default:
			if (file !== null) {
				progress.setStatus("Unhandled Error");
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (numFilesSelected > 0) {
			//document.getElementById(this.customSettings.cancelButtonId).disabled = false;
		}
		
		/* I want auto start the upload and I can do that here */
		this.startUpload();
	} catch (ex)  {
        this.debug(ex);
	}
}

function uploadStart(file) {
	try {
		/* I don't want to do any file validation or anything,  I'll just update the UI and
		return true to indicate that the upload should start.
		It's important to update the UI here because in Linux no uploadProgress events are called. The best
		we can do is say we are uploading.
		 */
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("上传中...");
		progress.toggleCancel(true, this);
	}
	catch (ex) {}
	
	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	try {
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		//progress.setProgress(percent);
		progress.setStatus("上传中... "+percent+"%");
	} catch (ex) {
		this.debug(ex);
	}
}

//操作方式1：暂不采用
function uploadSuccess(file, serverData) {
	try {
		var progress = new FileProgress(file,  this.customSettings.progressTarget);

		if (serverData.indexOf("FILEURL:")>=0) {
                        var urls=serverData.split(":");
                        var urlArr=urls[1].split('|||');
                        var imgUrl=urlArr[0];
                        var smallImgUrl=urlArr[1];
			addImage(smallImgUrl,imgUrl,this.customSettings.imageViewField);
                        document.getElementById(this.customSettings.imageField).value=imgUrl;
                        document.getElementById(this.customSettings.imageSmallField).value=smallImgUrl;
                        progress.setComplete();
			progress.setStatus("完成.");
			progress.toggleCancel(false);
		} else {
                        progress.setComplete();
			progress.setStatus("错误.");
			progress.toggleCancel(false);
		}

	} catch (ex) {
		this.debug(ex);
	}
}


//操作方式2：上传完成插入小图预览，点击打开大图
function uploadSuccessForImage(file, serverData) {
	try {
                serverData=DWZ.jsonEval(serverData);
		var progress = new FileProgress(file,  this.customSettings.progressTarget);

		if(serverData.err==""){
			addImage(serverData.msg,this.customSettings.imageViewField);
                        document.getElementById(this.customSettings.imageField).value=serverData.msg;
                        progress.setComplete();
			progress.setStatus("完成.");
			progress.toggleCancel(false);
		} else {
                        progress.setComplete();
			progress.setStatus("错误.");
			progress.toggleCancel(false);
		}

	} catch (ex) {
		this.debug(ex);
	}
}


//操作方式3：原文件上传后的地址直接插入，不作显示，用于文件类的上传
function uploadSuccessForFile(file, serverData) {
	try {
                serverData=DWZ.jsonEval(serverData);
		var progress = new FileProgress(file,  this.customSettings.progressTarget);

		if(serverData.err==""){
                        document.getElementById(this.customSettings.fileField).value=serverData.msg;
                        progress.setComplete();
			progress.setStatus("完成.");
			progress.toggleCancel(false);
		} else {
                        progress.setComplete();
			progress.setStatus("错误.");
			progress.toggleCancel(false);
		}

	} catch (ex) {
		this.debug(ex);
	}
}


//操作方式4：原文件上传后的地址插入到附件显示，用于多个附件的上传
function uploadSuccessForAttachment(file, serverData) {
	try {
                serverData=DWZ.jsonEval(serverData);
		var progress = new FileProgress(file,  this.customSettings.progressTarget);

		if(serverData.err==""){
                        var arrs=serverData.msg.split("|&|");
                        //alert(document.getElementById(this.customSettings.attachmentField).value);
                        document.getElementById(this.customSettings.attachmentField).value+='|f|'+serverData.msg;
                        var newLi = document.createElement("li");
                        newLi.setAttribute("rel", this.customSettings.attachmentField);
                        newLi.setAttribute("alt", '|f|'+serverData.msg);
                        var newA = document.createElement("a");
                        newA.href=arrs[1];
                        newA.innerHTML=arrs[0];
                        var newSpan = document.createElement("span");
                        newSpan.className="delAttachment";
                        newSpan.innerHTML='×';
                        
                        document.getElementById(this.customSettings.viewField).appendChild(newLi);
                        newLi.appendChild(newA);
                        newLi.appendChild(newSpan);
                        $(newSpan).click(function(){
                            var pa=$(this).parent();
                            var field=$("#"+pa.attr("rel"));
                            field.val(field.val().replace(pa.attr("alt"),""));
                            pa.remove();
                        });
                        //document.getElementById(this.customSettings.viewField).innerHTML+='<li><a href="'+arrs[1]+'">'+arrs[0]+'</a><span class="delAttachment">×</span></li>';
                        progress.setComplete();
			progress.setStatus("完成.");
			progress.toggleCancel(false);
		} else {
                        progress.setComplete();
			progress.setStatus("错误.");
			progress.toggleCancel(false);
		}

	} catch (ex) {
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			progress.setStatus("Upload Error: " + message);
			this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			progress.setStatus("Upload Failed.");
			this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			progress.setStatus("Server (IO) Error");
			this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			progress.setStatus("Security Error");
			this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			progress.setStatus("Upload limit exceeded.");
			this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			progress.setStatus("Failed Validation.  Upload skipped.");
			this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			// If there aren't any files left (they were all cancelled) disable the cancel button
			if (this.getStats().files_queued === 0) {
				//document.getElementById(this.customSettings.cancelButtonId).disabled = true;
			}
			progress.setStatus("取消");
			progress.setCancelled();
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			progress.setStatus("终止");
			break;
		default:
			progress.setStatus("Unhandled Error: " + errorCode);
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function uploadComplete(file) {
	if (this.getStats().files_queued === 0) {
		//document.getElementById(this.customSettings.cancelButtonId).disabled = true;
	}
}

// This event comes from the Queue Plugin
function queueComplete(numFilesUploaded) {
	//var status = document.getElementById("divStatus");
	//status.innerHTML = numFilesUploaded + " file" + (numFilesUploaded === 1 ? "" : "s") + " uploaded.";
}

//显示上传的图片生成的小图，并插入放大链接动作
function addImage(src,viewFieldId) {
        var tempcontainer=document.getElementById(viewFieldId);
	var newDd = document.createElement("dd");
	var newA = document.createElement("a");
	var newImg = document.createElement("img");
	newA.target = "dialog";
        newA.title = "查看原图";
	newA.href = "/admin.php/site/viewimg/imgsrc/"+src.replace(/\//g,"|xc1|");
	newImg.style.margin = "5px";
	newImg.border = "0";
        
        tempcontainer.innerHTML='<dt><label>图片预览</label></dt>';
	tempcontainer.appendChild(newDd);
	newDd.appendChild(newA);
	newA.appendChild(newImg);
        
	if (newImg.filters) {
		try {
			newImg.filters.item("DXImageTransform.Microsoft.Alpha").opacity = 0;
		} catch (e) {
			// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
			newImg.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + 0 + ')';
		}
	} else {
		newImg.style.opacity = 0;
	}

	newImg.onload = function () {
		fadeIn(newImg, 0);
	};
        var tempn=src.lastIndexOf(".");
        src=src.substr(0,tempn)+"_s"+src.substr(tempn);
	newImg.src = src;
        
        //配合DWZ
        //图片点击时通过DWZ-dialog方式打开预览
        $(newA).click(function(event){
            var $this=$(this);
            var title=$this.attr("title")||$this.text();
            var rel=$this.attr("rel")||"_blank";
            var options={};
            var w=$this.attr("width");
            var h=$this.attr("height");
            if(w)options.width=w;
            if(h)options.height=h;
            options.max=eval($this.attr("max")||"false");
            options.mask=eval($this.attr("mask")||"false");
            options.maxable=eval($this.attr("maxable")||"true");
            options.minable=eval($this.attr("minable")||"true");
            options.fresh=eval($this.attr("fresh")||"true");
            options.resizable=eval($this.attr("resizable")||"true");
            options.drawable=eval($this.attr("drawable")||"true");
            options.close=eval($this.attr("close")||"");
            options.param=$this.attr("param")||"";
            var url=unescape($this.attr("href")).replaceTmById($(event.target).parents(".unitBox:first"));
            DWZ.debug(url);
            if(!url.isFinishedTm()){
            alertMsg.error($this.attr("warn")||DWZ.msg("alertSelectMsg"));
            return false;}
            $.pdialog.open(url,rel,title,options);
            return false;});
        }

//显示上传的原图
function addSourceImage(src,viewFieldId) {
        var tempcontainer=document.getElementById(viewFieldId);
	var newDd = document.createElement("dd");
	var newImg = document.createElement("img");
	newImg.style.margin = "5px";
        
        tempcontainer.innerHTML='<dt><label>图片预览</label></dt>';
	tempcontainer.appendChild(newDd);
	newDd.appendChild(newImg);
	if (newImg.filters) {
		try {
			newImg.filters.item("DXImageTransform.Microsoft.Alpha").opacity = 0;
		} catch (e) {
			// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
			newImg.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + 0 + ')';
		}
	} else {
		newImg.style.opacity = 0;
	}

	newImg.onload = function () {
		fadeIn(newImg, 0);
	};
	newImg.src = src;
}

function fadeIn(element, opacity) {
	var reduceOpacityBy = 5;
	var rate = 30;	// 15 fps


	if (opacity < 100) {
		opacity += reduceOpacityBy;
		if (opacity > 100) {
			opacity = 100;
		}

		if (element.filters) {
			try {
				element.filters.item("DXImageTransform.Microsoft.Alpha").opacity = opacity;
			} catch (e) {
				// If it is not set initially, the browser will throw an error.  This will set it if it is not set yet.
				element.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=' + opacity + ')';
			}
		} else {
			element.style.opacity = opacity / 100;
		}
	}

	if (opacity < 100) {
		setTimeout(function () {
			fadeIn(element, opacity);
		}, rate);
	}
}