<?php
    /*
     * CCMS 的公用函数库
     */
class CFunc
{
    /*
     * writeLog 写日志函数
     * param $messages 是写入日志的内容
     */
    public static function writeLog($message)
    {
	$model=new Managelog;
	$model->content=$message;
        $model->manager_id=(int)(Yii::app()->user->id);
        $model->create_time=time();
        $model->setIsNewRecord(true);
        //var_dump($model);
        //Yii::app()->end();
	if($model->save()){
		return true;
	}
        return false;
    }
    
    /*
     * flushCache
     * param $backjson 决定是否返回DWZ的成功JSON
     */
    public static function flushCache($backjson=false)
    {
            try{
                $flushUrl=Yii::app()->request->hostInfo.'/index.php/site/flushcache';
                $result=file_get_contents($flushUrl);
                if($result!="true") throw new Exception("Flushcache Failed!");
                Yii::app()->cache->flush();
                if($backjson) echo '{"statusCode":"200", "message":"页面缓存已清理！", "callbackType":""}';
                return true;
            } catch (Exception $exc) {
                if($backjson) echo '{"statusCode":"300", "message":"页面缓存清理失败，请与管理员联系", "callbackType":""}';
                return false;
            }
    }
    
    /*
     * checkRight 检验权限
     * param $xp 指定要检验的是权限字符串中的第几位（从1开始）
     * 为方便修改权限对应位，请在本文件夹下的CDefine.php中定义相应的常量
     */
    public static function checkRight($xp,$rightStr=null){
        if(is_null($rightStr))
            $manageRight=isset(Yii::app()->user->manageright)?Yii::app()->user->manageright:'0';
        else
            $manageRight=$rightStr;
        //Yii::trace($xp,'cool.all right');
        if($xp>strlen($manageRight))
            return false;
        else
            return substr($manageRight,$xp-1,1)==1;
    }
    
    /*
     * jsAlert 用js弹出dialog
     */
    public static function jsAlert($message){
        echo "<script language='javascript'>alert('$message')</script>";
    }
    
    /*
     * jsAddToggleBtn 用Jquery完成toggle按钮
     * $value为null即未设置时，监听事件click，点一下显示，点一下隐藏；
     * $value不为null即被设置时，监听事件为change，当$btnId的值与$value相等时显示，否则隐藏
     */
    public static function jsAddToggleBtn($btnId,$toggleDivId,$value=null){
        if(is_null($value)){
            $event='click';
            $tempCode='$("#'.$toggleDivId.'").toggle();';
        }
        else{
            $event='change';
            $tempCode='if($(this).val()=="'.$value.'") $("#'.$toggleDivId.'").show();else $("#'.$toggleDivId.'").hide();';
        };
        $cs = Yii::app()->getClientScript();  
        $cs->registerScript(
          'c_jsAddToggleBtn',
          '$("#'.$btnId.'").live("'.$event.'", function(){'.$tempCode.'});',
          CClientScript::POS_END
        );
        //$tempJs="<script language='javascript'>";
        //$tempJs
        //$tempJs.="</script>";
    }
    
    /*
     * getSmallImg 返回相应小图的地址，与swfupload中上传双图相互相承
     */
    public static function getSmallImg($imgUrl,$allowExist=false){
        $pt=strrpos($imgUrl, '.');//返回.从右边起首先出现的位置，即分隔扩展名
        $smallImgTemp=substr($imgUrl, 0, $pt) . '_s' . substr($imgUrl, $pt);
        if($allowExist) return $smallImgTemp;
        try{
            if(file_exists(substr($smallImgTemp,1)))
                return $smallImgTemp;
            else
                return '/images/nopic.gif';
        } catch (Exception $exc) {
            //抓取错误，不提示
            return '/images/nopic.gif';
        }
    }
    
    /*
     * displayRight 显示为权限
     * param $manageRight 要转换为权限显示的字符串
     */
    public static function displayRight($manageRight=null){
        if(is_null($manageRight))
            $manageRight=Yii::app()->user->manageright;
        $str='<ul>';
        $str.=(self::checkRight(CR_CHANGEPASSWORD, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='修改密码';
        $str.='</li>';
        $str.=(self::checkRight(CR_CONTENT, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='资讯管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_CATEGORY, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='栏目管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_TEMPLATE, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='模板管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PICTURE, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='幻灯图片管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PICCATEGORY, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='幻灯图库管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PRODUCT, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='产品管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PROCATEGORY, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='分类管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PROTEMPLATE, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='模板管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_PARTNER, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='合作伙伴管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_STATISTICS, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='流量监控';
        $str.='</li>';
        $str.=(self::checkRight(CR_MANAGER, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='管理员管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_MANAGELOG, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='管理日志管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_CUSTOMPARAM, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='参数管理';
        $str.='</li>';
        $str.=(self::checkRight(CR_DBBACKUP, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='数据库备份/还原';
        $str.='</li>';
        $str.=(self::checkRight(CR_TAG, $manageRight))?'<li class="rightYes">':'<li class="rightNo">';
        $str.='标签管理';
        $str.='</li>';
        $str.='</ul>';
        return $str;
    }
    
    /*
     * mergeRight 把数组提交的管理权限合并成字符串
     * 与separateRight起反作用
     * param $managerightArray 是由数字组成的数组，有数字指向的位为1，其他为0
     */
    public static function mergeRight($managerightArray){
        if(!is_array($managerightArray)) return '';
        //为提高效率，不采用循环，默认取50位权限
        $str='00000000000000000000000000000000000000000000000000';
        foreach ($managerightArray as $rightBit){
            $str{$rightBit-1}=1;
        }
        return $str;
    }
    
    /*
     * separateRight 把字符串的管理权限转化为数组
     * 与mergeRight起反作用
     * param $managerightStr 是权限字符串，要转为由=1的位组成的数组，如100110变成array(1,4,5)
     */
    public static function separateRight($managerightStr){
        $arr=array();
        for($i=0;$i<strlen($managerightStr);$i++){
            if($managerightStr{$i}==1) $arr[]=$i+1;
        }
        return $arr;
    }
    /*
     * getCustomparam 获取自定义参数
     */
    public static function getCustomparam($paramname){
        $tempResult=Customparam::model()->find('paramname=:paramname', array(':paramname'=>$paramname));
        if(count($tempResult)>0)
            return $tempResult->paramvalue;
        else
            return '';
    }
    
    /*
     * getSonTree 找子栏目并定义级别和排序
     * param $pid 是初始parent_id
     * param $generation 是初始的级别，向下自动加1
     * 返回的是初始栏目下所有子栏目的数组，个项的两个元素是Category对象和所在层次数，array('category','generation')
     */
    public static function getSonTree($pid=0,$generation=0){
        $generation=$generation+1;
        $categoryTree=array();
        $mySons=Category::model()->findAll(array('condition'=>'parent_id='.$pid,'order'=>'weight desc,id asc'));
        if(count($mySons)>0){
            foreach($mySons as $mySon){
                array_push($categoryTree,array('category'=>$mySon,'generation'=>$generation));
                $newTree=self::getSonTree($mySon->id,$generation);
                foreach($newTree as $newTreeItem){
                    array_push($categoryTree,array('category'=>$newTreeItem['category'],'generation'=>$newTreeItem['generation']));
                }
            }
        }
        return $categoryTree;
    }
    
    /*
     * getProSonTree 找子栏目并定义级别和排序
     * param $pid 是初始parent_id
     * param $generation 是初始的级别，向下自动加1
     * 返回的是初始栏目下所有子栏目的数组，个项的两个元素是Category对象和所在层次数，array('category','generation')
     */
    public static function getProSonTree($pid=0,$generation=0){
        $generation=$generation+1;
        $categoryTree=array();
        $mySons=Procategory::model()->findAll(array('condition'=>'parent_id='.$pid,'order'=>'weight desc,id asc'));
        if(count($mySons)>0){
            foreach($mySons as $mySon){
                array_push($categoryTree,array('category'=>$mySon,'generation'=>$generation));
                $newTree=self::getProSonTree($mySon->id,$generation);
                foreach($newTree as $newTreeItem){
                    array_push($categoryTree,array('category'=>$newTreeItem['category'],'generation'=>$newTreeItem['generation']));
                }
            }
        }
        return $categoryTree;
    }
    
    /*
     * repeatStr 重复输出字符串
     * param $repeatOne 用于复制的原始字符串
     * param $times 重复次数
     * 返回的是重复后的字符串
     */
    public static function repeatStr($repeatOne,$times){
        $tempStr='';
        for($i=0;$i<$times;$i++){
            $tempStr.=$repeatOne;
        }
        return $tempStr;
    }
    
    
    /*
     * getBrowse 获取当前用户的浏览器类型
     */
     public static function getBrowse(){
         global $_SERVER;
         $Agent = $_SERVER['HTTP_USER_AGENT']; 
         $browser = '';
         $browserver = '';
         $Browser = array('Lynx', 'MOSAIC', 'AOL', 'Opera', 'JAVA', 'MacWeb', 'WebExplorer', 'OmniWeb'); 
         for($i = 0; $i <= 7; $i ++){
             if(strpos($Agent, $Browser[$i])){
                 $browser = $Browser[$i]; 
                 $browserver = '';
             }
         }
         if(preg_match('/Mozilla/', $Agent) && !preg_match('/MSIE/', $Agent)){
             $temp = explode('(', $Agent); 
             $Part = $temp[0];
             $temp = explode('/', $Part);
             $browserver = $temp[1];
             $temp = explode(' ', $browserver); 
             $browserver = $temp[0];
             $browserver = preg_replace('/([d.]+)/', '1', $browserver);
             $browserver = $browserver;
             $browser = 'Netscape Navigator'; 
         }
         if(preg_match('/Mozilla/', $Agent) && preg_match('/Opera/', $Agent)) {
             $temp = explode('(', $Agent);
             $Part = $temp[1]; 
             $temp = explode(')', $Part);
             $browserver = $temp[1];
             $temp = explode(' ', $browserver); 
             $browserver = $temp[2];
             $browserver = preg_replace('/([d.]+)/', '1', $browserver);
             $browserver = $browserver;
             $browser = 'Opera'; 
         }
         if(preg_match('/Mozilla/', $Agent) && preg_match('/MSIE/', $Agent)){
             $temp = explode('(', $Agent);
             $Part = $temp[1]; 
             $temp = explode(';', $Part);
             $Part = $temp[1];
             $temp = explode(' ', $Part);
             $browserver = $temp[2]; 
             $browserver = preg_replace('/([d.]+)/','1',$browserver);
             $browserver = $browserver;
             $browser = 'Internet Explorer';
         }
         if($browser != ''){ 
             $browseinfo = $browser.' '.$browserver;
         } else {
             $browseinfo = false;
         }
         return $browseinfo;
         }


    /*
     * getIP 获取当前用户的IP地址
     */
     public static function getIP(){
         global $_SERVER;
         if (getenv('HTTP_CLIENT_IP')) {
             $ip = getenv('HTTP_CLIENT_IP');
         }
         else if (getenv('HTTP_X_FORWARDED_FOR')) {
             $ip = getenv('HTTP_X_FORWARDED_FOR'); 
         }
         else if (getenv('REMOTE_ADDR')) {
             $ip = getenv('REMOTE_ADDR');
         }
         else {
             $ip = $_SERVER['REMOTE_ADDR'];
         }
         return $ip; 
     }


    /*
     * getOS 获取当前用户的操作系统类型
     */
     public static function getOS ()
     {
         global $_SERVER;
         $agent = $_SERVER['HTTP_USER_AGENT'];
         $os = false;
         if (preg_match('/win/i', $agent) && strpos($agent, '95')){ 
             $os = 'Windows 95';
         }
         else if (preg_match('/win 9x/i', $agent) && strpos($agent, '4.90')){
             $os = 'Windows ME'; 
         }
         else if (preg_match('/win/i', $agent) && preg_match('/98/', $agent)){
             $os = 'Windows 98';
         }
         else if (preg_match('/win/i', $agent) && preg_match('/nt 5.1/i', $agent)){ 
             $os = 'Windows XP';
         }
         else if (preg_match('/win/i', $agent) && preg_match('/nt 5/i', $agent)){
             $os = 'Windows 2000';
         } 
         else if (preg_match('/win/i', $agent) && preg_match('/nt/i', $agent)){
             $os = 'Windows NT';
         }
         else if (preg_match('/win/i', $agent) && preg_match('/32/', $agent)){ 
             $os = 'Windows 32';
         }
         else if (preg_match('/linux/i', $agent)){
             $os = 'Linux';
         }
         else if (preg_match('/unix/i', $agent)){ 
             $os = 'Unix';
         }
         else if (preg_match('/sun/i', $agent) && preg_match('/os/i', $agent)){
             $os = 'SunOS';
         } 
         else if (preg_match('/ibm/i', $agent) && preg_match('/os/i', $agent)){
             $os = 'IBM OS/2';
         }
         else if (preg_match('/Mac/i', $agent) && preg_match('/PC/i', $agent)){ 
             $os = 'Macintosh';
         }
         else if (preg_match('/PowerPC/i', $agent)){
             $os = 'PowerPC';
         }
         else if (preg_match('/AIX/i', $agent)){ 
             $os = 'AIX';
         }
         else if (preg_match('/HPUX/i', $agent)){
             $os = 'HPUX';
         }
         else if (preg_match('/NetBSD/i', $agent)){ 
             $os = 'NetBSD';
         }
         else if (preg_match('/BSD/i', $agent)){
             $os = 'BSD';
         }
         else if (preg_match('/OSF1/', $agent)){ 
             $os = 'OSF1';
         }
         else if (preg_match('/IRIX/', $agent)){
             $os = 'IRIX';
         }
         else if (preg_match('/FreeBSD/i', $agent)){ 
             $os = 'FreeBSD';
         }
         else if (preg_match('/teleport/i', $agent)){
             $os = 'teleport';
         }
         else if (preg_match('/flashget/i', $agent)){ 
             $os = 'flashget';
         }
         else if (preg_match('/webzip/i', $agent)){
             $os = 'webzip';
         }
         else if (preg_match('/offline/i', $agent)){ 
             $os = 'offline';
         }
         else {
             $os = 'Unknown';
         }
         return $os;
     }
     
    /*
     * writeStat 写入流量统计
     */
    public static function writeStat()
    {
        //先判断该流量要不要记录
        $tempUrl=$_SERVER['REQUEST_URI'];
        $tempUrlArr=explode('/',$tempUrl);
        //var_dump($tempUrlArr);
        //Yii::app()->end();
        if(count($tempUrlArr)==2 || !in_array($tempUrlArr[2],array('error','login','logout'))){
            try{
                $model=new Statistics;
                $model->ipaddress=$_SERVER['REMOTE_ADDR'];
                $model->url=$tempUrl;
                //$model->fromurl=$_SERVER['HTTP_REFERER'];
                if (isset($_SERVER['HTTP_REFERER'])) { 
                    //HTTP_REFERER found 
                    $model->fromurl=$_SERVER['HTTP_REFERER'];
                }else{ 
                    //HTTP_REFERER not found 
                }

                //若UV检验cookie不存在，则为独立访客，并创建cookie
                $cookie=Yii::app()->request->getCookies();
                if(is_null($cookie['ccms_stat'])){
                    $cookie=new CHttpCookie('ccms_stat','hello');
                    $cookie->expire = time()+60*60*12;
                    Yii::app()->request->cookies['ccms_stat']=$cookie;
                    $model->isuv=1;
                }
                else
                    $model->isuv=0;
                $model->visit_time=time();
                $model->setIsNewRecord(true);
                //var_dump($model);
                //Yii::app()->end();
                if($model->save()){
                        return true;
                }
            }
            catch (Exception $exc) {
                //抓取错误，不提示
                return false;
            }
        }
    }
    
    /*
     * getAllSonCategory 返回栏目下所有子栏目ID
     * $parentCategoryId 为父栏目ID
     * $tempArray 为递归用到的存储数组
     */
    public static function getAllSonCategory($parentCategoryId, &$tempArray){
        $sonArray=Category::model()->findAll(array('condition'=>'parent_id='.$parentCategoryId));
        foreach($sonArray as $sonItem){
            array_push($tempArray, $sonItem->id);
            CFunc::getAllSonCategory($sonItem->id,$tempArray);
        }
    }
    public static function getAllSonProcategory($parentCategoryId, &$tempArray){
        $sonArray=Procategory::model()->findAll(array('condition'=>'parent_id='.$parentCategoryId));
        foreach($sonArray as $sonItem){
            array_push($tempArray, $sonItem->id);
            CFunc::getAllSonProcategory($sonItem->id,$tempArray);
        }
    }


    /*
     * cutstr 截取字符串,通过strip_tags消除格式,防止符号采用编码影响字数
     * $strToCut 是要进行截取的字符串
     * $cutCount 是截取字符数,不足则不截取
     * $codeType 是编辑,默认为utf8
     * $cutReplace 截取完成后取代被截取的字符串,默认为...
     */
    public static function cutstr($strToCut,$cutCount,$codeType='utf8',$cutReplace='...'){
        $tempStr=trim(strip_tags($strToCut));
        $tempStr=str_replace('&ldquo;','“',$tempStr);
        $tempStr=str_replace('&rdquo;','”',$tempStr);
        $tempStr=str_replace('&lsquo;','‘',$tempStr);
        $tempStr=str_replace('&rsquo;','’',$tempStr);
        $tempStr=str_replace('&quot;','"',$tempStr);
        $tempStr=str_replace('&#39;','\'',$tempStr);
        $tempStr=str_replace('&nbsp;',' ',$tempStr);
        $tempStr=str_replace('&mdash;','—',$tempStr);
        $tempStr=str_replace('&middot;','·',$tempStr);
        $tempStr=str_replace('&hellip;','…',$tempStr);
        $tempStr=str_replace('&lt;','<',$tempStr);
        $tempStr=str_replace('&gt;','>',$tempStr);
        if(mb_strlen($tempStr,$codeType)>$cutCount)
            $tempStr=mb_substr($tempStr,0,$cutCount,$codeType).$cutReplace;
        $tempStr=str_replace('“','&ldquo;',$tempStr);
        $tempStr=str_replace('”','&rdquo;',$tempStr);
        $tempStr=str_replace('‘','&lsquo;',$tempStr);
        $tempStr=str_replace('’','&rsquo;',$tempStr);
        $tempStr=str_replace('"','&quot;',$tempStr);
        $tempStr=str_replace('\'','&#39;',$tempStr);
        $tempStr=str_replace(' ','&nbsp;',$tempStr);
        $tempStr=str_replace('—','&mdash;',$tempStr);
        $tempStr=str_replace('·','&middot;',$tempStr);
        $tempStr=str_replace('…','&hellip;',$tempStr);
        $tempStr=str_replace('<','&lt;',$tempStr);
        $tempStr=str_replace('>','&gt;',$tempStr);
        return $tempStr;
    }
    
    /*
     * srcUrlEncode 把地址变量进行编码，防止Yii地址美化下变量被误读
     */
    public static function srcUrlEncode($srcUrl){
        $tempStr=$srcUrl;
        $tempStr=str_replace('/','|xc1|',$tempStr);
        return $tempStr;
    }
    
    /*
     * srcUrlDecode 把地址变量进行编码，防止Yii地址美化下变量被误读
     */
    public static function srcUrlDecode($srcUrl){
        $tempStr=$srcUrl;
        $tempStr=str_replace('|xc1|','/',$tempStr);
        return $tempStr;
    }
}
?>
