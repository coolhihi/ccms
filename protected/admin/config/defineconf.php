<?php
/*
 * 本文件用于存放所有的自定义常量
 */

/*
 * 上传路径设置
 */
define('UPLOADLINK_PATH', 'uploadfiles/link');//附件的上传路径，注意：前后都不需要斜杠/
define('UPLOADIMAGE_PATH', 'uploadfiles/image');//图片的上传路径，注意：前后都不需要斜杠/
define('UPLOADFLASH_PATH', 'uploadfiles/flash');//FLASH的上传路径，注意：前后都不需要斜杠/
define('UPLOADMEDIA_PATH', 'uploadfiles/media');//媒体的上传路径，注意：前后都不需要斜杠/
define('UPLOADATTACHMENT_PATH', 'uploadfiles/attachment');//附件的上传路径，注意：前后都不需要斜杠/
define('DBBACKUP_PATH', 'dbbackupfiles');//数据库备份路径，注意：前后都不需要斜杠/
define('DBBACKUP_TABLES', 'ccms_customparam,ccms_template,ccms_protemplate,ccms_category,ccms_content,ccms_piccategory,ccms_picture,ccms_procategory,ccms_product,ccms_partner');//备份用的表格顺序，还原会自动倒序，为适应innoDB而定

/*
 * 自动生成预览缩略图的大小
 */
define('SMALLPIC_SIZE_WIDTH', 200);
define('SMALLPIC_SIZE_HEIGHT', 140);

/*
 * 权限常量在权限字符串中的位置
 */
define('CR_CHANGEPASSWORD',1);//修改自己密码
define('CR_CONTENT',2);//资讯管理
define('CR_CATEGORY',3);//栏目管理
define('CR_TEMPLATE',4);//资讯模板管理
define('CR_PICTURE',5);//图片管理
define('CR_PICCATEGORY',6);//图库管理
define('CR_PRODUCT',7);//产品管理
define('CR_PROCATEGORY',8);//产品分类管理
define('CR_PROTEMPLATE',9);//产品模板管理
define('CR_PARTNER',10);//合作伙伴管理
define('CR_STATISTICS',11);//流量监控
define('CR_MANAGER',12);//管理员管理
define('CR_MANAGELOG',13);//管理日志管理
define('CR_CUSTOMPARAM',14);//系统参数管理
define('CR_DBBACKUP',15);//数据备份管理
define('CR_TAG',16);//标签管理（关键词）
?>
