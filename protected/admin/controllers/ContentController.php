<?php

class ContentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Content;
                $model->weight=50;
                $model->category_id=0;
                $model->create_time_str=date('Y-m-d');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                if(isset($_GET['Content']))
                    $model->category_id=$_GET['Content']['category_id'];

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
                        $model->create_time=strtotime($model->create_time_str);
                        if(is_array($model->flagsArr)>0)
                            $model->flags=implode(',', $model->flagsArr);
                        if($model->summary==''){
                            //描述留空，则自动截取文章内容
                            $tempN=CFunc::getCustomparam('system_autosummary_count');
                            $tempN=(int)$tempN;
                            if($tempN>0){
                                $model->summary=CFunc::cutstr($model->content,$tempN);
                            }
                        }
                        
                        //读取标签，采用缓存
                        $cache=Yii::app()->cache;
                        $tags=$cache['coolcache_tags'];
                        if($tags===false){
                            //若标签库没有缓存，则建立缓存
                            $tags=array();
                            $tagArr=Tag::model()->findAll();
                            foreach($tagArr as $tagItem){
                                $tags[]=$tagItem->tagname;
                            }
                            $cache['coolcache_tags']=$tags;
                        }
                        if($model->tags==''){
                            //查找所有标签，若存在则自动设为此处标签
                            $tempTitle=strtolower($model->title);
                            foreach($tags as $tag){
                                if(strpos($tempTitle,$tag)!==false)
                                    $model->tags.=','.$tag;
                            }
                            if($model->tags!=''){
                                $model->tags=substr($model->tags,1);
                            }
                        }
                        else{
                            $model->tags=strtolower($model->tags);
                            $model->tags=str_replace('，',' ',$model->tags);
                            $model->tags=str_replace('　',' ',$model->tags);
                            $model->tags=trim($model->tags);
                            $model->tags=str_replace(' ',',',$model->tags);
                            $newtags=array_filter(explode(',', $model->tags));
                            foreach($newtags as $newtag){
                                if(!in_array($newtag,$tags)){
                                    //若标签不在库中，则添加到库中
                                    $itag=new Tag;
                                    $itag->tagname=$newtag;
                                    $itag->save();
                                    $cache->delete('coolcache_tags');
                                }
                            }
                        }
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"资讯已添加",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"content",
                                    "jumpUrl":"'.$this->createUrl('content/index').'",
                                    "jumpTitle":"资讯列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"资讯发布不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $model->flagsArr=explode(',', $model->flags);
                $model->create_time_str=date('Y-m-d',$model->create_time);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Content']))
		{
			$model->attributes=$_POST['Content'];
                        $model->create_time=strtotime($model->create_time_str);
                        if(is_array($model->flagsArr))
                            $model->flags=implode(',', $model->flagsArr);
                        else if($_POST['Content']['flagsArr']=="")
                                $model->flags="";
                        if($model->summary==''){
                            //描述留空，则自动截取文章内容
                            $tempN=CFunc::getCustomparam('system_autosummary_count');
                            $tempN=(int)$tempN;
                            if($tempN>0){
                                $model->summary=CFunc::cutstr($model->content,$tempN);
                            }
                        }
                        
                        //读取标签，采用缓存
                        $cache=Yii::app()->cache;
                        $tags=$cache['coolcache_tags'];
                        if($tags===false){
                            //若标签库没有缓存，则建立缓存
                            $tags=array();
                            $tagArr=Tag::model()->findAll();
                            foreach($tagArr as $tagItem){
                                $tags[]=$tagItem->tagname;
                            }
                            $cache['coolcache_tags']=$tags;
                        }
                        if($model->tags==''){
                            //查找所有标签，若存在则自动设为此处标签
                            $tempTitle=strtolower($model->title);
                            foreach($tags as $tag){
                                if(strpos($tempTitle,$tag)!==false)
                                    $model->tags.=','.$tag;
                            }
                            if($model->tags!=''){
                                $model->tags=substr($model->tags,1);
                            }
                        }
                        else{
                            $model->tags=strtolower($model->tags);
                            $model->tags=str_replace('，',' ',$model->tags);
                            $model->tags=str_replace('　',' ',$model->tags);
                            $model->tags=trim($model->tags);
                            $model->tags=str_replace(' ',',',$model->tags);
                            $newtags=array_filter(explode(',', $model->tags));
                            foreach($newtags as $newtag){
                                if(!in_array($newtag,$tags)){
                                    //若标签不在库中，则添加到库中
                                    $itag=new Tag;
                                    $itag->tagname=$newtag;
                                    $itag->save();
                                    $cache->delete('coolcache_tags');
                                }
                            }
                        }
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"资讯已修改",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"content",
                                    "jumpUrl":"'.$this->createUrl('content/index').'",
                                    "jumpTitle":"资讯列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"资讯修改不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

                        //删除成功
                        echo '{
                                "statusCode":"200",
                                "message":"删除成功",
                                "navTabId":"content",
                                "rel":"",
                                "callbackType":"",
                                "forwardUrl":"",
                                "confirmMsg":""
                        }';
                        Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isset($_GET['numPerPage'])) {
			Yii::app()->user->setState('numPerPage',(int)$_GET['numPerPage']);
			unset($_GET['numPerPage']);
		}  else {
			Yii::app()->user->setState('numPerPage',20);
                }
		if (isset($_GET['pageNum'])) {
			Yii::app()->user->setState('pageNum',(int)$_GET['pageNum']);
			unset($_GET['pageNum']);
		}  else {
			Yii::app()->user->setState('pageNum',0);
                }
		$model=new Content('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Content']))
			$model->attributes=$_GET['Content'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Content::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='content-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
