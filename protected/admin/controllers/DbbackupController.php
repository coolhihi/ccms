<?php

class DbbackupController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_DBBACKUP)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('backup'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_DBBACKUP)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('recover'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_DBBACKUP)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_DBBACKUP)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Backup Database.
	 */
	public function actionBackup()
	{
                if(isset($_POST['Surebackup'])){
                    
                    //调用备份扩展
                    Yii::import('ext.DatabaseBackup');
                    
                    //生成随机文件名，以防穷举下载
                    $filename='';
                    $temp_p='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    for($i=0;$i<12;$i++){
                        $temp_s=rand(0,61);
                        $filename.=$temp_p{$temp_s};
                    }
                    
                    //备份函数
                    DatabaseBackup::backupDb(DBBACKUP_PATH.'/'.$filename.'.sql',DBBACKUP_TABLES);
                    
                    //生成记录
                    $model=new Dbbackup();
                    $model->filename=$filename;
                    $model->backupremark=$_POST['Surebackup']['backupremark'];
                    CFunc::writeLog('备份了数据库');
                    if($model->save()){
                        //ajax提交的，保存成功
                        echo '{
                                    "statusCode":"200",
                                    "message":"备份成功",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"dbbackup",
                                    "jumpUrl":"'.$this->createUrl('dbbackup/index').'",
                                    "jumpTitle":"备份记录",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                        }';
                        Yii::app()->end();
                    }
                    else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"备份不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                    }
                }
		$this->render('backup');
	}

	/**
         * Recover Database.
	 */
	public function actionRecover()
	{
                if(isset($_POST['Recoverdb'])){
                    if($_POST['Recoverdb']['sure']=='1'){
                        //读取备份文件名
                        $model=Dbbackup::model()->findByPk($_POST['Recoverdb']['id']);
                        $filename=$model->filename;
                        
                        //调用备份扩展
                        Yii::import('ext.DatabaseBackup');
                        
                        //还原数据库
                        if(DatabaseBackup::recoverDb(DBBACKUP_PATH.'/'.$filename.'.sql',DBBACKUP_TABLES)){
                            CFunc::writeLog('还原了数据库 {版本：'.date('Y-m-d',$model->backuptime).'}');
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"还原成功！",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeAll",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else
                            $tempFlash="请与管理员联系。";
                    }
                    //ajax提交的，保存不成功
                    $tempFlash="请对还原操作进行确认。";
                    echo '{
                            "statusCode":"300",
                            "message":"还原不成功。'.$tempFlash.'",
                            "navTabId":"",
                            "rel":"",
                            "callbackType":"",
                            "forwardUrl":"",
                            "confirmMsg":""
                    }';
                    Yii::app()->end();
                }
		$this->render('recover');
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser

                        //删除成功
                        echo '{
                                "statusCode":"200",
                                "message":"删除成功",
                                "navTabId":"dbbackup",
                                "rel":"",
                                "callbackType":"",
                                "forwardUrl":"",
                                "confirmMsg":""
                        }';
                        Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isset($_GET['numPerPage'])) {
			Yii::app()->user->setState('numPerPage',(int)$_GET['numPerPage']);
			unset($_GET['numPerPage']);
		}  else {
			Yii::app()->user->setState('numPerPage',20);
                }
		if (isset($_GET['pageNum'])) {
			Yii::app()->user->setState('pageNum',(int)$_GET['pageNum']);
			unset($_GET['pageNum']);
		}  else {
			Yii::app()->user->setState('pageNum',0);
                }
		$model=new Dbbackup('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Dbbackup']))
			$model->attributes=$_GET['Dbbackup'];
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Dbbackup::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manager-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
