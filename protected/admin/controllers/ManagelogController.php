<?php

class ManagelogController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGELOG)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('empty'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGELOG)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Delete all models.
	 */
	public function actionEmpty()
	{
		if(isset($_POST['Logtodel'])){
                        $deleteAtt=array('manager_id'=>$_POST['Logtodel']['manager_id']);
                        if($deleteAtt['manager_id']==0)
                            Managelog::model()->deleteAll();//如果是0，表示删除全部管理员的日志
                        else
                            Managelog::model()->deleteAllByAttributes($deleteAtt);//否则，只删除所选择的管理员的日志

                        //删除成功
                        echo '{
                            "statusCode":"200",
                            "message":"日志已清空。",
                            "navTabId":"",
                            "rel":"",
                            "callbackType":"",
                            "forwardUrl":"",
                            "confirmMsg":""
                        }';
                        Yii::app()->end();
                }
                $this->render ('empty');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isset($_GET['numPerPage'])) {
			Yii::app()->user->setState('numPerPage',(int)$_GET['numPerPage']);
			unset($_GET['numPerPage']);
		}  else {
			Yii::app()->user->setState('numPerPage',20);
                }
		if (isset($_GET['pageNum'])) {
			Yii::app()->user->setState('pageNum',(int)$_GET['pageNum']);
			unset($_GET['pageNum']);
		}  else {
			Yii::app()->user->setState('pageNum',0);
                }
		$model=new Managelog('search');
                $model->setScenario('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Managelog']))
			$model->attributes=$_GET['Managelog'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Managelog::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='managelog-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
