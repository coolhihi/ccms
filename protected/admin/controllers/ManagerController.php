<?php

class ManagerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('changepswd'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CHANGEPASSWORD)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGER)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGER)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGER)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_MANAGER)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Manager;
                $model->scenario = 'createmanager';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manager']))
		{
                        $model->attributes=$_POST['Manager'];
                        if($model->validate()){
                            $model->password=$model->password2=$model->hashPassword($model->password);
                            $model->manageright=CFunc::mergeRight($model->managerightArray);
                            if($model->save()){
                                //ajax提交的，保存成功
                                echo '{
                                        "statusCode":"200",
                                        "message":"管理员已添加",
                                        "navTabId":"",
                                        "rel":"",
                                        "callbackType":"closeCurrentAndJump",
                                        "jumpId":"manager",
                                        "jumpUrl":"'.$this->createUrl('manager/index').'",
                                        "jumpTitle":"管理员列表",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                                }';
                                Yii::app()->end();
                            }
                            else{
                                //ajax提交的，保存不成功
                                $tempFlash="请与管理员联系。";
                                if(Yii::app()->user->hasFlash('saveError'))
                                        $tempFlash=Yii::app()->user->getFlash('saveError');
                                echo '{
                                        "statusCode":"300",
                                        "message":"管理员添加不成功。'.$tempFlash.'",
                                        "navTabId":"",
                                        "rel":"",
                                        "callbackType":"",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                                }';
                                Yii::app()->end();
                            }
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $model->scenario = 'updatemanager';
                $model->managerightArray=CFunc::separateRight($model->manageright);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manager']))
		{
                        //保存旧密码，若更新为空，用回原密码
                        $model->oldPassword=$model->password;
			$model->attributes=$_POST['Manager'];
                        if($model->validate()){
                            
                            //若为空，赋以旧密码的值
                            if(empty($model->password) || empty($model->password))
                                    $model->password=$model->password2=$model->oldPassword;
                            else{
                                $model->password=$model->password2=$model->hashPassword($model->password);
                            }
                            
                            $model->manageright=CFunc::mergeRight($model->managerightArray);
                            if($model->save()){
                                //ajax提交的，保存成功
                                echo '{
                                        "statusCode":"200",
                                        "message":"管理员已修改",
                                        "navTabId":"",
                                        "rel":"",
                                        "callbackType":"closeCurrentAndJump",
                                        "jumpId":"manager",
                                        "jumpUrl":"'.$this->createUrl('manager/index').'",
                                        "jumpTitle":"管理员列表",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                                }';
                                Yii::app()->end();
                            }
                            else{
                                //ajax提交的，保存不成功
                                $tempFlash="请与管理员联系。";
                                if(Yii::app()->user->hasFlash('saveError'))
                                        $tempFlash=Yii::app()->user->getFlash('saveError');
                                echo '{
                                        "statusCode":"300",
                                        "message":"管理员修改不成功。'.$tempFlash.'",
                                        "navTabId":"",
                                        "rel":"",
                                        "callbackType":"",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                                }';
                                Yii::app()->end();
                            }
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        /*
         * 修改自己的密码
         */
	public function actionChangepswd()
	{
		$model=new Manager;
                $model->scenario = 'changepswd';

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Manager']))
		{
                        $model->attributes=$_POST['Manager'];
                        
                        /*
                         * 因为不采用save()方法，不通过rules的规则进行验证，所以下面手动进行验证
                         */
                        if(empty($model->oldPassword) || empty($model->password) || empty($model->password2)){
                            //报错
                            echo '{
                                    "statusCode":"300",
                                    "message":"修改不成功，请输入密码",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        elseif($model->password!=$model->password2){
                            //报错
                            echo '{
                                    "statusCode":"300",
                                    "message":"修改不成功，两次输入的新密码不一致",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //验证原密码
                            //var_dump($model);
                            $oldModel=Manager::model()->findByPk(Yii::app()->user->id);
                            if($oldModel->validatePassword($model->oldPassword)){
                                    $updatePswd=array('password'=>$model->hashPassword($model->password));
                                    $model->updateByPk(Yii::app()->user->id, $updatePswd);
                                    CFunc::writeLog('修改了登陆密码');
                                    //报错
                                    echo '{
                                            "statusCode":"200",
                                            "message":"密码修改成功",
                                            "navTabId":"changePassword",
                                            "rel":"",
                                            "callbackType":"",
                                            "forwardUrl":"",
                                            "confirmMsg":""
                                    }';
                                    Yii::app()->end();
                            }
                            else{
                                //报错
                                echo '{
                                        "statusCode":"300",
                                        "message":"修改不成功，你输入的原密码不正确。",
                                        "navTabId":"",
                                        "rel":"",
                                        "callbackType":"",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                                }';
                                Yii::app()->end();
                            }
                        }
		}

		$this->render('changepswd',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

                        //删除成功
                        echo '{
                                "statusCode":"200",
                                "message":"删除成功",
                                "navTabId":"content",
                                "rel":"",
                                "callbackType":"",
                                "forwardUrl":"",
                                "confirmMsg":""
                        }';
                        Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            /*
		$dataProvider=new CActiveDataProvider('Manager');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
                */
            
            //用admin的内容代替
		$model=new Manager('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manager']))
			$model->attributes=$_GET['Manager'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	public function actionAdmin()
	{
		$model=new Manager('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Manager']))
			$model->attributes=$_GET['Manager'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Manager::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='manager-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
