<?php

class PictureController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_PICTURE)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_PICTURE)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_PICTURE)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_PICTURE)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Picture;
                $model->weight=50;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                if(isset($_GET['Picture']))
                    $model->piccategory_id=$_GET['Picture']['piccategory_id'];

		if(isset($_POST['Picture']))
		{
			$model->attributes=$_POST['Picture'];
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"图片已添加",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"picture",
                                    "jumpUrl":"'.$this->createUrl('picture/index').'",
                                    "jumpTitle":"图片列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"图片添加不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Picture']))
		{
			$model->attributes=$_POST['Picture'];
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"图片已修改",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"picture",
                                    "jumpUrl":"'.$this->createUrl('picture/index').'",
                                    "jumpTitle":"图片列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"图片修改不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

                        //删除成功
                        echo '{
                                "statusCode":"200",
                                "message":"删除成功",
                                "navTabId":"picture",
                                "rel":"",
                                "callbackType":"",
                                "forwardUrl":"",
                                "confirmMsg":""
                        }';
                        Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isset($_GET['numPerPage'])) {
			Yii::app()->user->setState('numPerPage',(int)$_GET['numPerPage']);
			unset($_GET['numPerPage']);
		}  else {
			Yii::app()->user->setState('numPerPage',20);
                }
		if (isset($_GET['pageNum'])) {
			Yii::app()->user->setState('pageNum',(int)$_GET['pageNum']);
			unset($_GET['pageNum']);
		}  else {
			Yii::app()->user->setState('pageNum',0);
                }
		$model=new Picture('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Picture']))
			$model->attributes=$_GET['Picture'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Picture::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='picture-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
