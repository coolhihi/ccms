<?php

class SiteController extends Controller
{
	public $layout='//layouts/main';
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
        
	public function accessRules()
	{
		return array(
			array(
                            'allow',
                            'actions'=>array('index','logout','viewimg','flushcache'),
                            'users'=>array('@'),
			),
			array(
                            'allow',
                            'actions'=>array('login','ajaxlogin','error'),
                            'users'=>array('*'),
			),
			array(
                            'deny',  // deny all users
                            'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
		$this->render('index');
	}
        
	public function actionViewimg($imgsrc)
	{
                echo CHtml::image(CFunc::srcUrlDecode($imgsrc));
	}
        
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest){
	    		echo $error['message'];
		}
	    	else{
                    $this->render('error', $error);
		}
	    }
	}
        
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
                                CFunc::writeLog('登陆系统');
				$this->redirect(array("index"));
                        }
		}
		// display the login form

                $this->pageTitle=Yii::app()->name.' - 登陆';
		$this->render('login',array('model'=>$model));
	}
        
	public function actionAjaxlogin()
	{
		$model=new LoginForm;

		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
                                CFunc::writeLog('重新登陆系统');
				echo '{"statusCode":"200", "message":"重新登陆成功！", "callbackType":"closeCurrent"}';
                                Yii::app()->end();
                        }
			else{
                                echo '{"statusCode":"300", "message":"用户名或密码错误，登陆不成功！"}';
                                Yii::app()->end();
                        }
		}
                
		$this->render('ajaxlogin');
	}
        
	public function actionLogout()
	{
                CFunc::writeLog('退出系统');
		Yii::app()->user->logout();
		$this->redirect("/");
	}
        
        /*
         * 清理后台的缓存
         */
        public function actionFlushcache(){
                CFunc::flushCache(true);
        }
}