<?php

class StatisticsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','bymonth','byyear'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_STATISTICS)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('empty'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_STATISTICS)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Delete all models.
	 */
	public function actionEmpty()
	{
		if(isset($_POST['Suretodel'])){
                        if($_POST['Suretodel']['sure']==1){
                            Statistics::model()->deleteAll();
                            CFunc::writeLog('清空了流量统计数据');

                            //清零成功
                            echo '{
                                        "statusCode":"200",
                                        "message":"流量已成功清零。",
                                        "navTabId":"statistics",
                                        "rel":"",
                                        "callbackType":"closeCurrent",
                                        "forwardUrl":"",
                                        "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            $tempFlash="请进行确认操作。";
                            //清零不成功
                            echo '{
                                    "statusCode":"300",
                                    "message":"清零失败。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                }
                $this->render ('empty');
	}

	/**
	 * Today's statistics.
	 */
	public function actionIndex()
	{
		$this->render('byday');
	}

	/**
	 * Month's statistics.
	 */
	public function actionBymonth()
	{
		$this->render('bymonth');
	}

	/**
	 * Year's statistics.
	 */
	public function actionByyear()
	{
		$this->render('byyear');
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Statistics::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='statistics-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
