<?php

class TagController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('create'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('update'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('delete'),
                                'users'=>array('@'),
                                'expression'=>'CFunc::checkRight(CR_CONTENT)',
                        ),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Tag;

		if(isset($_POST['Tag']))
		{
			$model->attributes=$_POST['Tag'];
                        $model->tagname=strtolower($model->tagname);
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"关键词已添加",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"tag",
                                    "jumpUrl":"'.$this->createUrl('tag/index').'",
                                    "jumpTitle":"关键词列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"关键词添加不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Tag']))
		{
			$model->attributes=$_POST['Tag'];
                        $model->tagname=strtolower($model->tagname);
			if($model->save()){
                            //ajax提交的，保存成功
                            echo '{
                                    "statusCode":"200",
                                    "message":"关键词已修改",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"closeCurrentAndJump",
                                    "jumpId":"tag",
                                    "jumpUrl":"'.$this->createUrl('tag/index').'",
                                    "jumpTitle":"关键词列表",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
                        else{
                            //ajax提交的，保存不成功
                            $tempFlash="请与管理员联系。";
                            if(Yii::app()->user->hasFlash('saveError'))
                                    $tempFlash=Yii::app()->user->getFlash('saveError');
                            echo '{
                                    "statusCode":"300",
                                    "message":"关键词修改不成功。'.$tempFlash.'",
                                    "navTabId":"",
                                    "rel":"",
                                    "callbackType":"",
                                    "forwardUrl":"",
                                    "confirmMsg":""
                            }';
                            Yii::app()->end();
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

                        //删除成功
                        echo '{
                                "statusCode":"200",
                                "message":"删除成功",
                                "navTabId":"tag",
                                "rel":"",
                                "callbackType":"",
                                "forwardUrl":"",
                                "confirmMsg":""
                        }';
                        Yii::app()->end();
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if (isset($_GET['numPerPage'])) {
			Yii::app()->user->setState('numPerPage',(int)$_GET['numPerPage']);
			unset($_GET['numPerPage']);
		}  else {
			Yii::app()->user->setState('numPerPage',20);
                }
		if (isset($_GET['pageNum'])) {
			Yii::app()->user->setState('pageNum',(int)$_GET['pageNum']);
			unset($_GET['pageNum']);
		}  else {
			Yii::app()->user->setState('pageNum',0);
                }
		$model=new Tag('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Tag']))
			$model->attributes=$_GET['Tag'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Tag::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tag-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
