<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Upload
 *
 * @author Uploadistrator
 */
class UploadController extends Controller {
    //put your code here
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow upload user to perform 'upload' and 'delete' actions
				'actions'=>array('link','image','flash','media','attachment'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        /*
         * 附件上传，纯上传，不生成缩略图
         */
        public function actionLink(){
            try{
                //$_FILES["Filedata"]["size"];
                $retval="";
                $pt=strrpos($_FILES["Filedata"]["name"], ".");
                if($pt) $retval=substr($_FILES["Filedata"]["name"], $pt);
                else $retval=".linktmp";
                //$tempArr=explode('.',$_FILES["Filedata"]["name"]);
                $fileName = md5(rand()*10000000);
                $fileName = UPLOADLINK_PATH.'/' . $fileName;
                $fileName = $fileName . $retval;
                move_uploaded_file($_FILES["Filedata"]["tmp_name"], $fileName);

                echo '{"err":"","msg":"'.Yii::app()->baseUrl.'/'.$fileName.'"}';
            } catch (Exception $exc) {
                echo '{"err":"1","msg":"上传失败，请与管理员联系"}';
            }
        }
        
        /*
         * 图片上传，纯上传，不生成缩略图
         */
        public function actionImage(){
            try{
                $retval="";
                $pt=strrpos($_FILES["Filedata"]["name"], ".");
                if($pt) $retval=substr($_FILES["Filedata"]["name"], $pt);
                else $retval=".imagetmp";
                //$tempArr=explode('.',$_FILES["Filedata"]["name"]);
                $fileName = md5(rand()*10000000);
                $fileName = UPLOADIMAGE_PATH.'/' . $fileName;
                $fileName = $fileName . $retval;
                @move_uploaded_file($_FILES["Filedata"]["tmp_name"], $fileName);
                
                //通过image扩展自动生成缩小图
                $smallFileName=CFunc::getSmallImg($fileName,true);
                //用下边的图片另存
                //@copy($fileName,$smallFileName);
                
                //以下采用Yii设入image扩展包进行缩放处理
                $image = Yii::app()->image->load($fileName);
                $image->resize(SMALLPIC_SIZE_WIDTH, SMALLPIC_SIZE_HEIGHT);//自动保持比例，设定为宽最大值和高最大值
                //$image->resize(400, 100)->rotate(-45)->quality(75)->sharpen(20);
                $image->save($smallFileName); // or $image->save('images/small.jpg');

                echo '{"err":"","msg":"'.Yii::app()->baseUrl.'/'.$fileName.'"}';
            } catch (Exception $exc) {
                echo '{"err":"1","msg":"上传失败，请与管理员联系"}';
            }
        }
        
        /*
         * FLASH上传，纯上传，不生成缩略图
         */
        public function actionFlash(){
            try{
                $retval="";
                $pt=strrpos($_FILES["Filedata"]["name"], ".");
                if($pt) $retval=substr($_FILES["Filedata"]["name"], $pt);
                else $retval=".flashtmp";
                //$tempArr=explode('.',$_FILES["Filedata"]["name"]);
                $fileName = md5(rand()*10000000);
                $fileName = UPLOADFLASH_PATH.'/' . $fileName;
                $fileName = $fileName . $retval;
                @move_uploaded_file($_FILES["Filedata"]["tmp_name"], $fileName);

                echo '{"err":"","msg":"'.Yii::app()->baseUrl.'/'.$fileName.'"}';
            } catch (Exception $exc) {
                echo '{"err":"1","msg":"上传失败，请与管理员联系"}';
            }
        }
        
        /*
         * 媒体上传，纯上传，不生成缩略图
         */
        public function actionMedia(){
            try{
                $retval="";
                $pt=strrpos($_FILES["Filedata"]["name"], ".");
                if($pt) $retval=substr($_FILES["Filedata"]["name"], $pt);
                else $retval=".mediatmp";
                //$tempArr=explode('.',$_FILES["Filedata"]["name"]);
                $fileName = md5(rand()*10000000);
                $fileName = UPLOADMEDIA_PATH.'/' . $fileName;
                $fileName = $fileName . $retval;
                @move_uploaded_file($_FILES["Filedata"]["tmp_name"], $fileName);

                echo '{"err":"","msg":"'.Yii::app()->baseUrl.'/'.$fileName.'"}';
            } catch (Exception $exc) {
                echo '{"err":"1","msg":"上传失败，请与管理员联系"}';
            }
        }
        
        /*
         * 附件上传，返回原文件名不生成缩略图
         */
        public function actionAttachment(){
            try{
                $retval="";
                $pt=strrpos($_FILES["Filedata"]["name"], ".");
                if($pt) $retval=substr($_FILES["Filedata"]["name"], $pt);
                else $retval=".attachmenttmp";
                //$tempArr=explode('.',$_FILES["Filedata"]["name"]);
                $fileName = md5(rand()*10000000);
                $fileName = UPLOADATTACHMENT_PATH.'/' . $fileName;
                $fileName = $fileName . $retval;
                @move_uploaded_file($_FILES["Filedata"]["tmp_name"], $fileName);

                echo '{"err":"","msg":"'.$_FILES["Filedata"]["name"].'|&|'.Yii::app()->baseUrl.'/'.$fileName.'"}';
            } catch (Exception $exc) {
                echo '{"err":"1","msg":"上传失败，请与管理员联系"}';
            }
        }
}

?>
