<?php 
/* 
* DatabaseBackup Class for Yii
* author COoL
* 
*/ 
class DatabaseBackup{
    
    //备份函数
    public static function backupDb($filepath, $tables = '*') {
        if ($tables == '*') {
            $tables = array();
            $tables = Yii::app()->db->schema->getTableNames();
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }
        $return = '';

        foreach ($tables as $table) {
            $result = Yii::app()->db->createCommand('SELECT * FROM ' . $table)->query();
            //$return.= 'TRUNCATE TABLE '.$table.';'."\n\n";
            //$return.= 'DROP TABLE IF EXISTS ' . $table . ';';
            //$row2 = Yii::app()->db->createCommand('SHOW CREATE TABLE ' . $table)->queryRow();
            //$return.= "\n\n" . $row2['Create Table'] . ";\n\n";
            foreach ($result as $row) {
                $return.= 'INSERT INTO ' . $table . ' VALUES(';
                foreach ($row as $data) {
                    $data = addslashes($data);

                    // Updated to preg_replace to suit PHP5.3 +
                    $data = preg_replace("/\n/", "\\n", $data);
                    if (isset($data)) {
                        $return.= '"' . $data . '"';
                    } else {
                        $return.= '""';
                    }
                    $return.= ',';
                }
                $return = substr($return, 0, strlen($return) - 1);
                $return.= ");\n";
            }
            $return.="\n";
        }
        //save file
        $handle = fopen($filepath, 'w+');
        fwrite($handle, $return);
        fclose($handle);
    }
    
    //还原函数
    public static function recoverDb($filepath, $tables = '*') {
        //设置时间限制为0，即不强制停止
        set_time_limit(0);
        
        
        //采用事务，导入出错则回滚
        $transaction = Yii::app()->db->beginTransaction();
        try{
            $sqls='';
            
            //反转备份表顺序
            $tables=explode(',', DBBACKUP_TABLES);
            $tables=array_reverse($tables);
            
            //删除所有表
            foreach($tables as $table){
                $sqls.='DROP TABLE '.$table.";\n";
            }
            $sqls.="\n\n";
            
            //读取结构
            $dbModelFile=DBBACKUP_PATH.'/init_model.sql';
            $handle=@fopen($dbModelFile,'r');
            $sqls.=fread($handle, filesize($dbModelFile));
            fclose($handle);
            $sqls.="\n\n";
            
            //读取备份
            $handle=@fopen($filepath,'r');
            $sqls.=fread($handle, filesize($filepath));
            fclose($handle);
            //echo $sqls;
            
            Yii::app()->db->createCommand($sqls)->query();
            $transaction->commit();
            return true;
        }
        catch (Exception $e) {
            $transaction->rollback(); //如果操作失败, 数据回滚
            return false;
        }
    }
} 