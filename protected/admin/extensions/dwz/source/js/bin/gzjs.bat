cd e:\guan\git\ccms\protected\admin\extensions\dwz\source\js\bin
e:

REM -------------- start package javascript --------------

type ..\src\dwz.core.js > dwzESC.js
type ..\src\dwz.util.date.js >> dwzESC.js
type ..\src\dwz.validate.method.js >> dwzESC.js
type ..\src\dwz.barDrag.js >> dwzESC.js
type ..\src\dwz.drag.js >> dwzESC.js
type ..\src\dwz.tree.js >> dwzESC.js
type ..\src\dwz.accordion.js >> dwzESC.js
type ..\src\dwz.ui.js >> dwzESC.js
type ..\src\dwz.theme.js >> dwzESC.js
type ..\src\dwz.switchEnv.js >> dwzESC.js

type ..\src\dwz.alertMsg.js >> dwzESC.js
type ..\src\dwz.contextmenu.js >> dwzESC.js
type ..\src\dwz.navTab.js >> dwzESC.js
type ..\src\dwz.tab.js >> dwzESC.js
type ..\src\dwz.resize.js >> dwzESC.js
type ..\src\dwz.dialog.js >> dwzESC.js
type ..\src\dwz.dialogDrag.js >> dwzESC.js
type ..\src\dwz.sortDrag.js >> dwzESC.js
type ..\src\dwz.cssTable.js >> dwzESC.js
type ..\src\dwz.stable.js >> dwzESC.js
type ..\src\dwz.taskBar.js >> dwzESC.js
type ..\src\dwz.ajax.js >> dwzESC.js
type ..\src\dwz.pagination.js >> dwzESC.js
type ..\src\dwz.database.js >> dwzESC.js
type ..\src\dwz.datepicker.js >> dwzESC.js
type ..\src\dwz.effects.js >> dwzESC.js
type ..\src\dwz.panel.js >> dwzESC.js
type ..\src\dwz.checkbox.js >> dwzESC.js
type ..\src\dwz.combox.js >> dwzESC.js
type ..\src\dwz.history.js >> dwzESC.js
type ..\src\dwz.print.js >> dwzESC.js

cscript ESC.wsf -l 1 -ow dwzESC1.js dwzESC.js
cscript ESC.wsf -l 2 -ow dwzESC2.js dwzESC1.js
cscript ESC.wsf -l 3 -ow dwzESC3.js dwzESC2.js

type dwzESC2.js > dwz.min.js
#gzip -f dwz.min.js
copy dwz.min.js ..\dwz.min.js /y

del dwzESC*.js
del dwz.min.js.gz
cmd