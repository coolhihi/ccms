function initEnv() {
	$("body").append(DWZ.frag["dwzFrag"]);

	if ( $.browser.msie && /6.0/.test(navigator.userAgent) ) {
		try {
			document.execCommand("BackgroundImageCache", false, true);
		}catch(e){}
	}
	//清理浏览器内存,只对IE起效
	if ($.browser.msie) {
		window.setInterval("CollectGarbage();", 10000);
	}

	$(window).resize(function(){
		initLayout();
		$(this).trigger("resizeGrid");
	});

	var ajaxbg = $("#background,#progressBar");
	ajaxbg.hide();
	$(document).ajaxStart(function(){
		ajaxbg.show();
	}).ajaxStop(function(){
		ajaxbg.hide();
	});
	
	$("#leftside").jBar({minW:150, maxW:700});
	
	if ($.taskBar) $.taskBar.init();
	navTab.init();
	if ($.fn.switchEnv) $("#switchEnvBox").switchEnv();
	if ($.fn.navMenu) $("#navMenu").navMenu();
		
	setTimeout(function(){
		initLayout();
		initUI();
		
		// navTab styles
		var jTabsPH = $("div.tabsPageHeader");
		jTabsPH.find(".tabsLeft").hoverClass("tabsLeftHover");
		jTabsPH.find(".tabsRight").hoverClass("tabsRightHover");
		jTabsPH.find(".tabsMore").hoverClass("tabsMoreHover");
	
	}, 10);

}
function initLayout(){
	var iContentW = $(window).width() - (DWZ.ui.sbar ? $("#sidebar").width() + 10 : 34) - 5;
	var iContentH = $(window).height() - $("#header").height() - 34;

	$("#container").width(iContentW);
	$("#container .tabsPageContent").height(iContentH - 34).find("[layoutH]").layoutH();
	$("#sidebar, #sidebar_s .collapse, #splitBar, #splitBarProxy").height(iContentH - 5);
	$("#taskbar").css({top: iContentH + $("#header").height() + 5, width:$(window).width()});
}

function initUI(_box){
	var $p = $(_box || document);

	//tables
	$("table.table", $p).jTable();
	
	// css tables
	$('table.list', $p).cssTable();

	//auto bind tabs
	$("div.tabs", $p).each(function(){
		var $this = $(this);
		var options = {};
		options.currentIndex = $this.attr("currentIndex") || 0;
		options.eventType = $this.attr("eventType") || "click";
		$this.tabs(options);
	});

	$("ul.tree", $p).jTree();
	$('div.accordion', $p).each(function(){
		var $this = $(this);
		$this.accordion({fillSpace:$this.attr("fillSpace"),alwaysOpen:true,active:0});
	});

	$(":button.checkboxCtrl, :checkbox.checkboxCtrl", $p).checkboxCtrl($p);
	
	if ($.fn.combox) $("select.combox",$p).combox();
	
	if ($.fn.xheditor) {
		var plugins={
			Code:{c:'btnCode',t:'\u63d2\u5165\u4ee3\u7801',h:1,e:function(){
				var _this=this;
				var htmlCode='<div><select id="xheCodeType"><option value="html">HTML/XML</option><option value="js">Javascript</option><option value="css">CSS</option><option value="php">PHP</option><option value="java">Java</option><option value="py">Python</option><option value="pl">Perl</option><option value="rb">Ruby</option><option value="cs">C#</option><option value="c">C++/C</option><option value="vb">VB/ASP</option><option value="">\u5176\u5b83</option></select></div><div><textarea id="xheCodeValue" wrap="soft" spellcheck="false" style="width:300px;height:100px;" /></div><div style="text-align:right;"><input type="button" id="xheSave" value="\u786e\u5b9a" /></div>';			var jCode=$(htmlCode),jType=$('#xheCodeType',jCode),jValue=$('#xheCodeValue',jCode),jSave=$('#xheSave',jCode);
				jSave.click(function(){
					_this.loadBookmark();
					_this.pasteHTML('<pre class="brush: '+jType.val()+'">'+_this.domEncode(jValue.val())+'</pre>');
					_this.hidePanel();
					return false;	
				});
				_this.saveBookmark();
				_this.showDialog(jCode);
			}},
			Youku:{c:'btnYouku',t:'\u63d2\u5165\u7f51\u7edc\u89c6\u9891\uff08\u652f\u6301\u4f18\u9177\u548c\u571f\u8c46\uff09',h:1,e:function(){
				var _this=this;
				var htmlYouku='<div>\u89c2\u770b\u7f51\u5740:&nbsp; <input type="text" id="xheYoukuUrl" value="http://" class="xheText" /></div><div>\u5bbd\u5ea6\u9ad8\u5ea6: <input type="text" id="xheYoukuWidth" style="width:40px;" value="480" /> x <input type="text" id="xheYoukuHeight" style="width:40px;" value="400" /></div><div style="text-align:right;"><input type="button" id="xheSave" value="\u786e\u5b9a" /></div>';
				var jYouku=$(htmlYouku),jUrl=$('#xheYoukuUrl',jYouku),jWidth=$('#xheYoukuWidth',jYouku),jHeight=$('#xheYoukuHeight',jYouku),jSave=$('#xheSave',jYouku);
				jSave.click(function(){
					_this.loadBookmark();
					var tmp_youku_id=jUrl.val();
					if(tmp_youku_id.toLowerCase().indexOf('youku')!=-1){
						tmp_youku_id=tmp_youku_id.replace(/^(.*?)id_([^\.]+?)\.html(.*?)$/,"$2");
						_this.pasteHTML('<embed src="http://player.youku.com/player.php/sid/'+tmp_youku_id+'/v.swf" quality="high" width="'+jWidth.val()+'" height="'+jHeight.val()+'" align="middle" allowScriptAccess="sameDomain" allowFullscreen="true" type="application/x-shockwave-flash"></embed>');
					}
					else if(tmp_youku_id.toLowerCase().indexOf('tudou')!=-1){
						tmp_youku_id=tmp_youku_id.replace(/^(.*?)view\/([^\.]+?)\/(.*?)$/,"$2");
						_this.pasteHTML('<embed src="http://www.tudou.com/v/'+tmp_youku_id+'/v.swf" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="'+jWidth.val()+'" height="'+jHeight.val()+'"></embed>');
					}
					//_this.pasteHTML('<embed src="http://player.video.qiyi.com/68616f922c974268868c1dc7f2d0cc98/0/95/20120809/785f575dee9ac4b1.swf-pid=23648-ptype=2-albumId=208815-tvId=248410" quality="high" width="480" height="400" align="middle" allowScriptAccess="always" type="application/x-shockwave-flash"></embed>');
					//_this.pasteHTML('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" width="'+jWidth.val()+'" height="'+jHeight.val()+'" id="myjwplayer" ><param name="src" value="/inc/jwplayer/player.swf" /><param name="flashvars" value="file='+jUrl.val()+'" /><param name="autostart" value="false" /><param name="PlayCount" value="1" />' + '<embed height="'\u3000+\u3000jHeight.val()\u3000+\u3000'" width="'\u3000+\u3000jWidth.val()\u3000+\u3000'" autostart="false" flashvars="file='\u3000+jUrl.val()+\u3000'" allowfullscreen="true" allowscriptaccess="always" bgcolor="#ffffff" src="/inc/jwplayer/player.swf"></embed></object>');
					_this.hidePanel();
					return false;	
				});
				_this.saveBookmark();
				_this.showDialog(jYouku);
			}},
			/*
			Flv:{c:'btnFlv',t:'\u63d2\u5165Flv\u89c6\u9891',h:1,e:function(){
				var _this=this;
				var htmlFlv='<div>Flv\u6587\u4ef6:&nbsp; <input type="text" id="xheFlvUrl" value="http://" class="xheText" /></div><div>\u5bbd\u5ea6\u9ad8\u5ea6: <input type="text" id="xheFlvWidth" style="width:40px;" value="480" /> x <input type="text" id="xheFlvHeight" style="width:40px;" value="400" /></div><div style="text-align:right;"><input type="button" id="xheSave" value="\u786e\u5b9a" /></div>';
				var jFlv=$(htmlFlv),jUrl=$('#xheFlvUrl',jFlv),jWidth=$('#xheFlvWidth',jFlv),jHeight=$('#xheFlvHeight',jFlv),jSave=$('#xheSave',jFlv);
				jSave.click(function(){
					_this.loadBookmark();
					_this.pasteHTML('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" width="'+jWidth.val()+'" height="'+jHeight.val()+'" id="myjwplayer" ><param name="src" value="/inc/jwplayer/player.swf" /><param name="flashvars" value="file='+jUrl.val()+'" /><param name="autostart" value="false" /><param name="PlayCount" value="1" />' + '<embed height="'\u3000+\u3000jHeight.val()\u3000+\u3000'" width="'\u3000+\u3000jWidth.val()\u3000+\u3000'" autostart="false" flashvars="file='\u3000+jUrl.val()+\u3000'" allowfullscreen="true" allowscriptaccess="always" bgcolor="#ffffff" src="/inc/jwplayer/player.swf"></embed></object>');
					//_this.pasteText('[flv='+jWidth.val()+','+jHeight.val()+']'+jUrl.val()+'[/flv]');
					_this.hidePanel();
					return false;	
				});
				_this.saveBookmark();
				_this.showDialog(jFlv);
			}},
			*/
			map:{c:'btnMap',t:'\u63d2\u5165Google\u5730\u56fe',e:function(){
				var _this=this;
				_this.saveBookmark();
				_this.showIframeModal('Google \u5730\u56fe','/inc/googlemap/googlemap.html',function(v){_this.loadBookmark();_this.pasteHTML('<img src="'+v+'" />');},538,404);
			}}
		};
		
		var emots={
			"msn":{
				"name":"MSN","count":40,"width":22,"height":22,"line":8
			},
			"pidgin":{
				"name":"Pidgin","width":22,"height":25,"line":8,"list":{
					"smile":"\u5fae\u7b11","cute":"\u53ef\u7231","wink":"\u7728\u773c","laugh":"\u5927\u7b11","victory":"\u80dc\u5229","sad":"\u4f24\u5fc3","cry":"\u54ed\u6ce3","angry":"\u751f\u6c14","shout":"\u5927\u9a82","curse":"\u8bc5\u5492","devil":"\u9b54\u9b3c","blush":"\u5bb3\u7f9e","tongue":"\u5410\u820c\u5934","envy":"\u7fa1\u6155","cool":"\u800d\u9177","kiss":"\u543b","shocked":"\u60ca\u8bb6","sweat":"\u6c57","sick":"\u751f\u75c5","bye":"\u518d\u89c1","tired":"\u7d2f","sleepy":"\u7761\u4e86","question":"\u7591\u95ee","rose":"\u73ab\u7470","gift":"\u793c\u7269","coffee":"\u5496\u5561","music":"\u97f3\u4e50","soccer":"\u8db3\u7403","good":"\u8d5e\u540c","bad":"\u53cd\u5bf9","love":"\u5fc3","brokenheart":"\u4f24\u5fc3"
				}
			},
			"ipb":{
				"name":"IPB","width":20,"height":25,"line":8,"list":{
					"smile":"\u5fae\u7b11","joyful":"\u5f00\u5fc3","laugh":"\u7b11","biglaugh":"\u5927\u7b11","w00t":"\u6b22\u547c","wub":"\u6b22\u559c","depres":"\u6cae\u4e27","sad":"\u60b2\u4f24","cry":"\u54ed\u6ce3","angry":"\u751f\u6c14","devil":"\u9b54\u9b3c","blush":"\u8138\u7ea2","kiss":"\u543b","surprised":"\u60ca\u8bb6","wondering":"\u7591\u60d1","unsure":"\u4e0d\u786e\u5b9a","tongue":"\u5410\u820c\u5934","cool":"\u800d\u9177","blink":"\u7728\u773c","whistling":"\u5439\u53e3\u54e8","glare":"\u8f7b\u89c6","pinch":"\u634f","sideways":"\u4fa7\u8eab","sleep":"\u7761\u4e86","sick":"\u751f\u75c5","ninja":"\u5fcd\u8005","bandit":"\u5f3a\u76d7","police":"\u8b66\u5bdf","angel":"\u5929\u4f7f","magician":"\u9b54\u6cd5\u5e08","alien":"\u5916\u661f\u4eba","heart":"\u5fc3\u52a8"
				}
			}
		};
		

		$("textarea.editor", $p).each(function(){
			var $this = $(this);
			$this.focus();
			var op = {plugins:plugins, emots:emots, loadCSS:'<style>pre{margin-left:2em;border-left:3px solid #CCC;padding:0 1em;}</style>', html5Upload:false, skin: $this.attr("skin") || 'default', tools: $this.attr("tools") || 'full'};
			var upAttrs = [
				["upLinkUrl","upLinkExt","zip,rar,txt"],
				["upImgUrl","upImgExt","jpg,jpeg,gif,png"],
				["upFlashUrl","upFlashExt","swf"],
				["upMediaUrl","upMediaExt","avi,flv,mp4,mp3"]
			];
			
			$(upAttrs).each(function(i){
				var urlAttr = upAttrs[i][0];
				var extAttr = upAttrs[i][1];
				
				if ($this.attr(urlAttr)) {
					op[urlAttr] = $this.attr(urlAttr);
					op[extAttr] = $this.attr(extAttr) || upAttrs[i][2];
				}
			});
			
			$this.xheditor(op);
		});
	}
	
	if ($.fn.uploadify) {
		$(":file[uploader]", $p).each(function(){
			var $this = $(this);
			var options = {
				uploader: $this.attr("uploader"),
				script: $this.attr("script"),
				cancelImg: $this.attr("cancelImg"),
				queueID: $this.attr("fileQueue") || "fileQueue",
				fileDesc: $this.attr("fileDesc") || "*.jpg;*.jpeg;*.gif;*.png;*.pdf",
				fileExt : $this.attr("fileExt") || "*.jpg;*.jpeg;*.gif;*.png;*.pdf",
				folder	: $this.attr("folder"),
				auto: true,
				multi: true,
				onError:uploadifyError,
				onComplete: $this.attr("onComplete") || uploadifyComplete,
				onAllComplete: uploadifyAllComplete
			};
			if ($this.attr("onComplete")) {
				options.onComplete = DWZ.jsonEval($this.attr("onComplete"));
			}
			if ($this.attr("onAllComplete")) {
				options.onAllComplete = DWZ.jsonEval($this.attr("onAllComplete"));
			}
			if ($this.attr("scriptData")) {
				options.scriptData = DWZ.jsonEval($this.attr("scriptData"));
			}
			$this.uploadify(options);
		});
	}
	if (typeof(SWFUpload)!="undefined") {
		$(".forswfupload", $p).each(function(){
			var $this = $(this);
			var options = {
				flash_url: $this.attr("flash_url"),
				flash9_url: $this.attr("flash9_url"),
				upload_url: $this.attr("upload_url"),
				post_params: {},
				file_size_limit: $this.attr("file_size_limit") || "2 MB",
				file_types : $this.attr("file_types") || "*.jpg;*.jpeg;*.gif;*.png;*.pdf",
				file_types_description : $this.attr("file_types_description") || "Image",
				file_upload_limit: 100,
				file_queue_limit: 0,
				custom_settings : {},
				debug: $this.attr("debug") || false,
				button_image_url: $this.attr("button_image_url") || "/images/SmallSpyGlassWithTransperancy_17x18.png",
				button_width: $this.attr("button_width") || "140",
				button_height: $this.attr("button_height") || "18",
				button_placeholder_id: $this.attr("button_placeholder_id"),
				button_text: $this.attr("button_text") || '<span class="sud_btn">本地上传 <span class="sud_sbtn">(2 MB max)</span></span>',
				button_text_style: $this.attr("button_text_style") || '.sud_btn { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .sud_sbtn { font-size: 10pt; }',
				button_text_top_padding: $this.attr("button_text_top_padding") || 0,
				button_text_left_padding: $this.attr("button_text_left_padding") || 18,
				button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
				button_cursor: SWFUpload.CURSOR.HAND,

				swfupload_preload_handler : preLoad,
				swfupload_load_failed_handler : loadFailed,
				file_queued_handler : fileQueued,
				file_queue_error_handler : fileQueueError,
				file_dialog_complete_handler : fileDialogComplete,
				upload_start_handler : uploadStart,
				upload_progress_handler : uploadProgress,
				upload_error_handler : uploadError,
				upload_success_handler : uploadSuccess,
				upload_complete_handler : uploadComplete,
				queue_complete_handler : queueComplete
			};
			if ($this.attr("swfupload_preload_handler")) {
				options.swfupload_preload_handler = DWZ.jsonEval($this.attr("swfupload_preload_handler"));
			}
			if ($this.attr("swfupload_load_failed_handler")) {
				options.swfupload_load_failed_handler = DWZ.jsonEval($this.attr("swfupload_load_failed_handler"));
			}
			if ($this.attr("file_queued_handler")) {
				options.file_queued_handler = DWZ.jsonEval($this.attr("file_queued_handler"));
			}
			if ($this.attr("file_dialog_complete_handler")) {
				options.file_dialog_complete_handler = DWZ.jsonEval($this.attr("file_dialog_complete_handler"));
			}
			if ($this.attr("upload_start_handler")) {
				options.upload_start_handler = DWZ.jsonEval($this.attr("upload_start_handler"));
			}
			if ($this.attr("upload_progress_handler")) {
				options.upload_progress_handler = DWZ.jsonEval($this.attr("upload_progress_handler"));
			}
			if ($this.attr("upload_error_handler")) {
				options.upload_error_handler = DWZ.jsonEval($this.attr("upload_error_handler"));
			}
			if ($this.attr("upload_success_handler")) {
				options.upload_success_handler = DWZ.jsonEval($this.attr("upload_success_handler"));
			}
			if ($this.attr("upload_complete_handler")) {
				options.upload_complete_handler = DWZ.jsonEval($this.attr("upload_complete_handler"));
			}
			if ($this.attr("queue_complete_handler")) {
				options.queue_complete_handler = DWZ.jsonEval($this.attr("queue_complete_handler"));
			}
			if ($this.attr("post_params")) {
				options.post_params = DWZ.jsonEval($this.attr("post_params"));
			}
			if ($this.attr("custom_settings")) {
				options.custom_settings = DWZ.jsonEval($this.attr("custom_settings"));
			}
			var temp=new SWFUpload(options);
		});
	}
	
	// init styles
	$("input[type=text], input[type=password], textarea", $p).addClass("textInput").focusClass("focus");

	$("input[readonly], textarea[readonly]", $p).addClass("readonly");
	$("input[disabled=true], textarea[disabled=true]", $p).addClass("disabled");

	$("input[type=text]", $p).not("div.tabs input[type=text]", $p).filter("[alt]").inputAlert();

	//Grid ToolBar
	$("div.panelBar li, div.panelBar", $p).hoverClass("hover");

	//Button
	$("div.button", $p).hoverClass("buttonHover");
	$("div.buttonActive", $p).hoverClass("buttonActiveHover");
	
	//tabsPageHeader
	$("div.tabsHeader li, div.tabsPageHeader li, div.accordionHeader, div.accordion", $p).hoverClass("hover");
	
	$("div.panel", $p).jPanel();

	//validate form
	$("form.required-validate", $p).each(function(){
		$(this).validate({
			focusInvalid: false,
			focusCleanup: true,
			errorElement: "span",
			ignore:".ignore",
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
					var message = DWZ.msg("validateFormError",[errors]);
					alertMsg.error(message);
				} 
			}
		});
	});

	if ($.fn.datepicker){
		$('input.date', $p).each(function(){
			var $this = $(this);
			var opts = {};
			if ($this.attr("format")) opts.pattern = $this.attr("format");
			if ($this.attr("yearstart")) opts.yearstart = $this.attr("yearstart");
			if ($this.attr("yearend")) opts.yearend = $this.attr("yearend");
			$this.datepicker(opts);
		});
	}

	// navTab
	$("a[target=navTab]", $p).each(function(){
		$(this).click(function(event){
			var $this = $(this);
			var title = $this.attr("title") || $this.text();
			var tabid = $this.attr("rel") || "_blank";
			var fresh = eval($this.attr("fresh") || "true");
			var external = eval($this.attr("external") || "false");
			var url = unescape($this.attr("href")).replaceTmById($(event.target).parents(".unitBox:first"));
			DWZ.debug(url);
			if (!url.isFinishedTm()) {
				alertMsg.error($this.attr("warn") || DWZ.msg("alertSelectMsg"));
				return false;
			}
			navTab.openTab(tabid, url,{title:title, fresh:fresh, external:external});

			event.preventDefault();
		});
	});
	
	//dialogs
	$("a[target=dialog]", $p).each(function(){
		$(this).click(function(event){
			var $this = $(this);
			var title = $this.attr("title") || $this.text();
			var rel = $this.attr("rel") || "_blank";
			var options = {};
			var w = $this.attr("width");
			var h = $this.attr("height");
			if (w) options.width = w;
			if (h) options.height = h;
			options.max = eval($this.attr("max") || "false");
			options.mask = eval($this.attr("mask") || "false");
			options.maxable = eval($this.attr("maxable") || "true");
			options.minable = eval($this.attr("minable") || "true");
			options.fresh = eval($this.attr("fresh") || "true");
			options.resizable = eval($this.attr("resizable") || "true");
			options.drawable = eval($this.attr("drawable") || "true");
			options.close = eval($this.attr("close") || "");
			options.param = $this.attr("param") || "";

			var url = unescape($this.attr("href")).replaceTmById($(event.target).parents(".unitBox:first"));
			DWZ.debug(url);
			if (!url.isFinishedTm()) {
				alertMsg.error($this.attr("warn") || DWZ.msg("alertSelectMsg"));
				return false;
			}
			$.pdialog.open(url, rel, title, options);
			
			return false;
		});
	});
	$("a[target=ajax]", $p).each(function(){
		$(this).click(function(event){
			var $this = $(this);
			var rel = $this.attr("rel");
			if (rel) {
				var $rel = $("#"+rel);
				$rel.loadUrl($this.attr("href"), {}, function(){
					$rel.find("[layoutH]").layoutH();
				});
			}

			event.preventDefault();
		});
	});
	
	$("div.pagination", $p).each(function(){
		var $this = $(this);
		$this.pagination({
			targetType:$this.attr("targetType"),
			rel:$this.attr("rel"),
			totalCount:$this.attr("totalCount"),
			numPerPage:$this.attr("numPerPage"),
			pageNumShown:$this.attr("pageNumShown"),
			currentPage:$this.attr("currentPage")
		});
	});

	if ($.fn.sortDrag) $("div.sortDrag", $p).sortDrag();

	// dwz.ajax.js
	if ($.fn.ajaxTodo) $("a[target=ajaxTodo]", $p).ajaxTodo();
	if ($.fn.dwzExport) $("a[target=dwzExport]", $p).dwzExport();

	if ($.fn.lookup) $("a[lookupGroup]", $p).lookup();
	if ($.fn.multLookup) $("[multLookup]:button", $p).multLookup();
	if ($.fn.suggest) $("input[suggestFields]", $p).suggest();
	if ($.fn.itemDetail) $("table.itemDetail", $p).itemDetail();
	if ($.fn.selectedTodo) $("a[target=selectedTodo]", $p).selectedTodo();
	if ($.fn.pagerForm) $("form[rel=pagerForm]", $p).pagerForm({parentBox:$p});

	// 这里放其他第三方jQuery插件...

	//toggleSelector
	$(".toggleSelector", $p).unbind("change");
	$(".toggleSelector", $p).each(function(){
		var $this = $(this);
		var $rel = $("."+$this.attr("rel"), $p);
		if($this.val()!=$this.attr("showValue"))
			$rel.css("display","none");
		//alert($this.val()+"?="+$this.attr("showValue"));
		$this.bind("change",function(){
			if($this.val()==$this.attr("showValue"))
				$rel.css("display","block");
			else
				$rel.css("display","none");
		});
	});
        $(".delAttachment", $p).each(function(){
		var $this = $(this);
                $this.bind("click",function(){
                    var pa=$(this).parent();
                    var field=$("#"+pa.attr("rel"));
                    field.val(field.val().replace(pa.attr("alt"),""));
                    pa.remove();
                });
        });
}


