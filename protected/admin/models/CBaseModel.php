<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 基础Model for CCMS
 */

class CBaseModel extends CActiveRecord{
    /*
     * 如果系统参数要求连图片文件一并删除，则每次删除时执行如下动作
     */
    public function afterDelete() {
        parent::afterDelete();
        if(CFunc::getCustomparam('system_picture_deletefile')=='1'){
            try{
                @unlink('.'.$this->image);
                @unlink('.'.CFunc::getSmallImg($this->image));
            } catch (Exception $exc) {
                //抓取错误，不提示，不知道为什么用 @ unlink 无效，只能这样
            }
        }
        @CFunc::flushCache();
    }
    public function afterSave() {
        parent::afterSave();
        @CFunc::flushCache();
    }
}
?>
