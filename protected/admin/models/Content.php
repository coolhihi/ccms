<?php

/**
 * This is the model class for table "{{content}}".
 *
 * The followings are the available columns in table '{{content}}':
 * @property integer $id
 * @property integer $category_id
 * @property integer $weight
 * @property string $title
 * @property string $author
 * @property string $origin
 * @property string $attachment
 * @property string $image
 * @property string $flags
 * @property integer $allowcomment
 * @property string $summary
 * @property string $content
 * @property string $tags
 * @property integer $clicktimes
 * @property integer $create_time
 * @property integer $lastedit_time
 * @property integer $enable
 *
 * The followings are the available model relations:
 * @property Category $category
 */
class Content extends CBaseModel
{
    public $flagsArr=array();
    public $image_s;
    public $create_time_str;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{content}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_id, weight, title, allowcomment, enable', 'required'),
			array('category_id, weight, allowcomment, clicktimes, create_time, lastedit_time, enable', 'numerical', 'integerOnly'=>true),
			array('title, author, origin, image, flags, tags', 'length', 'max'=>200),
			array('category_id', 'numerical', 'min'=>1, 'tooSmall'=>'请选择栏目.'),
			array('content, attachment, summary, image_s, flagsArr, create_time_str', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_id, title, author, origin, flags, tags, enable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_id' => '所属栏目',
			'weight' => '排序权重',
			'title' => '标题',
			'author' => '作者',
			'origin' => '来源',
                        'attachment' => '附件',
			'image' => '封面',
			'flags' => '特性',
			'flagsArr' => '特性',
			'allowcomment' => '评论',
			'summary' => '描述(SEO)',
			'content' => '内容',
			'tags' => '标签(SEO)',
			'clicktimes' => '点击数',
			'create_time' => '发布日期',
			'lastedit_time' => '修改时间',
			'enable' => '可用',
			'create_time_str' => '发布日期',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('origin',$this->origin,true);
		$criteria->compare('flags',$this->flags,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('enable',$this->enable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'lastedit_time DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
        
        /*
         * addClick 点击数加一
         */
        public function addClick(){
            //Yii::trace('a','cool.a');
            //以下花3.31秒
            //$this->updateByPk($this->id, array('clicktimes'=>$this->clicktimes+1));
            //Yii::trace('a','cool.b');
            
            //以下花5.9秒
            //$this->clicktimes+=1;
            //$this->update();
            //Yii::trace('a','cool.c');
            
            //以下花1.75秒，但@屏蔽不了错误，只能try
            try {
                Yii::app()->db->createCommand("update {{content}} set clicktimes=clicktimes+1 where id=".$this->id)->query();
            } catch (Exception $exc) {
            }
            //Yii::trace('a','cool.d');
        }
        
        /*
         * getFlags 返回特性的中文意义
         */
        public function getFlags(){
            $tempStr=$this->flags;
            $tempStr=str_replace('a', '头条', $tempStr);
            $tempStr=str_replace('b', '幻灯', $tempStr);
            $tempStr=str_replace('c', '推荐', $tempStr);
            return $tempStr;
        }
        
        public function beforeSave() {
		if(parent::beforeSave())
		{
			if($this->isNewRecord){
                                $this->lastedit_time=time();
                                $this->clicktimes=0;
                        }
                        else
                                $this->lastedit_time=time();
			return true;
		}
		else
			return false;
        }
        public function afterSave(){
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了文章 {".($this->title)."}");
            else
                CFunc::writeLog("修改了文章 {".($this->title)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了文章 {".($this->title)."}");
            return true;
        }
}