<?php

/**
 * This is the model class for table "{{customparam}}".
 *
 * The followings are the available columns in table '{{customparam}}':
 * @property integer $id
 * @property string $paramname
 * @property string $paramvalue
 * @property string $paramtitle
 * @property integer $manager_id
 * @property integer $enable
 * @property integer $systemparam
 */
class Customparam extends CBaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Customparam the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customparam}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('paramname, paramvalue, paramtitle, enable, systemparam', 'required'),
			array('manager_id, enable, systemparam', 'numerical', 'integerOnly'=>true),
			array('paramname', 'length', 'max'=>50),
			array('paramvalue, paramtitle', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, paramname, paramvalue, manager_id, enable, systemparam', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'paramname' => '参数名称',
			'paramvalue' => '参数值',
                        'paramtitle' => '参数标题',
			'manager_id' => '管理员',
			'enable' => '可用',
			'systemparam' => '系统参数',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('paramname',$this->paramname,true);
		$criteria->compare('paramvalue',$this->paramvalue,true);
                $criteria->compare('paramtitle',$this->paramtitle,true);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('enable',$this->enable);
		$criteria->compare('systemparam',$this->systemparam);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'systemparam DESC,id asc', //设置默认排序是create_time倒序
                        ),
		));
	}
        public function beforeSave() {
            if(parent::beforeSave()){
                if($this->isNewRecord){
                    $this->manager_id=Yii::app()->user->id;
                    $this->systemparam=0;
                }
                return true;
            }
            return false;
        }
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord){
                CFunc::writeLog('添加了参数 {'.$this->paramname.'}');
            }
            else{
                CFunc::writeLog('修改了参数 {'.$this->paramname.'}');
            }
        }
        public function beforeDelete() {
            if(parent::beforeDelete()){
                if($this->systemparam==1)
                    return false;
                else
                    return true;
            }
            return false;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog('删除了参数 {'.$this->paramname.'}');
        }
}