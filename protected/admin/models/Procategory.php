<?php

/**
 * This is the model class for table "{{procategory}}".
 *
 * The followings are the available columns in table '{{procategory}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $categoryname
 * @property integer $weight
 * @property string $image
 * @property integer $protemplate_id
 * @property string $introduction
 *
 * The followings are the available model relations:
 * @property Product[] $products
 */
class Procategory extends CActiveRecord
{
    public $image_s;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Procategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{procategory}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, categoryname, weight, protemplate_id', 'required'),
			array('parent_id, weight, protemplate_id', 'numerical', 'integerOnly'=>true),
			array('categoryname, image', 'length', 'max'=>200),
			array('introduction', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, categoryname, weight, image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'products' => array(self::HAS_MANY, 'Product', 'procategory_id'),
                        'protemplate' => array(self::BELONGS_TO, 'Protemplate', 'protemplate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => '父分类',
			'categoryname' => '分类名称',
			'weight' => '排序权重',
			'image' => '导航图片',
			'protemplate_id' => '选用模板',
			'introduction' => '分类简介',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('categoryname',$this->categoryname,true);
		$criteria->compare('weight',$this->weight);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                         'pagination'=>array(
                             'pageSize'=>30,
                         ),
		));
	}
        public function getParents($categoryId=null,$result=null){
            if(is_null($categoryId)) $categoryId=$this->id;
            if($categoryId==0){
                return '根目录'.$result;
            }
            else{
                $pCate=$this->model()->findByPk($categoryId);
                $result=' >> '.$pCate->categoryname.$result;
                return $this->getParents($pCate->parent_id,$result);
            }
        }
        public function beforeDelete() {
            if(parent::beforeDelete()){
                if(count(Procategory::model()->findByAttributes(array('parent_id'=>$this->id)))>0){
                    Yii::app()->user->setFlash('deleteError','存在子分类，删除失败！');
                    return false;
                }
                else if(count(Product::model()->findByAttributes(array('procategory_id'=>$this->id)))>0){
                    Yii::app()->user->setFlash('deleteError','该分类下还有文章，删除失败！');
                    return false;
                }
                else
                    return true;
            }
            return false;
        }
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了分类 {".($this->categoryname)."}");
            else
                CFunc::writeLog("修改了分类 {".($this->categoryname)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了分类 {".($this->categoryname)."}");
            return true;
        }
        public function beforeSave() {
            if(parent::beforeSave()){
                if($this->id==$this->parent_id){
                    Yii::app()->user->setFlash('saveError','父分类不能是自己，无法保存分类！');
                    return false;
                }
                return true;
            }
            return false;
        }
}