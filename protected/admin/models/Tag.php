<?php

/**
 * This is the model class for table "{{tag}}".
 *
 * The followings are the available columns in table '{{tag}}':
 * @property integer $id
 * @property string $tagname
 * @property integer $searchtimes
 */
class Tag extends CBaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tag the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{tag}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tagname', 'required'),
			array('searchtimes', 'numerical', 'integerOnly'=>true),
			array('tagname', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tagname, searchtimes', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tagname' => '关键字',
			'searchtimes' => '搜索数',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tagname',$this->tagname,true);
		$criteria->compare('searchtimes',$this->searchtimes);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'searchtimes DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
        
        /*
         * addSearch 搜索数加一
         */
        public function addSearch(){
            try {
                Yii::app()->db->createCommand("update {{tag}} set searchtimes=searchtimes+1 where tagname='".$this->tagname."'")->query();
            } catch (Exception $exc) {
            }
        }
        
        public function beforeSave() {
		if(parent::beforeSave())
		{
			if($this->isNewRecord){
                                $this->searchtimes=0;
                        }
			return true;
		}
		else
			return false;
        }
        public function afterSave(){
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了关键字 {".($this->tagname)."}");
            else
                CFunc::writeLog("修改了关键字 {".($this->tagname)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了关键字 {".($this->tagname)."}");
            return true;
        }
}