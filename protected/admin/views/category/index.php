<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加栏目" rel="createCategory"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{category_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{category_id}'));?>" target="navTab" title="修改栏目" rel="updateCategory"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{category_id}'));?>" target="navTab" title="栏目详情" rel="viewCategory"><span>详情</span></a></li>
		</ul>
	</div>
        <table class="list" width="100%" layoutH="47">
            <thead>
                <tr>
                    <th align="center">栏目名称</th>
                    <th width="130" align="center">栏目类型</th>
                    <th width="80" align="center">排序权重</th>
                    <th width="80" align="center">导航显示</th>
                    <th width="200" align="center">采用模板</th>
                </tr>
            </thead>
            <tbody>
        <?php
        $categoryArr=CFunc::getSonTree();
        foreach ($categoryArr as $categoryItem){
            echo '<tr target="category_id" rel="'.$categoryItem['category']->id.'">';
            echo '<td align="left">'.CFunc::repeatStr('　　',$categoryItem['generation']-1).' '.$categoryItem['category']->categoryname.'</td>';
            echo '<td align="center">'.$categoryItem['category']->getKind().'</td>';
            echo '<td align="center">'.$categoryItem['category']->weight.'</td>';
            echo '<td align="center">'.($categoryItem['category']->joinnav=='1'?'√':'×').'</td>';
            if(isset($categoryItem['category']->template->title))
                $templateTitle=$categoryItem['category']->template->title;
            else
                $templateTitle="默认模板";
            echo '<td align="center">'.$templateTitle.'</td>';
            echo '</tr>';
        }
        ?>
            </tbody>
        </table>
</div>