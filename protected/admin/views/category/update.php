<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加栏目" rel="createCategory"><span>添加</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>$model->id));?>" target="navTab" title="栏目详情" rel="viewCategory"><span>详情</span></a></li>
		</ul>
	</div>
        <?php
        /*
         * 提供父栏目选项，先行整理栏目树形结构
         */
        $categoryArr=CFunc::getSonTree();

        //把树形用缩进显示出来
        $categoryIds['0']='根目录';
        foreach($categoryArr as $categoryItem){
            if($categoryItem['category']->kind==1)
                $categoryIds[$categoryItem['category']->id]=CFunc::repeatStr('┈┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname;
        }

        /*
         * 提供模板选项
         */
        $templateArr=Template::model()->findAll(array('order'=>'id desc'));
        $templateIds=array();
        $templateIds['0']='默认模板';
        foreach ($templateArr as $templateItem){
            $templateIds[$templateItem->id]=$templateItem->title;
        };

        ?>
        <?php $form=$this->beginWidget('ext.dwz.DwzActiveForm', array(
                'id'=>'category_update-form',
                'enableAjaxValidation'=>false,
                'htmlOptions'=>array(
                    'class'=>'pageForm required-validate',
                    'onsubmit'=>'return validateCallback(this, navTabAjaxDone);',
                ),
        )); ?>

                        <div class="pageFormContent nowrap" layoutH="83">
                                <dl>
                                        <dt><?php echo $form->label($model,'parent_id'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'parent_id',$categoryIds); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'categoryname'); ?></dt>
                                        <dd><?php echo $form->textField($model,'categoryname',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'kind'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'kind',array('1'=>'普通栏目','2'=>'单页栏目','3'=>'外部栏目'),array("class"=>"toggleSelector","rel"=>"outsideurl_container","showValue"=>"3")); ?></dd>
                                </dl>
                                <dl class="outsideurl_container">
                                        <dt><?php echo $form->label($model,'outsideurl'); ?></dt>
                                        <dd><?php echo $form->textField($model,'outsideurl',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'joinnav'); ?></dt>
                                        <dd><?php echo $form->checkBox($model,'joinnav'); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'weight'); ?></dt>
                                        <dd><?php echo $form->textField($model,'weight',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'image'); ?></dt>
                                        <dd><?php echo $form->textField($model,'image',array(
                                            'id'=>'create_category_image',
                                            'size'=>30,
                                            'maxlength'=>100,
                                            'class'=>'forswfupload',
                                            'flash_url'=>"/inc/swfupload/swfupload.swf",
                                            'flash9_url'=>"/inc/swfupload/swfupload_fp9.swf",
                                            'upload_url'=>$this->createUrl('upload/image'),
                                            'post_params'=>'{"PHPSESSID" : "'.session_id().'"}',
                                            'file_size_limit'=>"3 MB",
                                            'file_types'=>"*.jpg;*.gif;*.png;*.jpeg",
                                            'file_types_description'=>"图片",
                                            'custom_settings'=>'{
                                                    progressTarget : "create_category_fsUploadProgress",
                                                    imageField : "create_category_image",
                                                    imageViewField : "create_category_image_preview"
                                            }',
                                            'button_placeholder_id'=>"create_category_upload_holder",
                                            //'debug'=>'false',

                                            // Button settings
                                            //'button_image_url'=>"/images/SmallSpyGlassWithTransperancy_17x18.png",
                                            //'button_width'=>"140",
                                            //'button_height'=>"18",
                                            'button_text'=>'<span class="sud_btn">本地上传 <span class="sud_sbtn">(3 MB max)</span></span>',
                                            //'button_text_style'=>'.sud_btn { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .sud_sbtn { font-size: 10pt; }',
                                            //'button_text_top_padding'=>'0',
                                            //'button_text_left_padding'=>'18',
                                            'upload_success_handler'=>'uploadSuccessForImage',
                                        )); ?>
                                        <div class="swfuploadbtn"><span id="create_category_upload_holder"></span></div>
                                        <div class="fieldset flash" id="create_category_fsUploadProgress"></div>
                                        </dd>
                                </dl>
                                <dl id="create_category_image_preview">
                                        <?php if(!empty($model->image)) echo '<dt><label>图片预览</label></dt><dd>'.CHtml::link(CHtml::image(CFunc::getSmallImg($model->image),null,array('border'=>'0')),array('site/viewimg','imgsrc'=>CFunc::srcUrlEncode($model->image)),array('target'=>'dialog','title'=>'查看原图')).'</dd>'; ?>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'template_id'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'template_id',$templateIds); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'introduction'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'introduction',array(
                                            'rows'=>15,
                                            'cols'=>107,
                                            'class'=>'editor',
                                            'upLinkUrl'=>$this->createUrl('upload/link'),
                                            'upLinkExt'=>"zip,rar,txt",
                                            'upImgUrl'=>$this->createUrl('upload/image'),
                                            'upImgExt'=>"jpg,jpeg,gif,png",
                                            'upFlashUrl'=>$this->createUrl('upload/flash'),
                                            'upFlashExt'=>"swf",
                                            'upMediaUrl'=>$this->createUrl('upload/media'),
                                            'upMediaExt'=>"avi,flv,mp4,mp3",
                                            "id"=>"create_category_introduction"
                                            )); ?></dd>
                                </dl>
                        </div>
                        <div class="formBar">
                                <ul>
                                        <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                                        <li>
                                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                                        </li>
                                </ul>
                        </div>

        <?php $this->endWidget(); ?>
</div>