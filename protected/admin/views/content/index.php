<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    /*
                     * 提供父栏目选项，先行整理栏目树形结构
                     */
                    $categoryArr=CFunc::getSonTree();

                    //把树形用缩进显示出来
                    $categoryIds['']='全部';
                    foreach($categoryArr as $categoryItem){
                        $categoryIds[$categoryItem['category']->id]=CFunc::repeatStr('┈┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname;
                    }
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'id'); ?></td>
                                <td><?php echo $form->textField($model,'id'); ?></td>
        
                                <td><?php echo $form->label($model,'category_id'); ?></td>
                                <td><?php echo $form->dropDownList($model,'category_id',$categoryIds,array('class'=>'combox')); ?></td>

                                <td><?php echo $form->label($model,'title'); ?></td>
                                <td><?php echo $form->textField($model,'title'); ?></td>

                                <td><?php echo $form->label($model,'enable'); ?></td>
                                <td><?php echo $form->dropDownList($model,'enable',array(''=>'全部','1'=>'可用','0'=>'不可用'),array('class'=>'combox')); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');echo isset($model->category_id)?'?Content[category_id]='.$model->category_id:'';?>" target="navTab" title="添加资讯" rel="createContent"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{content_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{content_id}'));?>" target="navTab" title="修改资讯" rel="updateContent"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{content_id}'));?>" target="navTab" title="资讯详情" rel="viewContent"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'content-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'title',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'category_id',
                    'value'=>'$data->category->categoryname',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    //'filter' => $categoryIds,
                ),
                array(
                    'name'=>'weight',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'create_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'110',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'date("Y-m-d",$data->create_time)',
                    'filter'=>true,
                ),
                array(
                    'name'=>'lastedit_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'110',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'date("Y-m-d H:i",$data->lastedit_time)',
                    'filter'=>true,
                ),
                array(
                    'name'=>'enable',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->enable==1?"√":"×"',
                ),
	),
)); ?>
</div>