<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
                        <li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加资讯" rel="createContent"><span>添加</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>$model->id));?>" target="navTab" title="资讯详情" rel="viewContent"><span>详情</span></a></li>
		</ul>
	</div>
        <?php
        /*
         * 提供父栏目选项，先行整理栏目树形结构
         */
        $categoryArr=CFunc::getSonTree();

        //把树形用缩进显示出来
        $categoryIds['0']='-=请选择=-';
        foreach($categoryArr as $categoryItem){
            if($categoryItem['category']->kind==1)
                $categoryIds[$categoryItem['category']->id]=CFunc::repeatStr('┈┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname;
        }

        //改变必填项的标签样式
        CHtml::$beforeRequiredLabel="";
        CHtml::$afterRequiredLabel="";
        ?>
        <?php $form=$this->beginWidget('ext.dwz.DwzActiveForm', array(
                'id'=>'content_update-form',
                'enableAjaxValidation'=>false,
                'htmlOptions'=>array(
                    'class'=>'pageForm required-validate',
                    'onsubmit'=>'return validateCallback(this, navTabAjaxDone);',
                ),
        )); ?>

                        <div class="pageFormContent nowrap" layoutH="83">
                                <dl>
                                        <dt><?php echo $form->label($model,'category_id'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'category_id',$categoryIds); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'title'); ?></dt>
                                        <dd><?php echo $form->textField($model,'title',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'flagsArr'); ?></dt>
                                        <dd class="nolabelstyle"><?php echo $form->checkBoxList($model,'flagsArr',array('a'=>'头条','b'=>'幻灯','c'=>'推荐'),array('separator'=>'　')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'attachment'); ?></dt>
                                        <dd><?php echo $form->hiddenField($model,'attachment',array(
                                            'id'=>'update_content_attachment',
                                            'class'=>'forswfupload',
                                            'flash_url'=>"/inc/swfupload/swfupload.swf",
                                            'flash9_url'=>"/inc/swfupload/swfupload_fp9.swf",
                                            'upload_url'=>$this->createUrl('upload/attachment'),
                                            'post_params'=>'{"PHPSESSID" : "'.session_id().'"}',
                                            'file_size_limit'=>"3 MB",
                                            'file_types'=>"*.rar;*.zip;*.7z;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx",
                                            'file_types_description'=>"附件",
                                            'custom_settings'=>'{
                                                    progressTarget : "update_content_attachment_fsUploadProgress",
                                                    attachmentField : "update_content_attachment",
                                                    viewField : "update_content_attachmentlist"
                                            }',
                                            'button_placeholder_id'=>"update_content_attachment_upload_holder",
                                            //'debug'=>'false',

                                            // Button settings
                                            //'button_image_url'=>"/images/SmallSpyGlassWithTransperancy_17x18.png",
                                            //'button_width'=>"140",
                                            //'button_height'=>"18",
                                            'button_text'=>'<span class="sud_btn">本地上传 <span class="sud_sbtn">(3 MB max)</span></span>',
                                            //'button_text_style'=>'.sud_btn { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .sud_sbtn { font-size: 10pt; }',
                                            //'button_text_top_padding'=>'0',
                                            //'button_text_left_padding'=>'18',
                                            'upload_success_handler'=>'uploadSuccessForAttachment',
                                        )); ?>
                                        <div class="swfuploadbtn"><span id="update_content_attachment_upload_holder"></span></div>
                                        <div class="fieldset flash" id="update_content_attachment_fsUploadProgress"></div>
                                        </dd>
                                </dl>
                                <dl id="update_content_attachmentlist" class="attachmentlist">
                                        <?php
                                        if(!empty($model->attachment)){
                                            $attachmentArr=explode('|f|', $model->attachment);
                                            foreach ($attachmentArr as $attachmentItem){
                                                if(!strpos($attachmentItem,'|&|')) continue;
                                                $attachment=explode('|&|', $attachmentItem);
                                                echo '<li rel="update_content_attachment" alt="|f|'.$attachmentItem.'">'.CHtml::link($attachment[0],$attachment[1]).'<span class="delAttachment">×</span></li>';
                                            }
                                        }
                                        ?>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'image'); ?></dt>
                                        <dd><?php echo $form->textField($model,'image',array(
                                            'id'=>'update_content_image',
                                            'size'=>30,
                                            'maxlength'=>100,
                                            'class'=>'forswfupload',
                                            'flash_url'=>"/inc/swfupload/swfupload.swf",
                                            'flash9_url'=>"/inc/swfupload/swfupload_fp9.swf",
                                            'upload_url'=>$this->createUrl('upload/image'),
                                            'post_params'=>'{"PHPSESSID" : "'.session_id().'"}',
                                            'file_size_limit'=>"3 MB",
                                            'file_types'=>"*.jpg;*.gif;*.png;*.jpeg",
                                            'file_types_description'=>"图片",
                                            'custom_settings'=>'{
                                                    progressTarget : "update_content_fsUploadProgress",
                                                    imageField : "update_content_image",
                                                    imageViewField : "update_content_image_preview"
                                            }',
                                            'button_placeholder_id'=>"update_content_upload_holder",
                                            //'debug'=>'false',

                                            // Button settings
                                            //'button_image_url'=>"/images/SmallSpyGlassWithTransperancy_17x18.png",
                                            //'button_width'=>"140",
                                            //'button_height'=>"18",
                                            'button_text'=>'<span class="sud_btn">本地上传 <span class="sud_sbtn">(3 MB max)</span></span>',
                                            //'button_text_style'=>'.sud_btn { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .sud_sbtn { font-size: 10pt; }',
                                            //'button_text_top_padding'=>'0',
                                            //'button_text_left_padding'=>'18',
                                            'upload_success_handler'=>'uploadSuccessForImage',
                                        )); ?>
                                        <div class="swfuploadbtn"><span id="update_content_upload_holder"></span></div>
                                        <div class="fieldset flash" id="update_content_fsUploadProgress"></div>
                                        </dd>
                                </dl>
                                <dl id="update_content_image_preview">
                                        <?php if(!empty($model->image)) echo '<dt><label>图片预览</label></dt><dd>'.CHtml::link(CHtml::image(CFunc::getSmallImg($model->image),null,array('border'=>'0')),array('site/viewimg','imgsrc'=>CFunc::srcUrlEncode($model->image)),array('target'=>'dialog','title'=>'查看原图')).'</dd>'; ?>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'content'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'content',array(
                                            'rows'=>15,
                                            'cols'=>107,
                                            'class'=>'editor',
                                            'upLinkUrl'=>$this->createUrl('upload/link'),
                                            'upLinkExt'=>"zip,rar,txt",
                                            'upImgUrl'=>$this->createUrl('upload/image'),
                                            'upImgExt'=>"jpg,jpeg,gif,png",
                                            'upFlashUrl'=>$this->createUrl('upload/flash'),
                                            'upFlashExt'=>"swf",
                                            'upMediaUrl'=>$this->createUrl('upload/media'),
                                            'upMediaExt'=>"avi,flv,mp4,mp3",
                                            "id"=>"update_content_content"
                                            )); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'weight'); ?></dt>
                                        <dd><?php echo $form->textField($model,'weight',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'author'); ?></dt>
                                        <dd><?php echo $form->textField($model,'author',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'origin'); ?></dt>
                                        <dd><?php echo $form->textField($model,'origin',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'tags'); ?></dt>
                                        <dd><?php echo $form->textField($model,'tags',array('size'=>30,'maxlength'=>100)); ?> (留空将自动找到<font class="warning_red">标签库</font>中存在的词)</dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'summary'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'summary',array('rows'=>4,'cols'=>50)); ?> (留空将自动截取<font class="warning_red"><?php echo CFunc::getCustomparam('system_autosummary_count');?></font>字)</dd>
                                </dl>
                                <dl<?php echo (CFunc::getCustomparam('system_comment_on')=='1')?'':' class="justh"'; ?>>
                                        <dt><?php echo $form->label($model,'allowcomment'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'allowcomment',array('1'=>'允许','0'=>'不允许')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'create_time_str'); ?></dt>
                                        <dd><?php echo $form->textField($model,'create_time_str',array('size'=>30,'maxlength'=>100,'class'=>'date','[pattern'=>'yyyy-mm-dd'.']','[yearstart'=>'-80'.']','[yearend'=>'5'.']')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'enable'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'enable',array('1'=>'可用','0'=>'不可用')); ?></dd>
                                </dl>
                        </div>
                        <div class="formBar">
                                <ul>
                                        <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                                        <li>
                                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                                        </li>
                                </ul>
                        </div>

        <?php $this->endWidget(); ?>
</div>