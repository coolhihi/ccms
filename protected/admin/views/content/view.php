<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加资讯" rel="createContent"><span>添加</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>$model->id));?>" target="navTab" title="修改资讯" rel="viewContent"><span>修改</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'id'); ?></dt>
                    <dd><?php echo $model->id; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'weight'); ?></dt>
                    <dd><?php echo $model->weight; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'category_id'); ?></dt>
                    <dd><?php echo $model->category->getParents(); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'title'); ?></dt>
                    <dd><?php echo $model->title; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'author'); ?></dt>
                    <dd><?php echo $model->author; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'origin'); ?></dt>
                    <dd><?php echo $model->origin; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'attachment'); ?></dt>
                    <dd>
                    <?php
                    if($model->attachment=='')
                        echo '无附件';
                    else{
                        $attachmentList=explode('|f|',$model->attachment);
                        foreach($attachmentList as $attachmentItem){
                            if(!strpos($attachmentItem,'|&|')) continue;
                            $arrs=explode('|&|',$attachmentItem);
                            echo '<li><a href="'.$arrs[1].'">'.$arrs[0].'</a></li>';
                        }
                    }
                    ?>
                    </dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'image'); ?></dt>
                    <dd><?php echo $model->image==''?'无图':CHtml::link(CHtml::image(CFunc::getSmallImg($model->image),null,array('border'=>'0')),array('site/viewimg','imgsrc'=>CFunc::srcUrlEncode($model->image)),array('target'=>'dialog','title'=>'查看原图')); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'flags'); ?></dt>
                    <dd><?php echo $model->getFlags(); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'allowcomment'); ?></dt>
                    <dd><?php echo $model->allowcomment==1?'√':'×'; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'tags'); ?></dt>
                    <dd><?php echo $model->tags; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'summary'); ?></dt>
                    <dd><?php echo $model->summary; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'content'); ?></dt>
                    <dd><?php echo $model->content; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'clicktimes'); ?></dt>
                    <dd><?php echo $model->clicktimes; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'create_time'); ?></dt>
                    <dd><?php echo date('Y-m-d H:i:s',$model->create_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'lastedit_time'); ?></dt>
                    <dd><?php echo date('Y-m-d H:i:s',$model->lastedit_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'enable'); ?></dt>
                    <dd><?php echo $model->enable==1?'可用':'禁用'; ?></dd>
                </dl>
        </div>
</div>