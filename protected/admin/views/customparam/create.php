<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		</ul>
	</div>
        <?php $form=$this->beginWidget('ext.dwz.DwzActiveForm', array(
                'id'=>'customparam_create-form',
                'htmlOptions'=>array(
                    'class'=>'pageForm required-validate',
                    'onsubmit'=>'return validateCallback(this, navTabAjaxDone);',
                ),
        )); ?>

                        <div class="pageFormContent nowrap" layoutH="83">
                                <dl>
                                        <dt><?php echo $form->label($model,'paramtitle'); ?></dt>
                                        <dd><?php echo $form->textField($model,'paramtitle',array('size'=>30,'maxlength'=>100,'alt'=>'标题仅用于备注参数意义')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'paramname'); ?></dt>
                                        <dd><?php echo $form->textField($model,'paramname',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'paramvalue'); ?></dt>
                                        <dd><?php echo $form->textField($model,'paramvalue',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'enable'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'enable',array('1'=>'可用','0'=>'不可用')); ?></dd>
                                </dl>
                        </div>
                        <div class="formBar">
                                <ul>
                                        <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                                        <li>
                                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                                        </li>
                                </ul>
                        </div>

        <?php $this->endWidget(); ?>
</div>