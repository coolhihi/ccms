<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'paramtitle'); ?></td>
                                <td><?php echo $form->textField($model,'paramtitle'); ?></td>

                                <td><?php echo $form->label($model,'paramname'); ?></td>
                                <td><?php echo $form->textField($model,'paramname'); ?></td>

                                <td><?php echo $form->label($model,'paramvalue'); ?></td>
                                <td><?php echo $form->textField($model,'paramvalue'); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加参数" rel="createCustomparam"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{customparam_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{customparam_id}'));?>" target="navTab" title="修改参数" rel="updateCustomparam"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{customparam_id}'));?>" target="navTab" title="参数详情" rel="viewCustomparam"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'customparam-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'paramtitle',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                        'width'=>'250',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'paramname',
                    'headerHtmlOptions'=>array(
                        'width'=>'250',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'paramvalue',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'enable',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->enable==1?"√":"×"',
                ),
	),
)); ?>
</div>