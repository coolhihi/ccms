<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加参数" rel="createCustomparam"><span>添加</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>$model->id));?>" target="navTab" title="修改参数" rel="viewCustomparam"><span>修改</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'id'); ?></dt>
                    <dd><?php echo $model->id; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'paramname'); ?></dt>
                    <dd><?php echo $model->paramname; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'paramvalue'); ?></dt>
                    <dd><?php echo $model->paramvalue; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'paramtitle'); ?></dt>
                    <dd><?php echo $model->paramtitle; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'enable'); ?></dt>
                    <dd><?php echo $model->enable==1?'可用':'禁用'; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'systemparam'); ?></dt>
                    <dd><?php echo $model->systemparam==1?'是':'否' ?></dd>
                </dl>
        </div>
</div>