<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
		</ul>
	</div>
    <form id="surebackup-form" method="post" action="<?php echo Yii::app()->createUrl($this->route);?>" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
                <div class="pageFormContent nowrap" layoutH="83">
                    <dl>
                        <dd>* 为了更好地处理日志和统计数据，本功能不对管理员信息、管理日志和流量统计三模块的数据进行备份。</dd>
                    </dl>
                    <dl>
                        <dt><label for="backupremark">数据备注</label></dt>
                        <dd><?php echo CHtml::textField('Surebackup[backupremark]');?><?php echo CHtml::hiddenField('Surebackup[sure]','1');?></dd>
                    </dl>
                </div>
                <div class="formBar">
                    <ul>
                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">备份</button></div></div></li>
                        <li>
                            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                        </li>
                    </ul>
                </div>
    </form>
</div>