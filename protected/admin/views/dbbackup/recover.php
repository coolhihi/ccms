<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
		</ul>
	</div>
    <form id="recoverdb-form" method="post" action="<?php echo Yii::app()->createUrl($this->route);?>" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
                <div class="pageFormContent nowrap" layoutH="83">
                    <dl>
                        <dd>* 还原功能不会造成近期“管理员列表”、“管理日志”和“流量统计”数据的丢失，请放心。</dd>
                    </dl>
                    <dl>
                        <dt><label for="backupremark">备份记录</label></dt>
                        <dd><?php
                            $dbbackupArr=Dbbackup::model()->findAll(array('order'=>'backuptime desc'));
                            $dbbackupIds=array();
                            $dbbackupIds['']='请选择';
                            foreach ($dbbackupArr as $dbbackupItem){
                                $dbbackupIds[$dbbackupItem->id]=date('Y-m-d H:i',$dbbackupItem->backuptime);
                            }
                            echo CHtml::dropDownList('Recoverdb[id]','Recoverdb[id]', $dbbackupIds);
                        ?></dd>
                    </dl>
                    <dl>
                        <dt><label for="backupremark">还原确认</label></dt>
                        <dd><?php echo CHtml::dropDownList('Recoverdb[sure]','0', array('0'=>'否','1'=>'是'));?></dd>
                    </dl>
                </div>
                <div class="formBar">
                    <ul>
                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">还原</button></div></div></li>
                        <li>
                            <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                        </li>
                    </ul>
                </div>
    </form>
</div>