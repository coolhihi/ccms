<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
		</ul>
	</div>
        <?php
        /*
         * 提供管理员选项，先行读出管理员列表
         */
        $managerArr=Manager::model()->findAll();
        $managerIds=array();
        $managerIds['0']='全部';
        foreach ($managerArr as $managerItem){
            $managerIds[$managerItem->id]=$managerItem->username;
        }
        ?>
        <form id="logtodel-form" method="post" action="<?php echo Yii::app()->createUrl($this->route);?>" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
                    <div class="pageFormContent nowrap" layoutH="83">
                        <dl>
                            <dt><label for="Managerlog_manager_id">请选择管理员</label></dt>
                            <dd><?php echo CHtml::dropDownList('Logtodel[manager_id]','Logtodel[manager_id]', $managerIds);?></dd>
                        </dl>
                    </div>
                    <div class="formBar">
                        <ul>
                            <li><div class="buttonActive"><div class="buttonContent"><button type="submit">清空</button></div></div></li>
                            <li>
                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                            </li>
                        </ul>
                    </div>
        </form>
</div>