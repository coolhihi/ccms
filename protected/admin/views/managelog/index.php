<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    /*
                     * 提供管理员选项，先行读出管理员列表
                     */
                    $managerArr=Manager::model()->findAll();
                    $managerIds=array();
                    $managerIds['']='全部';
                    foreach ($managerArr as $managerItem){
                        $managerIds[$managerItem->id]=$managerItem->username;
                    }
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'manager.username'); ?></td>
                                <td><?php echo $form->dropDownList($model,'manager_id',$managerIds); ?></td>

                                <td><?php echo $form->label($model,'content'); ?></td>
                                <td><?php echo $form->textField($model,'content'); ?></td>

				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="view" href="<?php echo $this->createUrl('empty');?>" target="navTab" title="清空日志" rel="emptyManagelog"><span>清空</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'managelog-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'manager_id',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'header' => '管理员',
                    'value' => '$data->manager->username."(ID:".$data->manager_id.")"',
                ),
                array(
                    'name'=>'content',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'create_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'date("Y-m-d H:i",$data->create_time)',
                    'filter'=>true,
                ),
	),
)); ?>
</div>