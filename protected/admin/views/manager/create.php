<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
		</ul>
	</div>
        <?php $form=$this->beginWidget('ext.dwz.DwzActiveForm', array(
                'id'=>'manager_create-form',
                'htmlOptions'=>array(
                    'class'=>'pageForm required-validate',
                    'onsubmit'=>'return validateCallback(this, navTabAjaxDone);',
                ),
        )); ?>

                        <div class="pageFormContent nowrap" layoutH="83">
                                <dl>
                                        <dt><?php echo $form->label($model,'username'); ?></dt>
                                        <dd><?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'password'); ?></dt>
                                        <dd><?php echo $form->passwordField($model,'password',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'password2'); ?></dt>
                                        <dd><?php echo $form->passwordField($model,'password2',array('size'=>30,'maxlength'=>100,'equalTo'=>'#manager_create-form_Manager_password')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'realname'); ?></dt>
                                        <dd><?php echo $form->textField($model,'realname',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'manageright'); ?></dt>
                                        <dd class="nolabelstyle"><?php echo $form->checkBoxList($model,'managerightArray',array(
                                                CR_CHANGEPASSWORD=>'修改密码',
                                                CR_CONTENT=>'资讯管理',
                                                CR_CATEGORY=>'栏目管理',
                                                CR_TEMPLATE=>'模板管理',
                                                CR_PICTURE=>'幻灯图片管理',
                                                CR_PICCATEGORY=>'幻灯图库管理',
                                                CR_PRODUCT=>'产品管理',
                                                CR_PROCATEGORY=>'分类管理',
                                                CR_PROTEMPLATE=>'模板管理',
                                                CR_PARTNER=>'合作伙伴管理',
                                                CR_STATISTICS=>'流量监控',
                                                CR_MANAGER=>'管理员管理',
                                                CR_MANAGELOG=>'管理日志管理',
                                                CR_CUSTOMPARAM=>'参数管理',
                                                CR_DBBACKUP=>'数据库备份/还原',
                                                CR_TAG=>'标签管理',
                                            ),array('separator'=>'<br />','template'=>'{input} {label}')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'enable'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'enable',array('1'=>'可用','0'=>'不可用')); ?></dd>
                                </dl>
                        </div>
                        <div class="formBar">
                                <ul>
                                        <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                                        <li>
                                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                                        </li>
                                </ul>
                        </div>

        <?php $this->endWidget(); ?>
</div>