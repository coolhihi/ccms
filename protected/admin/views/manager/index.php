<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'id'); ?></td>
                                <td><?php echo $form->textField($model,'id'); ?></td>
                                
                                <td><?php echo $form->label($model,'username'); ?></td>
                                <td><?php echo $form->textField($model,'username'); ?></td>
                                
                                <td><?php echo $form->label($model,'realname'); ?></td>
                                <td><?php echo $form->textField($model,'realname'); ?></td>

                                <td><?php echo $form->label($model,'enable'); ?></td>
                                <td><?php echo $form->dropDownList($model,'enable',array(''=>'全部','1'=>'可用','0'=>'不可用'),array('class'=>'combox')); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加管理员" rel="createManager"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{manager_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{manager_id}'));?>" target="navTab" title="修改管理员" rel="updateManager"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{manager_id}'));?>" target="navTab" title="管理员详情" rel="viewManager"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'manager-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'username',
                    'headerHtmlOptions'=>array(
                        'width'=>'200',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'realname',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'lastlogin_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->lastlogin_time==$data->create_time?"无登陆记录":date("Y-m-d H:i",$data->lastlogin_time)',
                    'filter'=>true,
                ),
                array(
                    'name'=>'enable',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->enable==1?"√":"×"',
                ),
	),
)); ?>
</div>