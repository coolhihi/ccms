<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加管理员" rel="createManager"><span>添加</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>$model->id));?>" target="navTab" title="修改管理员" rel="viewManager"><span>修改</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'id'); ?></dt>
                    <dd><?php echo $model->id; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'username'); ?></dt>
                    <dd><?php echo $model->username; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'realname'); ?></dt>
                    <dd><?php echo $model->realname; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'create_time'); ?></dt>
                    <dd><?php echo date('Y-m-d H:i:s',$model->create_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'lastlogin_time'); ?></dt>
                    <dd><?php echo $model->lastlogin_time==$model->create_time?"无登陆记录":date("Y-m-d H:i",$model->lastlogin_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'enable'); ?></dt>
                    <dd><?php echo $model->enable==1?'可用':'禁用'; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'manageright'); ?></dt>
                    <dd class="managerrightstyle"><?php echo CFunc::displayRight($model->manageright); ?></dd>
                </dl>
        </div>
</div>