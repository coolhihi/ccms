<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'id'); ?></td>
                                <td><?php echo $form->textField($model,'id'); ?></td>

                                <td><?php echo $form->label($model,'title'); ?></td>
                                <td><?php echo $form->textField($model,'title'); ?></td>

                                <td><?php echo $form->label($model,'url'); ?></td>
                                <td><?php echo $form->textField($model,'url'); ?></td>

				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
                <?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加合作伙伴" rel="createPartner"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{partner_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{partner_id}'));?>" target="navTab" title="修改合作伙伴" rel="updatePartner"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{partner_id}'));?>" target="navTab" title="合作伙伴详情" rel="viewPartner"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'partner-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'title',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'url',
                    'headerHtmlOptions'=>array(
                        'width'=>'250',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'weight',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
	),
)); ?>
</div>