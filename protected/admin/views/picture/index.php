<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    /*
                     * 提供图库选项
                     */
                    $piccategoryArr=Piccategory::model()->findAll();
                    $piccategoryIds=array(''=>'全部');
                    foreach($piccategoryArr as $tempIds){
                        $piccategoryIds[$tempIds->id]=$tempIds->categoryname;
                    }

                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'id'); ?></td>
                                <td><?php echo $form->textField($model,'id'); ?></td>
        
                                <td><?php echo $form->label($model,'piccategory_id'); ?></td>
                                <td><?php echo $form->dropDownList($model,'piccategory_id',$piccategoryIds,array('class'=>'combox')); ?></td>

                                <td><?php echo $form->label($model,'title'); ?></td>
                                <td><?php echo $form->textField($model,'title'); ?></td>

                                <td><?php echo $form->label($model,'enable'); ?></td>
                                <td><?php echo $form->dropDownList($model,'enable',array(''=>'全部','1'=>'可用','0'=>'不可用'),array('class'=>'combox')); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');echo isset($model->piccategory_id)?'?Picture[piccategory_id]='.$model->piccategory_id:'';?>" target="navTab" title="添加图片" rel="createPicture"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{picture_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{picture_id}'));?>" target="navTab" title="修改图片" rel="updatePicture"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{picture_id}'));?>" target="navTab" title="图片详情" rel="viewPicture"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'picture-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'title',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'image',
                    'header'=>'图片预览',
                    'headerHtmlOptions'=>array(
                        'width'=>'210',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'CHtml::image(CFunc::getSmallImg($data->image))',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'piccategory_id',
                    'value'=>'$data->piccategory->categoryname',
                    'headerHtmlOptions'=>array(
                        'width'=>'150',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    //'filter' => $categoryIds,
                ),
                array(
                    'name'=>'weight',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'create_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'date("Y-m-d H:i",$data->create_time)',
                    'filter'=>true,
                ),
                array(
                    'name'=>'enable',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->enable==1?"√":"×"',
                ),
	),
)); ?>
</div>