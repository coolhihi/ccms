<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加分类" rel="createProcategory"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{procategory_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{procategory_id}'));?>" target="navTab" title="修改分类" rel="updateProcategory"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{procategory_id}'));?>" target="navTab" title="分类详情" rel="viewProcategory"><span>详情</span></a></li>
		</ul>
	</div>
        <table class="list" width="100%" layoutH="47">
            <thead>
                <tr>
                    <th align="center">分类名称</th>
                    <th width="80" align="center">排序权重</th>
                    <th width="200" align="center">采用模板</th>
                </tr>
            </thead>
            <tbody>
        <?php
        $procategoryArr=CFunc::getProSonTree();
        foreach ($procategoryArr as $procategoryItem){
            echo '<tr target="procategory_id" rel="'.$procategoryItem['category']->id.'">';
            echo '<td align="left">'.CFunc::repeatStr('　　',$procategoryItem['generation']-1).' '.$procategoryItem['category']->categoryname.'</td>';
            echo '<td align="center">'.$procategoryItem['category']->weight.'</td>';
            if(isset($procategoryItem['category']->protemplate->title))
                $templateTitle=$procategoryItem['category']->protemplate->title;
            else
                $templateTitle="默认模板";
            echo '<td align="center">'.$templateTitle.'</td>';
            echo '</tr>';
        }
        ?>
            </tbody>
        </table>
</div>