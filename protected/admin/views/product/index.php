<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    /*
                     * 提供父栏目选项，先行整理栏目树形结构
                     */
                    $procategoryArr=CFunc::getProSonTree();

                    //把树形用缩进显示出来
                    $procategoryIds['']='全部';
                    foreach($procategoryArr as $procategoryItem){
                        $procategoryIds[$procategoryItem['category']->id]=CFunc::repeatStr('┈┈',$procategoryItem['generation']-1).$procategoryItem['category']->categoryname;
                    }
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'id'); ?></td>
                                <td><?php echo $form->textField($model,'id'); ?></td>
        
                                <td><?php echo $form->label($model,'procategory_id'); ?></td>
                                <td><?php echo $form->dropDownList($model,'procategory_id',$procategoryIds,array('class'=>'combox')); ?></td>

                                <td><?php echo $form->label($model,'title'); ?></td>
                                <td><?php echo $form->textField($model,'title'); ?></td>

                                <td><?php echo $form->label($model,'enable'); ?></td>
                                <td><?php echo $form->dropDownList($model,'enable',array(''=>'全部','1'=>'可用','0'=>'不可用'),array('class'=>'combox')); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');echo isset($model->procategory_id)?'?Product[procategory_id]='.$model->procategory_id:'';?>" target="navTab" title="添加产品" rel="createProduct"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{product_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{product_id}'));?>" target="navTab" title="修改产品" rel="updateProduct"><span>修改</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>'{product_id}'));?>" target="navTab" title="产品详情" rel="viewProduct"><span>详情</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'product-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'title',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'procategory_id',
                    'value'=>'$data->procategory->categoryname',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    //'filter' => $categoryIds,
                ),
                array(
                    'name'=>'weight',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
                array(
                    'name'=>'create_time',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'date("Y-m-d H:i",$data->create_time)',
                    'filter'=>true,
                ),
                array(
                    'name'=>'enable',
                    'headerHtmlOptions'=>array(
                        'width'=>'70',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'value'=>'$data->enable==1?"√":"×"',
                ),
	),
)); ?>
</div>