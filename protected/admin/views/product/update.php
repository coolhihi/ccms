<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
                        <li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加产品" rel="createProduct"><span>添加</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('view',array('id'=>$model->id));?>" target="navTab" title="产品详情" rel="viewProduct"><span>详情</span></a></li>
		</ul>
	</div>
        <?php
        /*
         * 提供父栏目选项，先行整理栏目树形结构
         */
        $procategoryArr=CFunc::getProSonTree();

        //把树形用缩进显示出来
        $procategoryIds['0']='-=请选择=-';
        foreach($procategoryArr as $procategoryItem){
            $procategoryIds[$procategoryItem['category']->id]=CFunc::repeatStr('┈┈',$procategoryItem['generation']-1).$procategoryItem['category']->categoryname;
        }
        ?>
        <?php $form=$this->beginWidget('ext.dwz.DwzActiveForm', array(
                'id'=>'product_update-form',
                'htmlOptions'=>array(
                    'class'=>'pageForm required-validate',
                    'onsubmit'=>'return validateCallback(this, navTabAjaxDone);',
                ),
        )); ?>

                        <div class="pageFormContent nowrap" layoutH="83">
                                <dl>
                                        <dt><?php echo $form->label($model,'procategory_id'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'procategory_id',$procategoryIds); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'weight'); ?></dt>
                                        <dd><?php echo $form->textField($model,'weight',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'title'); ?></dt>
                                        <dd><?php echo $form->textField($model,'title',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'image'); ?></dt>
                                        <dd><?php echo $form->textField($model,'image',array(
                                            'id'=>'update_product_image',
                                            'size'=>30,
                                            'maxlength'=>100,
                                            'class'=>'forswfupload',
                                            'flash_url'=>"/inc/swfupload/swfupload.swf",
                                            'flash9_url'=>"/inc/swfupload/swfupload_fp9.swf",
                                            'upload_url'=>$this->createUrl('upload/image'),
                                            'post_params'=>'{"PHPSESSID" : "'.session_id().'"}',
                                            'file_size_limit'=>"3 MB",
                                            'file_types'=>"*.jpg;*.gif;*.png;*.jpeg",
                                            'file_types_description'=>"图片",
                                            'custom_settings'=>'{
                                                    progressTarget : "update_product_fsUploadProgress",
                                                    imageField : "update_product_image",
                                                    imageViewField : "update_product_image_preview"
                                            }',
                                            'button_placeholder_id'=>"update_product_upload_holder",
                                            //'debug'=>'false',

                                            // Button settings
                                            //'button_image_url'=>"/images/SmallSpyGlassWithTransperancy_17x18.png",
                                            //'button_width'=>"140",
                                            //'button_height'=>"18",
                                            'button_text'=>'<span class="sud_btn">本地上传 <span class="sud_sbtn">(3 MB max)</span></span>',
                                            //'button_text_style'=>'.sud_btn { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .sud_sbtn { font-size: 10pt; }',
                                            //'button_text_top_padding'=>'0',
                                            //'button_text_left_padding'=>'18',
                                            'upload_success_handler'=>'uploadSuccessForImage',
                                        )); ?>
                                        <div class="swfuploadbtn"><span id="update_product_upload_holder"></span></div>
                                        <div class="fieldset flash" id="update_product_fsUploadProgress"></div>
                                        </dd>
                                </dl>
                                <dl id="update_product_image_preview">
                                        <?php if(!empty($model->image)) echo '<dt><label>图片预览</label></dt><dd>'.CHtml::link(CHtml::image(CFunc::getSmallImg($model->image),null,array('border'=>'0')),array('site/viewimg','imgsrc'=>CFunc::srcUrlEncode($model->image)),array('target'=>'dialog','title'=>'查看原图')).'</dd>'; ?>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'flagsArr'); ?></dt>
                                        <dd class="nolabelstyle"><?php echo $form->checkBoxList($model,'flagsArr',array('a'=>'头条','b'=>'幻灯','c'=>'推荐'),array('separator'=>'　')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'content'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'content',array(
                                            'rows'=>15,
                                            'cols'=>107,
                                            'class'=>'editor',
                                            'upLinkUrl'=>$this->createUrl('upload/link'),
                                            'upLinkExt'=>"zip,rar,txt",
                                            'upImgUrl'=>$this->createUrl('upload/image'),
                                            'upImgExt'=>"jpg,jpeg,gif,png",
                                            'upFlashUrl'=>$this->createUrl('upload/flash'),
                                            'upFlashExt'=>"swf",
                                            'upMediaUrl'=>$this->createUrl('upload/media'),
                                            'upMediaExt'=>"avi,flv,mp4,mp3",
                                            "id"=>"update_product_content"
                                            )); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'tags'); ?></dt>
                                        <dd><?php echo $form->textField($model,'tags',array('size'=>30,'maxlength'=>100)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'enable'); ?></dt>
                                        <dd><?php echo $form->dropDownList($model,'enable',array('1'=>'可用','0'=>'不可用')); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'param1'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'param1',array('rows'=>4,'cols'=>50)); ?></dd>
                                </dl>
                                <dl>
                                        <dt><?php echo $form->label($model,'param2'); ?></dt>
                                        <dd><?php echo $form->textArea($model,'param2',array('rows'=>4,'cols'=>50)); ?></dd>
                                </dl>
                        </div>
                        <div class="formBar">
                                <ul>
                                        <!--<li><a class="buttonActive" href="javascript:;"><span>保存</span></a></li>-->
                                        <li><div class="buttonActive"><div class="buttonContent"><button type="submit">保存</button></div></div></li>
                                        <li>
                                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                                        </li>
                                </ul>
                        </div>

        <?php $this->endWidget(); ?>
</div>