<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加产品" rel="createProduct"><span>添加</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>$model->id));?>" target="navTab" title="修改产品" rel="viewProduct"><span>修改</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'id'); ?></dt>
                    <dd><?php echo $model->id; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'weight'); ?></dt>
                    <dd><?php echo $model->weight; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'procategory_id'); ?></dt>
                    <dd><?php echo $model->procategory->getParents(); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'title'); ?></dt>
                    <dd><?php echo $model->title; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'image'); ?></dt>
                    <dd><?php echo $model->image==''?'无图':CHtml::link(CHtml::image(CFunc::getSmallImg($model->image),null,array('border'=>'0')),array('site/viewimg','imgsrc'=>CFunc::srcUrlEncode($model->image)),array('target'=>'dialog','title'=>'查看原图')); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'flags'); ?></dt>
                    <dd><?php echo $model->getFlags(); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'tags'); ?></dt>
                    <dd><?php echo $model->tags; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'content'); ?></dt>
                    <dd><?php echo $model->content; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'param1'); ?></dt>
                    <dd><?php echo $model->param1; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'param2'); ?></dt>
                    <dd><?php echo $model->param2; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'clicktimes'); ?></dt>
                    <dd><?php echo $model->clicktimes; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'create_time'); ?></dt>
                    <dd><?php echo date('Y-m-d H:i:s',$model->create_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'lastedit_time'); ?></dt>
                    <dd><?php echo date('Y-m-d H:i:s',$model->lastedit_time); ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'enable'); ?></dt>
                    <dd><?php echo $model->enable==1?'可用':'禁用'; ?></dd>
                </dl>
        </div>
</div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
                array(
                    'name'=>'procategory_id',
                    'value'=>$model->procategory->getParents(),
                ),
		'weight',
		'title',
                array(
                    'name'=>'image',
                    'value'=>$model->image==''?'无图':CHtml::link(CHtml::image(CFunc::getSmallImg($model->image)),$model->image,array('target'=>'_blank','class'=>'smallpic')),
                    'type'=>'raw',
                ),
                array(
                    'name'=>'flags',
                    'value'=>$model->getFlags(),
                ),
		'tags',
		'content:raw',
		'param1',
		'param2',
		'clicktimes',
                array(
                    'name'=>'create_time',
                    'value'=>date('Y-m-d H:i:s',$model->create_time),
                ),
                array(
                    'name'=>'lastedit_time',
                    'value'=>date('Y-m-d H:i:s',$model->lastedit_time),
                ),
                array(
                    'name'=>'enable',
                    'value'=>$model->enable==1?'可用':'禁用',
                ),
	),
)); ?>
</div>