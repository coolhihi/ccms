<div class="pageContent">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>false,
        'htmlOptions'=>array(
            'class'=>'pageForm',
            'onsubmit'=>"return validateCallback(this, dialogAjaxDone)",
        ),
)); ?>
	<div class="pageFormContent" layoutH="58">
		<div class="unit">
			<label>用户名：</label>
			<input name="LoginForm[username]" type="text" class="required" size="16" />
		</div>
		<div class="unit">
			<label>密码：</label>
			<input name="LoginForm[password]" type="password" class="required" maxlength="16" />
		</div>
	</div>
	<div class="formBar">
		<ul>
			<li><div class="buttonActive"><div class="buttonContent"><button type="submit">提交</button></div></div></li>
			<li><div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div></li>
		</ul>
	</div>
<?php $this->endWidget(); ?>
	
</div>