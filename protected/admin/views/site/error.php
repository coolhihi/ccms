<?php
    $cs=Yii::app()->getClientScript();
    $cs->registerCssFile('/css/site/error.css');
?>
<div id="error_word">
<?php
switch ($code){
    case '404':
        echo '对不起，你访问的页面不存在。';
        break;
    default:
        echo '对不起，你访问的页面出错了。';
}
?>
    <a href="#" onclick="javascript:history.go(-1);return false;">返回上一页</a>
    <!--<?php echo CHtml::encode($message); ?>-->
</div>