<?php $this->widget('ext.dwz.DwzWidget'); ?>
    <div id="layout">
        <div id="header">
            <div class="headerNav">
                <a class="logo" href="/admin.php">标志</a>
                <ul class="nav">
                    <li><?php echo Yii::app()->user->managername; ?>, 您好，欢迎登陆CCMS网站管理系统</li>
                    <li><a href="<?php echo $this->createUrl("site/flushcache");?>" target="ajaxTodo" rel="flushcache" title="清理缓存">清理缓存</a></li>
                    <li><a href="<?php echo $this->createUrl("manager/changepswd");?>" target="navTab" rel="changePassword" title="修改密码">修改密码</a></li>
                    <li><a href="<?php echo $this->createUrl("site/logout");?>">退出</a></li>
		</ul>
		<ul class="themeList" id="themeList">
                    <li theme="default"><div class="selected">蓝色</div></li>
                    <li theme="green"><div>绿色</div></li>
                    <li theme="silver"><div>银色</div></li>
		</ul>
            </div>
            <!-- navMenu -->
	</div>
	<div id="leftside">
            <div id="sidebar_s">
                <div class="collapse">
                    <div class="toggleCollapse"><div></div></div>
		</div>
            </div>
            <div id="sidebar">
                <div class="toggleCollapse"><h2>管理菜单</h2><div>收缩</div></div>
                <div class="accordion" fillSpace="sidebar">
                    <?php if(CFunc::checkRight(CR_CONTENT)||CFunc::checkRight(CR_CATEGORY)||CFunc::checkRight(CR_PICCATEGORY)||CFunc::checkRight(CR_PICTURE)||CFunc::checkRight(CR_PROCATEGORY)||CFunc::checkRight(CR_PRODUCT)): ?>
                    <div class="accordionHeader">
                        <h2><span>Folder</span>快速管理</h2>
                    </div>
                    <div class="accordionContent">
			<ul class="tree treeFolder">
                            <?php
                                $allCates=Category::model()->findAll();
                                $allParents=array();
                                foreach($allCates as $cate){
                                    if($cate->parent_id!=0)
                                        $allParents[]=$cate->parent_id;
                                }
                                $categoryArr=CFunc::getSonTree();
                            ?>
                            <?php if(CFunc::checkRight(CR_CONTENT) || CFunc::checkRight(CR_CATEGORY)): ?>
                            <li>
                                <a>资料管理</a>
                                <ul>
                                    <?php
                                    foreach($categoryArr as $categoryItem){
                                        if(in_array($categoryItem['category']->id,$allParents))
                                            echo '<li><a>'.CFunc::repeatStr('┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname.'</a></li>';
                                        else if($categoryItem['category']->kind==1)
                                            echo '<li><a href="'.$this->createUrl("content/index").'?Content[category_id]='.$categoryItem['category']->id.'" target="navTab" rel="content" title="'.$categoryItem['category']->categoryname.'">'.CFunc::repeatStr('┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname.'</a></li>';
                                        else if($categoryItem['category']->kind==2)
                                            echo '<li><a href="'.$this->createUrl("category/update",array('id'=>$categoryItem['category']->id)).'" target="navTab" rel="updateCategory" title="'.$categoryItem['category']->categoryname.'">'.CFunc::repeatStr('┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname.'</a></li>';
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php
                                $allProCates=Procategory::model()->findAll();
                                $allProParents=array();
                                foreach($allProCates as $procate){
                                    if($procate->parent_id!=0)
                                        $allProParents[]=$procate->parent_id;
                                }
                                $procategoryArr=CFunc::getProSonTree();
                            ?>
                            <?php if(CFunc::checkRight(CR_PRODUCT) || CFunc::checkRight(CR_PROCATEGORY)): ?>
                            <li>
                                <a>产品管理</a>
                                <ul>
                                    <?php
                                    foreach($procategoryArr as $procategoryItem){
                                        if(in_array($procategoryItem['category']->id,$allProParents))
                                            echo '<li><a>'.CFunc::repeatStr('┈',$procategoryItem['generation']-1).$procategoryItem['category']->categoryname.'</a></li>';
                                        else
                                            echo '<li><a href="'.$this->createUrl("product/index").'?Product[procategory_id]='.$procategoryItem['category']->id.'" target="navTab" rel="product" title="'.$procategoryItem['category']->categoryname.'">'.CFunc::repeatStr('┈',$procategoryItem['generation']-1).$procategoryItem['category']->categoryname.'</a></li>';
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_PICTURE)): ?>
                            <?php
                            $piccategoryArr=Piccategory::model()->findAll(array('order'=>'weight desc,id asc'));
                            ?>
                            <li>
                                <a>幻灯图管理</a>
                                <ul>
                                    <?php
                                    foreach($piccategoryArr as $piccategoryItem){
                                        echo '<li><a href="'.$this->createUrl("picture/index").'?Picture[piccategory_id]='.$piccategoryItem->id.'" target="navTab" rel="Picture" title="'.$piccategoryItem->categoryname.'">'.$piccategoryItem->categoryname.'</a></li>';
                                    }
                                    ?>
                                </ul>
                            </li>
                            <?php endif; ?>
			</ul>
                    </div>
                    <?php endif; ?>
                    <?php if(CFunc::checkRight(CR_CONTENT)||CFunc::checkRight(CR_CATEGORY)||CFunc::checkRight(CR_TEMPLATE)||CFunc::checkRight(CR_PICCATEGORY)||CFunc::checkRight(CR_PICTURE)): ?>
                    <div class="accordionHeader">
                        <h2><span>Folder</span>图文系统</h2>
                    </div>
                    <div class="accordionContent">
			<ul class="tree treeFolder">
                            <?php if(CFunc::checkRight(CR_CONTENT)): ?>
                            <li>
                                <a>资讯管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("content/index");?>" target="navTab" rel="content" title="资讯列表">资讯列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("content/create");?>" target="navTab" rel="createContent" title="添加资讯">添加资讯</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_CATEGORY)): ?>
                            <li>
                                <a>栏目管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("category/index");?>" target="navTab" rel="category" title="栏目列表">栏目列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("category/create");?>" target="navTab" rel="createCategory" title="添加栏目">添加栏目</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_TEMPLATE)): ?>
                            <li>
                                <a>模板管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("template/index");?>" target="navTab" rel="template" title="资讯模板列表">模板列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("template/create");?>" target="navTab" rel="createTemplate" title="添加资讯模板">添加模板</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_PICTURE)): ?>
                            <li>
                                <a>图片管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("picture/index");?>" target="navTab" rel="picture" title="图片列表">图片列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("picture/create");?>" target="navTab" rel="createPicture" title="添加图片">添加图片</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_PICCATEGORY)): ?>
                            <li>
                                <a>图库管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("piccategory/index");?>" target="navTab" rel="piccategory" title="图库列表">图库列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("piccategory/create");?>" target="navTab" rel="createPiccategory" title="添加图库">添加图库</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
			</ul>
                    </div>
                    <?php endif; ?>
                    <?php if(CFunc::checkRight(CR_PROCATEGORY)||CFunc::checkRight(CR_PRODUCT)||CFunc::checkRight(CR_PROTEMPLATE)): ?>
                    <div class="accordionHeader">
			<h2><span>Folder</span>产品系统</h2>
                    </div>
                    <div class="accordionContent">
                        <ul class="tree treeFolder">
                            <?php if(CFunc::checkRight(CR_PRODUCT)): ?>
                            <li>
                                <a>产品管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("product/index");?>" target="navTab" rel="product" title="产品列表">产品列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("product/create");?>" target="navTab" rel="createProduct" title="产品资讯">添加产品</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_PROCATEGORY)): ?>
                            <li>
                                <a>分类管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("procategory/index");?>" target="navTab" rel="procategory" title="分类列表">分类列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("procategory/create");?>" target="navTab" rel="createProcategory" title="添加分类">添加分类</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_PROTEMPLATE)): ?>
                            <li>
                                <a>模板管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("protemplate/index");?>" target="navTab" rel="protemplate" title="产品模板列表">模板列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("protemplate/create");?>" target="navTab" rel="createProtemplate" title="添加产品模板">添加模板</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
			</ul>
                    </div>
                    <?php endif; ?>
                    <?php if(CFunc::checkRight(CR_PARTNER)||CFunc::checkRight(CR_STATISTICS)): ?>
                    <div class="accordionHeader">
			<h2><span>Folder</span>其他功能</h2>
                    </div>
                    <div class="accordionContent">
                        <ul class="tree treeFolder">
                            <?php if(CFunc::checkRight(CR_PARTNER)): ?>
                            <li>
                                <a>合作伙伴管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("partner/index");?>" target="navTab" rel="partner" title="合作伙伴列表">合作伙伴列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("partner/create");?>" target="navTab" rel="createPartner" title="添加合作伙伴">添加合作伙伴</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_STATISTICS)): ?>
                            <li>
                                <a>流量监控</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("statistics/index");?>" target="navTab" rel="statistics" title="流量监控">日流量</a></li>
                                    <li><a href="<?php echo $this->createUrl("statistics/bymonth");?>" target="navTab" rel="statistics" title="流量监控">月流量</a></li>
                                    <li><a href="<?php echo $this->createUrl("statistics/byyear");?>" target="navTab" rel="statistics" title="流量监控">年流量</a></li>
                                    <li><a href="<?php echo $this->createUrl("statistics/empty");?>" target="navTab" rel="emptyStatistics" title="流量清零">清零</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
			</ul>
                    </div>
                    <?php endif; ?>
                    <?php if(CFunc::checkRight(CR_MANAGER)||CFunc::checkRight(CR_MANAGELOG)||CFunc::checkRight(CR_CUSTOMPARAM)||CFunc::checkRight(CR_DBBACKUP)): ?>
                    <div class="accordionHeader">
			<h2><span>Folder</span>系统设置</h2>
                    </div>
                    <div class="accordionContent">
                        <ul class="tree treeFolder">
                            <?php if(CFunc::checkRight(CR_MANAGER)): ?>
                            <li>
                                <a>管理员管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("manager/index");?>" target="navTab" rel="manager" title="管理员列表">管理员列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("manager/create");?>" target="navTab" rel="createManager" title="添加管理员">添加管理员</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_MANAGELOG)): ?>
                            <li>
                                <a>管理日志</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("managelog/index");?>" target="navTab" rel="managelog" title="日志列表">日志列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("managelog/empty");?>" target="navTab" rel="emptyManagelog" title="清空日志">清空日志</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_CUSTOMPARAM)): ?>
                            <li>
                                <a>参数设置</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("customparam/index");?>" target="navTab" rel="customparam" title="参数列表">参数列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("customparam/create");?>" target="navTab" rel="createCustomparam" title="添加参数">添加参数</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_DBBACKUP)): ?>
                            <li>
                                <a>数据库备份</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("dbbackup/index");?>" target="navTab" rel="dbbackup" title="备份记录">备份记录</a></li>
                                    <li><a href="<?php echo $this->createUrl("dbbackup/backup");?>" target="navTab" rel="backupDbbackup" title="备份数据">备份数据</a></li>
                                    <li><a href="<?php echo $this->createUrl("dbbackup/recover");?>" target="navTab" rel="recoverDbbackup" title="还原数据">还原数据</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(CFunc::checkRight(CR_TAG)): ?>
                            <li>
                                <a>标签管理</a>
                                <ul>
                                    <li><a href="<?php echo $this->createUrl("tag/index");?>" target="navTab" rel="tag" title="标签列表">标签列表</a></li>
                                    <li><a href="<?php echo $this->createUrl("tag/create");?>" target="navTab" rel="createTag" title="添加标签">添加标签</a></li>
                                </ul>
                            </li>
                            <?php endif; ?>
			</ul>
                    </div>
                    <?php endif; ?>
		</div>
            </div>
	</div>
	<div id="container">
            <div id="navTab" class="tabsPage">
                <div class="tabsPageHeader">
                    <div class="tabsPageHeaderContent"><!-- 显示左右控制时添加 class="tabsPageHeaderMargin" -->
                        <ul class="navTab-tab">
                            <li tabid="main" class="main"><a href="javascript:;"><span><span class="home_icon">系统主页</span></span></a></li>
                        </ul>
                    </div>
                    <div class="tabsLeft">left</div><!-- 禁用只需要添加一个样式 class="tabsLeft tabsLeftDisabled" -->
                    <div class="tabsRight">right</div><!-- 禁用只需要添加一个样式 class="tabsRight tabsRightDisabled" -->
                    <div class="tabsMore">more</div>
                </div>
                <ul class="tabsMoreList">
                    <li><a href="javascript:;">系统主页</a></li>
                </ul>
                <div class="navTab-panel tabsPageContent layoutBox">
                    <div class="page unitBox">
                        <div class="accountInfo">
                            <div class="alertInfo">
                                <h2>上次登陆时间</h2>
                                <?php echo date('Y-m-d H:i',Yii::app()->user->lastlogin_time); ?> 　若未尝登陆，请及时
                                <a href="<?php echo $this->createUrl('manager/changepswd'); ?>" target="navTab" rel="changePassword">修改密码</a>
                            </div>
                            <p><span>快速导航</span></p>
                            <p class="c_fastnav">
                                <a href="<?php echo $this->createUrl('content/create'); ?>" target="navTab" rel="createContent">添加资讯</a> 
                                <a href="<?php echo $this->createUrl('product/create'); ?>" target="navTab" rel="createProduct">添加产品</a> 
                                <a href="<?php echo $this->createUrl('partner/create'); ?>" target="navTab" rel="createPartner">添加合作伙伴</a> 
                                <a href="<?php echo $this->createUrl('managelog/index'); ?>" target="navTab" rel="managelog">管理日志</a> 
                                <a href="<?php echo $this->createUrl('statistics/index'); ?>" target="navTab" rel="statistics">今日流量</a> 
                                <a href="<?php echo $this->createUrl('dbbackup/backup'); ?>" target="navTab" rel="backupDbbackup">备份数据库</a> 
                                <a href="<?php echo $this->createUrl('customparam/update',array('id'=>10)); ?>" target="navTab" rel="updateCustomparam">更改缓存时间</a>
                            </p>
                        </div>
                        <?php
                        //栏目数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_category`");
                        $categoryCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //文章数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_content`");
                        $contentCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //图库数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_piccategory`");
                        $piccategoryCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //图片数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_picture`");
                        $pictureCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //产品分类数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_procategory`");
                        $procategoryCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //产品数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_product`");
                        $productCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //合作伙伴数
                        $dbCommand = Yii::app()->db->createCommand("SELECT COUNT(*) as count FROM `ccms_partner`");
                        $partnerCount = $dbCommand->queryAll();
                        unset($dbCommand);
                        //最新备份记录
                        $dbCommand = Yii::app()->db->createCommand("SELECT * FROM `ccms_dbbackup` order by backuptime desc limit 1");
                        $newestDbbackup = $dbCommand->queryAll();
                        $dbbackupRemark="没有备份";
                        if(count($newestDbbackup)==1) $dbbackupRemark=$newestDbbackup[0]["backupremark"].'（'.date('Y年m月d日',$newestDbbackup[0]["backuptime"]).'）';
                        unset($dbCommand);
                        ?>
                        <div class="pageFormContent c_container" layoutH="80"<?php //style="margin-right:230px"?>>
                            <div class="c_title">网站数据信息：</div>
                            <dl>
                                <dt>资讯栏目数：</dt>
                                <dd><?php echo $categoryCount[0]['count']; ?></dd>
                            </dl>
                            <dl>
                                <dt>资讯总数：</dt>
                                <dd><?php echo $contentCount[0]['count']; ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>幻灯图库数：</dt>
                                <dd><?php echo $piccategoryCount[0]['count']; ?></dd>
                            </dl>
                            <dl>
                                <dt>幻灯图片数：</dt>
                                <dd><?php echo $pictureCount[0]['count']; ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>产品分类数：</dt>
                                <dd><?php echo $procategoryCount[0]['count']; ?></dd>
                            </dl>
                            <dl>
                                <dt>产品总数：</dt>
                                <dd><?php echo $productCount[0]['count']; ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>合作伙伴数：</dt>
                                <dd><?php echo $partnerCount[0]['count']; ?></dd>
                            </dl>
                            <dl>
                                <dt>最新备份记录：</dt>
                                <dd><?php echo $dbbackupRemark; ?></dd>
                            </dl>

                            <div class="divider"></div>
                            <div class="c_title">服务器信息：</div>
                            <dl class="clearLeft">
                                <dt>服务器IP：</dt>
                                <dd><?php echo GetHostByName($_SERVER['SERVER_NAME']); ?></dd>
                            </dl>
                            <dl>
                                <dt>web服务端口：</dt>
                                <dd><?php echo($_SERVER["SERVER_PORT"]); ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>PHP版本：</dt>
                                <dd><?php echo PHP_VERSION; ?></dd>
                            </dl>
                            <dl>
                                <dt>PHP运行方式：</dt>
                                <dd><?PHP echo php_sapi_name(); ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>操作系统：</dt>
                                <dd><?php echo PHP_OS; ?></dd>
                            </dl>
                            <dl>
                                <dt>物理位置：</dt>
                                <dd><?php echo getcwd(); ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>上传文件限制：</dt>
                                <dd><?php echo ini_get('upload_max_filesize'); ?></dd>
                            </dl>
                            <dl>
                                <dt>文件上传：</dt>
                                <dd><?php echo is_writable('./'.UPLOADIMAGE_PATH.'')?'允许':'<font class="warning_red">禁止 （无法实现文件上传功能）</font>';?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>数据库备份：</dt>
                                <dd><?php echo is_writable('./'.DBBACKUP_PATH.'')?'允许':'<font class="warning_red">禁止 （无法实现数据库备份功能）</font>';?></dd>
                            </dl>
                            <dl>
                                <dt>支持OpenSSL：</dt>
                                <dd><?php echo function_exists('openssl_sign')?'是':'<font class="warning_red">否 (留言无法自动转至邮箱)</font>'; ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>GD库版本：</dt>
                                <dd><?php $tempgd=gd_info();echo $tempgd['GD Version'];unset($tempgd);?></dd>
                            </dl>

                            <div class="divider"></div>
                            <div class="c_title">客户端信息：</div>
                            <dl class="clearLeft">
                                <dt>客户端IP：</dt>
                                <dd><?php echo CFunc::getIP(); ?></dd>
                            </dl>
                            <dl>
                                <dt>连接端口：</dt>
                                <dd><?php echo $_SERVER['REMOTE_PORT']; ?></dd>
                            </dl>
                            <dl class="clearLeft">
                                <dt>操作系统：</dt>
                                <dd><?php echo CFunc::getOS(); ?></dd>
                            </dl>
                            <dl>
                                <dt>浏览器内核：</dt>
                                <dd><?php echo CFunc::getBrowse(); ?></dd>
                            </dl>
                        </div>
                        <?php
                        /*
                        <div style="width:230px;position: absolute;top:60px;right:0" layoutH="80">
                            <iframe width="100%" height="430" class="share_self"  frameborder="0" scrolling="no" src="http://widget.weibo.com/weiboshow/index.php?width=0&height=430&fansRow=2&ptype=1&skin=1&isTitle=0&noborder=1&isWeibo=1&isFans=0&uid=1739071261&verifier=c683dfe7"></iframe>
                        </div>
                        */
                        ?>
                    </div>
                </div>
            </div>
	</div>
    </div>
    <div id="footer">Copyright &copy; 2012 Author:<a href="http://www.gxxsite.com/case/ccms/aboutus.html" target="navTab" title="关于作者">COoL</a>.</div>