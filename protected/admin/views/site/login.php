<?php
    $cs=Yii::app()->getClientScript();
    $cs->registerCssFile('/css/loginform.css');
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
<div class="loginform">
	<div class="loginform_bg">
            <div class="loginform_logo"></div>
            <div>
                <table width="0">
                    <tr>
                        <td width="130">&nbsp;</td>
                        <td><?php if($model->username!=""):?><span class="msg_ico">对不起，您输入的账号或密码不正确！</span><?php endif; ?></td>
                    </tr>
                    <tr>
                        <td><strong>用户名：</strong></td>
                        <td align="right"><input name="LoginForm[username]" id="LoginForm_username" type="text" class="loginform_txt" size="16" /></td>
                    </tr>
                    <tr>
                        <td><strong>密&nbsp;&nbsp;&nbsp;&nbsp;码：</strong></td>
                        <td align="right"><input name="LoginForm[password]" id="LoginForm_password" type="password" class="loginform_txt" maxlength="16" /></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td align="right"><input name="" type="submit" class="loginform_btn" value=" " />　</td>
                    </tr>
                </table>
            </div>
    </div>
</div>
<?php $this->endWidget(); ?>