<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('index');?>" target="navTab" rel="statistics" title="流量监控"><span>日流量</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('bymonth');?>" target="navTab" rel="statistics" title="流量监控"><span>月流量</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('byyear');?>" target="navTab" rel="statistics" title="流量监控"><span>年流量</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('empty');?>" target="navTab" title="流量清零" rel="emptyStatistics"><span>清零</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
            <div class="flash_chart">
                <?php
                    //获取日期
                    $where='to_days(from_unixtime(visit_time))=to_days(curdate())';

                    //从数据库统计数量
                    $pvbyhour=Yii::app()->db->createCommand("select temp.hour as hour,ifnull(stat.pv,0) as count from (select 0 hour union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9 union all select 10 union all select 11 union all select 12 union all select 13 union all select 14 union all select 15 union all select 16 union all select 17 union all select 18 union all select 19 union all select 20 union all select 21 union all select 22 union all select 23) as temp left join (select count(id) as pv,hour(from_unixtime(visit_time)) as hour from ccms_statistics where ".$where." group by hour(from_unixtime(visit_time)) order by hour) as stat on temp.hour=stat.hour")->queryAll();
                    $ipbyhour=Yii::app()->db->createCommand("select temp.hour as hour,ifnull(stat.ip,0) as count from (select 0 hour union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9 union all select 10 union all select 11 union all select 12 union all select 13 union all select 14 union all select 15 union all select 16 union all select 17 union all select 18 union all select 19 union all select 20 union all select 21 union all select 22 union all select 23) as temp left join (select count(distinct ipaddress) as ip,hour(from_unixtime(visit_time)) as hour from ccms_statistics where ".$where." group by hour(from_unixtime(visit_time)) order by hour) as stat on temp.hour=stat.hour")->queryAll();
                    $uvbyhour=Yii::app()->db->createCommand("select temp.hour as hour,ifnull(stat.uv,0) as count from (select 0 hour union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9 union all select 10 union all select 11 union all select 12 union all select 13 union all select 14 union all select 15 union all select 16 union all select 17 union all select 18 union all select 19 union all select 20 union all select 21 union all select 22 union all select 23) as temp left join (select count(id) as uv,hour(from_unixtime(visit_time)) as hour from ccms_statistics where isuv=1 and ".$where." group by hour(from_unixtime(visit_time)) order by hour) as stat on temp.hour=stat.hour")->queryAll();
                    //$ipmodel=Yii::app()->db->createCommand("select count(distinct ipaddress) as ip from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())")->queryAll();
                    //$uvmodel=Yii::app()->db->createCommand("select count(id) as uv from ccms_statistics where isuv=1 and to_days(from_unixtime(visit_time))=to_days(curdate())")->queryAll();

                    //把数据转成数组形式
                    $pvforchart=array();
                    $ipforchart=array();
                    $uvforchart=array();
                    for ($key=0;$key<24;$key++){
                        $pvforchart[]=$pvbyhour[$key]['count'];
                        $ipforchart[]=$ipbyhour[$key]['count'];
                        $uvforchart[]=$uvbyhour[$key]['count'];
                    }
                    $pvmax=max($pvforchart);
                    $pvmax=$pvmax>9?$pvmax:9;
                    $pvstep=ceil($pvmax/10);

                    $flashChart = new EOFC2;
                    // Minimum usage. You will always need at least this.
                    $flashChart->begin();
                    $flashChart->axis('x',array('range' => array(0,24,1),'labels'=>array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24')));
                    $flashChart->axis('y',array('range' => array(0,$pvmax+$pvstep,$pvstep))); //以pv来定义y轴结构
                    $flashChart->setToolTip('#key#:#val#');

                    //$flashChart->setTitle('24小时分布图','font-size:14px; color: #222222;');
                    $flashChart->setLegend('x','今日分时流量', '{color:#1835ff;font-size:12px;}');
                    $flashChart->setLegend('y','count', '{color:#1835ff;font-size:12px;}');

                    //定义数据来源
                    $flashChart->setData($pvforchart,'{n}',false,'pv');
                    $flashChart->setData($ipforchart,'{n}',false,'ip');
                    $flashChart->setData($uvforchart,'{n}',false,'uv');
                    //$flashChart->setData(array(3,4,6,9,120),'{n}',false,'Oranges');
                    $flashChart->renderData('line_dot',array('colour'=>'#cc0000','key'=>array('PV (展示)','12')),'pv');
                    $flashChart->renderData('line_dot',array('colour'=>'#0000dd','key'=>array('IP','12')),'ip');
                    $flashChart->renderData('line_dot',array('colour'=>'#009900','key'=>array('UV (独立访客)','12')),'uv');
                    //$flashChart->renderData('line',array('colour'=>'#0000ff'),'Oranges');
                    $flashChart->render('100%','300');
                ?>
            </div>
            <div class="stat_total">
                <?php
                    //今日总流量统计
                    $todaypvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())")->queryRow();
                    $todayipmodel=Yii::app()->db->createCommand("select count(distinct ipaddress) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())")->queryRow();
                    $todayuvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where isuv=1 and to_days(from_unixtime(visit_time))=to_days(curdate())")->queryRow();
                    //var_dump($model);

                    //昨日总流量统计
                    $yesterdaypvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())-1")->queryRow();
                    $yesterdayipmodel=Yii::app()->db->createCommand("select count(distinct ipaddress) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())-1")->queryRow();
                    $yesterdayuvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where isuv=1 and to_days(from_unixtime(visit_time))=to_days(curdate())-1")->queryRow();

                    //前日总流量统计
                    $beforepvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())-2")->queryRow();
                    $beforeipmodel=Yii::app()->db->createCommand("select count(distinct ipaddress) as value from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate())-2")->queryRow();
                    $beforeuvmodel=Yii::app()->db->createCommand("select count(id) as value from ccms_statistics where isuv=1 and to_days(from_unixtime(visit_time))=to_days(curdate())-2")->queryRow();

                ?>
                <div class="stat_tb">
                    <div class="stat_tb_title">今日统计</div>
                    <div class="stat_tb_item">PV: <?php echo $todaypvmodel["value"]; ?></div>
                    <div class="stat_tb_item">IP: <?php echo $todayipmodel["value"]; ?></div>
                    <div class="stat_tb_item">UV: <?php echo $todayuvmodel["value"]; ?></div>
                </div>
                <div class="stat_tb">
                    <div class="stat_tb_title">昨日统计</div>
                    <div class="stat_tb_item">PV: <?php echo $yesterdaypvmodel["value"]; ?></div>
                    <div class="stat_tb_item">IP: <?php echo $yesterdayipmodel["value"]; ?></div>
                    <div class="stat_tb_item">UV: <?php echo $yesterdayuvmodel["value"]; ?></div>
                </div>
                <div class="stat_tb">
                    <div class="stat_tb_title">前日统计</div>
                    <div class="stat_tb_item">PV: <?php echo $beforepvmodel["value"]; ?></div>
                    <div class="stat_tb_item">IP: <?php echo $beforeipmodel["value"]; ?></div>
                    <div class="stat_tb_item">UV: <?php echo $beforeuvmodel["value"]; ?></div>
                </div>
                <div class="stat_bb">
                    <div class="stat_bb_title">热门来源</div>
                    <?php
                        $nowserver=str_replace('www.', '', $_SERVER['SERVER_NAME']);
                        $hotmodel=Yii::app()->db->createCommand("select if((INSTR(fromurl,'".$nowserver."')>0 and INSTR(fromurl,'".$nowserver."')<13),'站内链接',fromurl) as fromurl,count(id) as num from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate()) group by if((INSTR(fromurl,'".$nowserver."')>0 and INSTR(fromurl,'".$nowserver."')<13),'站内链接',fromurl) order by num desc limit 0,10")->queryAll();
                        //var_dump($hotmodel);
                        foreach($hotmodel as $hotitem){
                    ?>
                    <div class="stat_bb_url"><?php if($hotitem["fromurl"]==''){echo '地址栏输入';}else if($hotitem["fromurl"]=='站内链接'){echo '站内链接';}else echo CHtml::link($hotitem["fromurl"],$hotitem["fromurl"],array('target'=>'_blank')); ?></div>
                    <div class="stat_bb_num"><?php echo $hotitem["num"]; ?></div>
                    <?php
                        }
                    ?>
                </div>
                <div class="stat_bb">
                    <div class="stat_bb_title">热门页面</div>
                    <?php
                        $hotmodel=Yii::app()->db->createCommand("select url,count(id) as num from ccms_statistics where to_days(from_unixtime(visit_time))=to_days(curdate()) group by url order by num desc limit 0,10")->queryAll();
                        //var_dump($hotmodel);
                        foreach($hotmodel as $hotitem){
                    ?>
                    <div class="stat_bb_url"><?php echo CHtml::link($hotitem["url"],$hotitem["url"],array('target'=>'_blank')); ?></div>
                    <div class="stat_bb_num"><?php echo $hotitem["num"]; ?></div>
                    <?php
                        }
                    ?>
                </div>
            </div>
        </div>
</div>