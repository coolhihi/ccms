<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('index');?>" target="navTab" rel="statistics" title="流量监控"><span>日流量</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('bymonth');?>" target="navTab" rel="statistics" title="流量监控"><span>月流量</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('byyear');?>" target="navTab" rel="statistics" title="流量监控"><span>年流量</span></a></li>
			<li><a class="view" href="<?php echo $this->createUrl('empty');?>" target="navTab" title="流量清零" rel="emptyStatistics"><span>清零</span></a></li>
		</ul>
	</div>
        <form id="suretodel-form" method="post" action="<?php echo Yii::app()->createUrl($this->route);?>" class="pageForm required-validate" onsubmit="return validateCallback(this, navTabAjaxDone);">
                    <div class="pageFormContent nowrap" layoutH="83">
                        <dl>
                            <dt>确定要清零？</dt>
                            <dd><?php echo CHtml::dropDownList('Suretodel[sure]','0', array('0'=>'否','1'=>'是'));?></dd>
                        </dl>
                    </div>
                    <div class="formBar">
                        <ul>
                            <li><div class="buttonActive"><div class="buttonContent"><button type="submit">清空</button></div></div></li>
                            <li>
                                <div class="button"><div class="buttonContent"><button type="button" class="close">取消</button></div></div>
                            </li>
                        </ul>
                    </div>
        </form>
</div>