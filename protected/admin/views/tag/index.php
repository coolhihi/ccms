<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageHeader">
	<div class="searchBar">
                <?php
                    /*
                     * 提供父栏目选项，先行整理栏目树形结构
                     */
                    $categoryArr=CFunc::getSonTree();

                    //把树形用缩进显示出来
                    $categoryIds['']='全部';
                    foreach($categoryArr as $categoryItem){
                        $categoryIds[$categoryItem['category']->id]=CFunc::repeatStr('┈┈',$categoryItem['generation']-1).$categoryItem['category']->categoryname;
                    }
                    $form=$this->beginWidget('CActiveForm', array(
                        'action'=>Yii::app()->createUrl($this->route),
                        'method'=>'get',
                        'htmlOptions'=>array(
                                'onsubmit'=>'return navTabSearch(this);',
                        ),
                    ));
                ?>
		<table class="searchContent nolabelstyle">
			<tr>
                                <td><?php echo $form->label($model,'tagname'); ?></td>
                                <td><?php echo $form->textField($model,'tagname'); ?></td>
                                
				<td><div class="buttonActive"><div class="buttonContent"><button type="submit">检索</button></div></div></td>
			</tr>
		</table>
<?php $this->endWidget(); ?>
	</div>
</div>
<div class="pageContent">
        <div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加关键词" rel="createTag"><span>添加</span></a></li>
			<li><a class="delete" href="<?php echo $this->createUrl('delete',array('id'=>'{tag_id}'));?>" target="ajaxTodo" title="确定要删除吗?"><span>删除</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>'{tag_id}'));?>" target="navTab" title="修改关键词" rel="updateTag"><span>修改</span></a></li>
		</ul>
	</div>
<?php $this->widget('ext.dwz.DwzGridView', array(
	'id'=>'tag-grid',
	'dataProvider'=>$model->search(),
        'pager'=>array(
            'cssFile'=>false,
            'class'=>'ext.dwz.DwzPager',
        ),
        'htmlOptions'=>array(
            'layoutH'=>'90',
        ),
	'columns'=>array(
            /*
                array(
                    'name'=>'id',
                    'headerHtmlOptions'=>array(
                        'width'=>'50',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
             */
                array(
                    'name'=>'tagname',
                    'headerHtmlOptions'=>array(
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'left',
                    ),
                ),
                array(
                    'name'=>'searchtimes',
                    'headerHtmlOptions'=>array(
                        'width'=>'130',
                        'align'=>'center',
                    ),
                    'htmlOptions'=>array(
                        'align'=>'center',
                    ),
                ),
	),
)); ?>
</div>