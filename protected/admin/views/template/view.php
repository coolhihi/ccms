<?php Yii::app()->clientScript->scriptMap = array('jquery.js'=>false, 'jquery.min.js'=>false);?>
<div class="pageContent">
	<div class="panelBar">
		<ul class="toolBar">
			<li><a class="add" href="<?php echo $this->createUrl('create');?>" target="navTab" title="添加模板" rel="createTemplate"><span>添加</span></a></li>
			<li><a class="edit" href="<?php echo $this->createUrl('update',array('id'=>$model->id));?>" target="navTab" title="修改模板" rel="updateTemplate"><span>修改</span></a></li>
		</ul>
	</div>
        <div class="pageFormContent nowrap" layoutH="47">
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'id'); ?></dt>
                    <dd><?php echo $model->id; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'title'); ?></dt>
                    <dd><?php echo $model->title; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'categoryfile'); ?></dt>
                    <dd><?php echo $model->categoryfile; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'contentfile'); ?></dt>
                    <dd><?php echo $model->contentfile; ?></dd>
                </dl>
		<div class="divider"></div>
                <dl>
                    <dt><?php echo CHtml::activeLabel($model, 'perpage'); ?></dt>
                    <dd><?php echo $model->perpage; ?></dd>
                </dl>
        </div>
</div>