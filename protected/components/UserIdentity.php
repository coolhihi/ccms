<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    private $_id;

    /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=Manager::model()->find('enable=1 and LOWER(username)=?',array(strtolower($this->username)));
                //Yii::trace(md5($this->password),'cool.pd');
                //Yii::trace($user->password,'cool.collectpd');
                //Yii::log($user->password,'info','cool.collectpd');
		if($user===null)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if(!$user->validatePassword($this->password))
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->_id=$user->id;
			$this->username=$user->username;
			$this->setState('id', $user->id);
			$this->setState('managername', $user->username);
			$this->setState('manageright', $user->manageright);
			$this->setState('lastlogin_time', $user->lastlogin_time);
			$this->setState('returnUrl', '/admin');
			$this->errorCode=self::ERROR_NONE;
                        
                        //更新最后登陆时间
                        //var_dump($user);
                        //$user->lastlogin_time=time();
                        //$user->save();
                        //Yii::app()->end();
                        $updatePro=array('lastlogin_time'=>time());
                        $user->updateByPk($this->id, $updatePro);
                        //用updateByPk方法不会调用afterSave()函数，避免产生歧义日志
		}
		return $this->errorCode==self::ERROR_NONE;
	}
        public function getId(){
            return $this->_id;
        }
}