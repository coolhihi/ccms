<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CCMS Example',
	'timeZone' => 'Asia/Shanghai',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.helpers.*',
		'application.extensions.mail.*',
	),

	'modules'=>array(
            'sitemap' => array(
                'class' => 'ext.sitemap.SitemapModule',     //or whatever the correct path is
                'actions' => array(
                    'site/index', //no configuration specified, this will only work if site/index has default argument values
                    array(
                        'route' => 'category/view',
                        'params' => array( //specify action parameters
                            'model' => array(
                                'class' => 'Category',
                                'criteria' => array(),
                                'map' => array(
                                    'id' => 'id',
                                ),
                            ),              
                        ),
                    ),
                    array(
                        'route' => 'content/view',
                        'params' => array( //specify action parameters
                            'model' => array(
                                'class' => 'Content',
                                'criteria' => array('condition'=>'enable=1'),
                                'map' => array(
                                    'id' => 'id',
                                ),
                            ),
                        ),
                    ),
                ),
                'absoluteUrls' => true|false,               //optional
                'protectedControllers' => array('stat'),    //optional
                'protectedActions' =>array('site/error'),   //optional
                'priority' => '0.5',                        //optional
                'changefreq' => 'daily',                    //optional
                //'lastmod' => '1985-11-05',                  //optional
                'cacheId' => 'cache',                       //optional
                'cachingDuration' => 36000000,                  //optional
            ),
	),

	// language
	'language'=>'zh_cn',
        
        // theme
        'theme'=>'demo',
    
	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
                        'showScriptName'=>false,            //隐藏index.php
                        'urlSuffix'=>'.htm',               //加上.html
                        'rules'=>array(
                            'sitemap.xml'   => 'sitemap/default/index',
                            'sitemap.html'  => 'sitemap/default/index/format/html',
                        ),
		),
		
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=coolccms',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'tablePrefix' => 'ccms_',
		),

		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'trace, info,error, warning',
				),
				// uncomment the following to show log messages on web pages
				//注释此array可使logs不显示在网页上
                                /*
				array(
					'class'=>'CWebLogRoute',
                                        'levels'=>'trace, info, error, warning',
                                        'categories'=>'cool.*',
				),
				*/
			),
		),
		'cache' => array (
			'class' => 'system.caching.CFileCache'
		),
	),
);