<?php

class CategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + view',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','page'),
                    )
		);
	}
        
        public function actionView($id){
            //Yii::trace($id,'cool.dd');
            if($model=Category::model()->findByPk($id)){
                
                //读取自定义变量中的关键字和描述 + 栏目名称
                if(Yii::app()->request->getParam('page'))
                    $this->pageTitle=$model->categoryname.' - 第'.Yii::app()->request->getParam('page').'页'.' - '.Yii::app()->name;
                else
                    $this->pageTitle=$model->categoryname.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords').', '.$model->categoryname, 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description').' '.$model->categoryname, 'Description');
                        
                $templateExist=!is_null(Template::model()->findByPk($model->template_id));
                $tempPerpage=20;
                
                switch ($model->kind){
                    case '1':
                        //普通栏目，列表
                        $criteria=new CDbCriteria;
                        $criteria->compare('category_id',$model->id);
                        $criteria->compare('enable',1);
                        if($templateExist){
                            if($tempPerpage=$model->template->perpage!="")
                                $tempPerpage=$model->template->perpage;
                        }
                        $dataProvider=new CActiveDataProvider('Content', array(
                            'criteria'=>$criteria,
                            'pagination'=>array(
                                'pageVar'=>'page',
                                'pageSize'=>$tempPerpage,
                            ),
                            'sort'=>array(
                                'defaultOrder'=>'weight DESC,create_time DESC', //设置默认排序是create_time倒序
                            ),
                        ));
                        if(!$templateExist){
                            //模板已不存在，或选的是默认模板
                            $this->render('category',array('model'=>$model,'dataProvider'=>$dataProvider));
                        }
                        else if($this->getViewFile($model->template->categoryfile)!==false)
                            $this->render($model->template->categoryfile,array('model'=>$model,'dataProvider'=>$dataProvider));
                        else
                            $this->render('category',array('model'=>$model,'dataProvider'=>$dataProvider));
                        break;
                    case '2':
                        if(!$templateExist){
                            //模板已不存在，或选的是默认模板
                            $this->render('singlepage',array('model'=>$model));
                        }
                        else if($this->getViewFile($model->template->categoryfile)!==false)
                            $this->render($model->template->categoryfile,array('model'=>$model));
                        else
                            $this->render('singlepage',array('model'=>$model));
                        break;
                    case '3':
                        $this->redirect($model->outsideurl);
                        break;
                }
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
        }
}
