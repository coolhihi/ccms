<?php

class ContentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + view',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','pagenum'),
                    )
		);
	}
        
        public function actionView($id){
            if(@$model=Content::model()->findByPk($id)){
                //读取自定义变量中的关键字和描述 + 文章关键字
                if(Yii::app()->request->getParam('pagenum'))
                    $this->pageTitle=$model->title.' - 第'.Yii::app()->request->getParam('pagenum').'页'.' - '.Yii::app()->name;
                else
                    $this->pageTitle=$model->title.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag($model->tags, 'Keywords');
                Yii::app()->clientScript->registerMetaTag($model->summary, 'Description');
                
                if($model->enable=='1'){
                        //处理分页
                        $tempContent=explode('<hr name="pageline" />',$model->content);
                        $pagenum=1;
                        if(isset($_GET['pagenum'])){
                            $pagenum=1*$_GET['pagenum'];
                            if($pagenum>count($tempContent))
                                $pagenum=count($tempContent);
                        }
                        
                        $contentPager='<div class="contentpager">';
                        for($i=1;$i<=count($tempContent);$i++){
                            if($i==$pagenum)
                                $contentPager.='<li><a class="currentcontentpage">'.$i.'</a></li>';
                            else
                                $contentPager.='<li><a href="'.$this->createUrl($this->route,array('id'=>$id,'pagenum'=>$i)).'">'.$i.'</a></li>';
                        }
                        $contentPager.='</div>';
                        
                        $model->content.='';//文章前显示内容
                        $model->content=$tempContent[$pagenum-1];
                        $model->content.=$contentPager;//文章后显示内容
                        
                        if(is_null(Template::model()->findByPk($model->category->template_id))){
                            //模板已不存在，或选的是默认模板
                            $this->render('content',array('model'=>$model));
                        }
                        else if($this->getViewFile($model->category->template->contentfile)!==false)
                            $this->render($model->category->template->contentfile,array('model'=>$model));
                        else
                            $this->render('content',array('model'=>$model));
                }
                else
                    throw new CHttpException('404','该文章已被禁用.');
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
        }
}
