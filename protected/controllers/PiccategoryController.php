<?php

class PiccategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + index, gallery',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','page'),
                    )
		);
	}
        
        /*
         * index 图库首页
         */
        public function actionIndex(){
                
                //读取自定义变量中的关键字和描述
                $this->pageTitle='图库 - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
            $gallerys=Piccategory::model()->findAll();
            $this->render('index',array('gallerys'=>$gallerys));
            
        }
        
        /*
         * gallery 图库
         */
        public function actionGallery($id){
            if(@$model=Piccategory::model()->findByPk($id)){
                
                //读取自定义变量中的关键字和描述
                $this->pageTitle=$model->categoryname.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
                $criteria=new CDbCriteria;
                $criteria->compare('piccategory_id',$model->id);
                $criteria->compare('enable',1);
                $dataProvider=new CActiveDataProvider('Picture', array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageVar'=>'page',
                        'pageSize'=>'20',
                    ),
                    'sort'=>array(
                        'defaultOrder'=>'weight DESC,create_time DESC', //设置默认排序是create_time倒序
                    ),
                ));
                if($this->getViewFile($model->renderfile)!==false)
                    $this->render($model->renderfile,array('model'=>$model,'dataProvider'=>$dataProvider));
                else
                    $this->render('gallery',array('model'=>$model,'dataProvider'=>$dataProvider));
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
        }
}
