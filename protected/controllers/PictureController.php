<?php

class PictureController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + view',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','page'),
                    )
		);
	}
        
        /*
         * photo 图片页
         */
        public function actionView($id){
            
            if(@$model=Picture::model()->findByPk($id)){
                
                //读取自定义变量中的关键字和描述
                $this->pageTitle=$model->title.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
                if($model->enable=='1'){
                        //点击数自加一
                        @ $model->addClick();
                        if($this->getViewFile($model->piccategory->picturerenderfile)!==false)
                            $this->render($model->piccategory->picturerenderfile,array('model'=>$model));
                        else
                            $this->render('view',array('model'=>$model));
                }
                else
                    throw new CHttpException('404','该图片已被禁用.');
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
            
        }
}
