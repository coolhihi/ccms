<?php

class ProcategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + index, procategory',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','page'),
                    )
		);
	}
        
        /*
         * 产品中心
         */
	public function actionIndex()
	{
                
                //读取自定义变量中的关键字和描述
                $this->pageTitle='产品中心 - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
                        $criteria=new CDbCriteria;
                        
                        $pageSize="4";
                        
                        $allSonProcategory=array();
                        CFunc::getAllSonProcategory(0,$allSonProcategory);
                        $criteria->addInCondition('procategory_id',$allSonProcategory);
                        $criteria->compare('enable',1);
                        $dataProvider=new CActiveDataProvider('Product', array(
                            'criteria'=>$criteria,
                            'pagination'=>array(
                                'pageVar'=>'page',
                                'pageSize'=>$pageSize,
                            ),
                            'sort'=>array(
                                'defaultOrder'=>'weight DESC,create_time DESC', //设置默认排序是create_time倒序
                            ),
                        ));
		$this->render('index',array('dataProvider'=>$dataProvider));
	}
        
        /*
         * procategory 产品分类页，分析设置并采用正式方式展示
         */
        public function actionProcategory($id){
            
            if($model=Procategory::model()->findByPk($id)){
                
                //读取自定义变量中的关键字和描述 + 产品分类名
                $this->pageTitle=$model->categoryname.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords').', '.$model->categoryname, 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
                        $criteria=new CDbCriteria;
                        
                        $pageSize="4";
                        
                        $allSonProcategory=array($id);
                        CFunc::getAllSonProcategory($id,$allSonProcategory);
                        $criteria->addInCondition('procategory_id',$allSonProcategory);
                        $criteria->compare('enable',1);
                        $dataProvider=new CActiveDataProvider('Product', array(
                            'criteria'=>$criteria,
                            'pagination'=>array(
                                'pageVar'=>'page',
                                'pageSize'=>$pageSize,
                            ),
                            'sort'=>array(
                                'defaultOrder'=>'weight DESC,create_time DESC', //设置默认排序是create_time倒序
                            ),
                        ));
                        if($this->getViewFile($model->renderfile)!==false)
                            $this->render($model->renderfile,array('model'=>$model,'dataProvider'=>$dataProvider));
                        else
                            $this->render('procategory',array('model'=>$model,'dataProvider'=>$dataProvider));
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
        }
}
