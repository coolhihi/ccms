<?php

class ProductController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
                    array (
                        //'COutputCache',
                        'COutputCache + index, view',
                        'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                        'varyByParam' => array('id','page'),
                    )
		);
	}
        
        /*
         * index 图库首页
         */
        public function actionIndex(){
                
                //读取自定义变量中的关键字和描述
                $this->pageTitle='图库 - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
            $gallerys=Piccategory::model()->findAll();
            $this->render('index',array('gallerys'=>$gallerys));
            
        }
        
        /*
         * product 产品页
         */
        public function actionView($id){
            
            if(@$model=Product::model()->findByPk($id)){
                
                //读取自定义变量中的关键字和描述 + 产品关键字
                $this->pageTitle=$model->title.' - '.Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords').', '.$model->tags, 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
                if($model->enable=='1'){
                        //点击数自加一
                        @ $model->addClick();
                        
                        if($this->getViewFile($model->procategory->productrenderfile)!==false)
                            $this->render($model->procategory->productrenderfile,array('model'=>$model));
                        else
                            $this->render('view',array('model'=>$model));
                }
                else
                    throw new CHttpException('404','该产品已被禁用.');
            }
            else{
                throw new CHttpException('404','错误的访问地址.');
            }
        }
}
