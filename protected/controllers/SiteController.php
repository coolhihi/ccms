<?php

class SiteController extends Controller
{
	public $layout='//layouts/main';
	/**
	 * Declares class-based actions.
	 */
        
        public function filters() {
            return array (
                array (
                    //'COutputCache',
                    'COutputCache + index',
                    'duration' => CFunc::getCustomparam('system_cache_duration'), //缓存时间从系统数据库设定读得
                    'varyByParam' => array('id','page'),
                )
            );
        }
        
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
                
                //读取自定义变量中的关键字和描述
                if(Yii::app()->request->getParam('page'))
                     $this->pageTitle=Yii::app()->name.' - 第'.Yii::app()->request->getParam('page').'页';
                else
                    $this->pageTitle=Yii::app()->name;
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest){
	    		echo $error['message'];
		}
	    	else{
                        //读取自定义变量中的关键字和描述
                        $this->pageTitle=Yii::app()->name.' - 错误';
                        Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_keywords'), 'Keywords');
                        Yii::app()->clientScript->registerMetaTag(CFunc::getCustomparam('custom_website_description'), 'Description');
                
	        	$this->render('error', $error);
		}
	    }
	}
        
        /*
         * 清理所有页面缓存
         */
        public function actionFlushcache(){
            try{
                Yii::app()->cache->flush();
                echo "true";
            } catch (Exception $exc) {
                echo "false";
            }
        }
}