<?php

class StatController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		);
	}
        
        public function actionIndex($surl,$sfrom,$controllerid,$actionid,$id){
            //在缓存情况下依然以JS调用本统计
            $surl=CFunc::srcUrlDecode($surl);
            $sfrom=CFunc::srcUrlDecode($sfrom);
            
            //如果页面不是被站内JS调用，则直接结束
            //echo 'alert("'.$_SERVER['HTTP_REFERER'].'");';
            
            //若页面不作统计，则直接结束
            $unstatArray=array('error','login');
            if(in_array($actionid, $unstatArray)){
                    Yii::app()->end();
            }
            
            //流量统计
            try{
                    $model=new Statistics;
                    $model->ipaddress=$_SERVER['REMOTE_ADDR'];
                    $model->url=$surl;
                    $model->fromurl=$sfrom;

                    //若UV检验cookie不存在，则为独立访客，并创建cookie
                    $cookie=Yii::app()->request->getCookies();
                    if(is_null($cookie['ccms_stat'])){
                        $cookie=new CHttpCookie('ccms_stat','hello');
                        $cookie->expire = time()+60*60*12;
                        Yii::app()->request->cookies['ccms_stat']=$cookie;
                        $model->isuv=1;
                    }
                    else
                        $model->isuv=0;
                    $model->visit_time=time();
                    $model->setIsNewRecord(true);
                    //var_dump($model);
                    //Yii::app()->end();
                    if($model->save()){
                            //return true;
                    }
            }
            catch (Exception $exc) {
                    //抓取错误，不提示
                    //return false;
            }
            
            //如果是资讯，产品，图片，则点击数加一
            //并更新缓存的点击数
            try {
                if($controllerid=='content' && $actionid=='view' && $id!='0'){
                        //资讯点击加一
                        Yii::app()->db->createCommand("update {{content}} set clicktimes=clicktimes+1 where id=".$id)->execute();
                        $ckresult=Yii::app()->db->createCommand("select clicktimes from {{content}} where id=".$id)->query()->read();
                        echo "document.getElementById('CkTimes').innerText='".$ckresult['clicktimes']."';\n";
                }
                else if($controllerid=='picture' && $actionid=='view' && $id!='0'){
                        //图片点击加一
                        Yii::app()->db->createCommand("update {{picture}} set clicktimes=clicktimes+1 where id=".$id)->execute();
                        $ckresult=Yii::app()->db->createCommand("select clicktimes from {{picture}} where id=".$id)->query()->read();
                        echo "document.getElementById('CkTimes').innerText='".$ckresult['clicktimes']."';\n";
                }
                else if($controllerid=='product' && $actionid=='view' && $id!='0'){
                        //产品点击加一
                        Yii::app()->db->createCommand("update {{product}} set clicktimes=clicktimes+1 where id=".$id)->execute();
                        $ckresult=Yii::app()->db->createCommand("select clicktimes from {{product}} where id=".$id)->query()->read();
                        echo "document.getElementById('CkTimes').innerText='".$ckresult['clicktimes']."';\n";
                }
            } catch (Exception $exc) {
            }
        }
}
