create table ccms_manager(
    id int not null auto_increment,
    username varchar(100) not null,
    password varchar(100) not null,
    realname varchar(100) not null,
    manageright varchar(100) not null,
    create_time int not null,
    lastlogin_time int not null,
    enable int,
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_managelog(
    id int not null auto_increment,
    manager_id int not null,
    content varchar(200) not null,
    create_time int not null,
    primary key(id),
    foreign key(manager_id) references ccms_manager(id)
    on update cascade on delete restrict,
    index(manager_id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_customparam(
    id int not null auto_increment,
    paramname varchar(50) not null,
    paramvalue varchar(200) not null,
    paramtitle varchar(200) not null,
    manager_id int not null,
    enable int not null,
    systemparam int not null,
    primary key(id),
    foreign key(manager_id) references ccms_manager(id)
    on update cascade on delete restrict,
    unique(paramname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_category(
    id int not null auto_increment,
    parent_id int not null,
    categoryname varchar(200) not null,
    kind int not null,
    outsideurl varchar(200),
    joinnav int not null,
    weight int not null,
    image varchar(200),
    template_id int not null,
    introduction text,
    primary key(id),
    index(parent_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_content(
    id int not null auto_increment,
    category_id int not null,
    weight int not null,
    title varchar(200) not null,
    author varchar(200),
    origin varchar(200),
    attachment text,
    image varchar(200),
    flags varchar(200),
    allowcomment int not null,
    summary text,
    content text,
    tags varchar(200),
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    enable int not null,
    primary key(id),
    foreign key(category_id) references ccms_category(id)
    on update cascade on delete restrict,
    index(category_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_procategory(
    id int not null auto_increment,
    parent_id int not null,
    categoryname varchar(200) not null,
    weight int not null,
    image varchar(200),
    protemplate_id int not null,
    introduction text,
    primary key(id),
    index(parent_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_product(
    id int not null auto_increment,
    procategory_id int not null,
    weight int not null,
    title varchar(200) not null,
    image varchar(200),
    flags varchar(200),
    content text,
    tags varchar(200),
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    enable int not null,
    param1 text,
    param2 text,
    primary key(id),
    foreign key(procategory_id) references ccms_procategory(id)
    on update cascade on delete restrict,
    index(procategory_id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_piccategory(
    id int not null auto_increment,
    categoryname varchar(200) not null,
    weight int not null,
    renderfile varchar(200),
    picturerenderfile varchar(200),
    image varchar(200),
    introduction text,
    primary key(id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_picture(
    id int not null auto_increment,
    piccategory_id int not null,
    weight int not null,
    title varchar(200) not null,
    author varchar(200),
    origin varchar(200),
    image varchar(200),
    introduction text,
    width int,
    height int,
    flags varchar(200),
    enable int not null,
    clicktimes int not null,
    create_time int not null,
    lastedit_time int not null,
    primary key(id),
    foreign key(piccategory_id) references ccms_piccategory(id)
    on update cascade on delete cascade,
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_partner(
    id int not null auto_increment,
    title varchar(200) not null,
    image varchar(200),
    url varchar(200),
    description text,
    weight int not null,
    create_time int not null,
    primary key(id),
    index(weight)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_template(
    id int not null auto_increment,
    title varchar(200) not null,
    categoryfile varchar(200),
    contentfile varchar(200),
    perpage int,
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_protemplate(
    id int not null auto_increment,
    title varchar(200) not null,
    categoryfile varchar(200),
    contentfile varchar(200),
    perpage int,
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_statistics(
    id int not null auto_increment,
    ipaddress varchar(200) not null,
    url varchar(200) not null,
    fromurl varchar(200),
    isuv int not null,
    visit_time int not null,
    primary key(id),
    index(ipaddress),
    index(isuv)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_dbbackup(
    id int not null auto_increment,
    filename varchar(200) not null,
    backuptime int not null,
    backupremark varchar(200),
    primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1;

create table ccms_tag(
    id int not null auto_increment,
    tagname varchar(100) not null,
    searchtimes int not null,
    primary key(id),
    unique(tagname),
    index(tagname)
)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

insert into ccms_manager(
    username,password,realname,manageright,create_time,lastlogin_time,enable
)
values
(
    'admin','96796cca2bab85d63673299c90eb6d99','Great Administrator','111111111111111111111111111111111111111111111111111111111111',UNIX_TIMESTAMP(NOW()),UNIX_TIMESTAMP(NOW()),1
);

insert into ccms_customparam(
    paramname,paramvalue,paramtitle,enable,manager_id,systemparam
)values
('system_all_on','1','网站开关',1,1,1),
('system_company_name','donson','开发公司',1,1,1),
('system_company_website','http://www.donson.com.cn','开发公司链接',1,1,1),
('system_manager_name','COoL','开发者',1,1,1),
('system_manager_email','andrew404@163.com','开发者邮箱',1,1,1),
('system_comment_on','0','评论开关',1,1,1),
('system_picture_deletefile','1','同时删除图片',1,1,1),
('system_feedback_on','0','留言至邮箱',1,1,1),
('system_feedback_email','andrew404@163.com','留言邮箱地址',1,1,1),
('system_cache_duration','0','页面缓存时间',1,1,1),
('system_autosummary_count','0','自动描述字数',1,1,1),
('custom_website_name','website name','网站名称',1,1,0),
('custom_website_keywords','网站关键字,用逗号或空格间隔开','网站关键字',1,1,0),
('custom_website_description','请更换上网站描述','网站描述',1,1,0),
('custom_website_recordnumber','NO.123456','备案号',1,1,0),
('custom_website_copyright','companyname Co.,Ltd','网站授权',1,1,0);
