<?php

/**
 * This is the model class for table "{{category}}".
 *
 * The followings are the available columns in table '{{category}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $categoryname
 * @property integer $kind
 * @property string $outsideurl
 * @property integer $joinnav
 * @property integer $weight
 * @property string $image
 * @property integer $template_id
 * @property string $introduction
 *
 * The followings are the available model relations:
 * @property Content[] $contents
 * @property Template $template
 */
class Category extends CBaseModel
{
    public $image_s;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('parent_id, categoryname, kind, joinnav, weight, template_id', 'required'),
			array('parent_id, kind, joinnav, weight, template_id', 'numerical', 'integerOnly'=>true),
			array('categoryname, image', 'length', 'max'=>200),
			array('introduction, image_s, outsideurl', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, parent_id, weight, kind, categoryname, joinnav', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contents' => array(self::HAS_MANY, 'Content', 'category_id', 'order'=>'contents.weight desc,contents.id desc'),
			'template' => array(self::BELONGS_TO, 'Template', 'template_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => '父栏目ID',
			'categoryname' => '栏目名称',
			'kind' => '栏目类型',
			'outsideurl' => '外部地址',
			'joinnav' => '导航显示',
			'weight' => '排序权重',
			'image' => '栏目主图',
			'template_id' => '选用模板',
			'introduction' => '栏目简介',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('joinnav',$this->joinnav);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('kind',$this->kind);
		$criteria->compare('categoryname',$this->categoryname,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                         'pagination'=>array(
                             'pageSize'=>30,
                         ),
		));
	}
        
        public function getKind(){
            switch($this->kind){
                case 1:
                    return '普通栏目';
                case 2:
                    return '单页栏目';
                case 3:
                    return '外部栏目';
            }
        }
        public function getParents($categoryId=null,$result=null){
            if(is_null($categoryId)) $categoryId=$this->id;
            if($categoryId==0){
                return '根目录'.$result;
            }
            else{
                $pCate=$this->model()->findByPk($categoryId);
                $result=' >> '.$pCate->categoryname.$result;
                return $this->getParents($pCate->parent_id,$result);
            }
        }
        public function beforeDelete() {
            if(parent::beforeDelete()){
                if(count(Category::model()->findByAttributes(array('parent_id'=>$this->id)))>0){
                    Yii::app()->user->setFlash('deleteError','存在子栏目！');
                    return false;
                }
                else if(count(Content::model()->findByAttributes(array('category_id'=>$this->id)))>0){
                    Yii::app()->user->setFlash('deleteError','该栏目下还有文章！');
                    return false;
                }
                else
                    return true;
            }
            return false;
        }
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了栏目 {".($this->categoryname)."}");
            else
                CFunc::writeLog("修改了栏目 {".($this->categoryname)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了栏目 {".($this->categoryname)."}");
            return true;
        }
        public function beforeSave() {
            if(parent::beforeSave()){
                if($this->id==$this->parent_id){
                    Yii::app()->user->setFlash('saveError','父栏目不能是自己！');
                    return false;
                }
                return true;
            }
            return false;
        }
}