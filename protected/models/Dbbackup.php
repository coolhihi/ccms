<?php

/**
 * This is the model class for table "{{dbbackup}}".
 *
 * The followings are the available columns in table '{{dbbackup}}':
 * @property integer $id
 * @property string $filename
 * @property integer $backuptime
 * @property string $backupremark
 *
 */
class Dbbackup extends CBaseModel
{
    /*
     * Property in Model but not in DB
     */
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return Manager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{dbbackup}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('filename,backupremark', 'required'),
			array('backuptime', 'numerical', 'integerOnly'=>true),
			array('filename,backupremark', 'length', 'max'=>200),
                        array('id,backuptime','safe','on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'backuptime' => '备份时间',
			'filename' => '备份文件名',
			'backupremark' => '备注',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('backupremark',$this->backupremark,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'backuptime DESC', //设置默认排序是backuptime倒序
                        ),
		));
	}
        
        public function beforeSave() {
		if(parent::beforeSave())
		{
			$this->backuptime=time();
			return true;
		}
		else
			return false;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了数据库备份 {".(date('Y-m-d H:i',$this->backuptime))."}");
            return true;
        }
}