<?php

/**
 * This is the model class for table "{{managelog}}".
 *
 * The followings are the available columns in table '{{managelog}}':
 * @property integer $id
 * @property integer $manager_id
 * @property string $content
 * @property integer $create_time
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Managelog extends CBaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Managelog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{managelog}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content', 'required'),
			array('manager_id, create_time', 'numerical', 'integerOnly'=>true),
			array('content', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, manager_id, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'manager_id' => '管理员ID',
			'content' => '操作内容',
			'create_time' => '时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id);
		$criteria->compare('manager_id',$this->manager_id);
		$criteria->compare('content',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'create_time DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
}