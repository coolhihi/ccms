<?php

/**
 * This is the model class for table "{{manager}}".
 *
 * The followings are the available columns in table '{{manager}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $realname
 * @property string $manageright
 * @property integer $create_time
 * @property integer $lastlogin_time
 * @property integer $enable
 *
 * The followings are the available model relations:
 * @property Managelog[] $managelogs
 */
class Manager extends CBaseModel
{
    /*
     * Property in Model but not in DB
     */
    public $oldPassword;
    public $password2;
    public $managerightArray=array();
    
	/**
	 * Returns the static model of the specified AR class.
	 * @return Manager the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{manager}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, realname', 'required'),
			array('create_time, lastlogin_time, enable', 'numerical', 'integerOnly'=>true),
			array('username, password, realname, manageright', 'length', 'max'=>100),
                        array('password2', 'compare', 'compareAttribute'=>'password'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, realname, enable', 'safe', 'on'=>'search'),
                        array('managerightArray, manageright','safe'),
			array('oldPassword, password, password2', 'required', 'on'=>'changepswd'),
                        array('password, password2','required','on'=>'createmanager'),
                        array('password, password2','safe','on'=>'updatemanager'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'managelogs' => array(self::HAS_MANY, 'Managelog', 'manager_id', 'order'=>'managelogs.id desc'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => '用户名',
			'password' => '密码',
			'realname' => '真实姓名',
			'manageright' => '管理权限',
			'create_time' => '创建时间',
			'lastlogin_time' => '最后登陆时间',
			'enable' => '状态',
			'oldPassword' => '旧密码',
			'password2' => '确认密码',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('realname',$this->realname,true);
		$criteria->compare('enable',$this->enable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
		));
	}

	/**
	 * Checks if the given password is correct.
	 * @param string the password to be validated
	 * @return boolean whether the password is valid
	 */
	public function validatePassword($password)
	{
		return $this->hashPassword($password)===$this->password;
	}

	/**
	 * Generates the password hash.
	 * @param string password
	 * @param string salt
	 * @return string hash
	 */
	public function hashPassword($password)
	{
		return md5('c'.$password.'c');
	}
        
        public function beforeSave() {
		if(parent::beforeSave())
		{
			if($this->isNewRecord)
			{
				$this->create_time=$this->lastlogin_time=time();
			}
			return true;
		}
		else
			return false;
        }
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了管理员 {".($this->username)."}");
            else
                CFunc::writeLog("修改了管理员 {".($this->username)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了管理员 {".($this->username)."}");
            return true;
        }
}