<?php

/**
 * This is the model class for table "{{piccategory}}".
 *
 * The followings are the available columns in table '{{piccategory}}':
 * @property integer $id
 * @property string $categoryname
 * @property integer $weight
 * @property string $renderfile
 * @property string $picturerenderfile
 * @property string $image
 * @property string $introduction
 *
 * The followings are the available model relations:
 * @property Picture[] $pictures
 */
class Piccategory extends CBaseModel
{
    public $image_s;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Piccategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{piccategory}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('weight, categoryname', 'required'),
			array('weight', 'numerical', 'integerOnly'=>true),
			array('image, categoryname, renderfile, picturerenderfile', 'length', 'max'=>200),
			array('introduction, image_s', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categoryname', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pictures' => array(self::HAS_MANY, 'Picture', 'piccategory_id', 'order'=>'pictures.weight desc,pictures.id desc'),
			'picturesCount' => array(self::STAT, 'Picture', 'piccategory_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'weight' => '排序权重',
			'renderfile' => '图库视图',
			'picturerenderfile' => '图片视图',
			'image' => '图库封面',
			'introduction' => '图库简介',
			'categoryname' => '图库名',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
                $criteria->compare('weight',$this->weight);
                $criteria->compare('image',$this->image,true);
		$criteria->compare('categoryname',$this->categoryname,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'weight DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog ('创建了新图库 {'.$this->categoryname.'}');
            else
                CFunc::writeLog ('修改了图库 {'.$this->categoryname.'}');
        }
        public function beforeDelete() {
            if(parent::beforeDelete()){
                if(count(Picture::model()->findByAttributes(array('piccategory_id'=>$this->id)))>0){
                    Yii::app()->user->setFlash('deleteError','图库中存在图片，删除失败！');
                    return false;
                }
                else
                    return true;
            }
            return false;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog('删除了图库 {'.$this->categoryname.'}');
        }
}