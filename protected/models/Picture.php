<?php

/**
 * This is the model class for table "{{picture}}".
 *
 * The followings are the available columns in table '{{picture}}':
 * @property integer $id
 * @property integer $piccategory_id
 * @property integer $weight
 * @property string $title
 * @property string $author
 * @property string $origin
 * @property string $image
 * @property string $introduction
 * @property string $flags
 * @property integer $width
 * @property integer $height
 * @property integer $enable
 * @property integer $clicktimes
 * @property integer $create_time
 * @property integer $lastedit_time
 * @property integer $manager_id
 *
 * The followings are the available model relations:
 * @property Piccategory $piccategory
 */
class Picture extends CBaseModel
{
    public $image_s;
	/**
	 * Returns the static model of the specified AR class.
	 * @return Picture the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{picture}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('piccategory_id, weight, title, image', 'required'),
			array('piccategory_id, weight, width, height, manager_id, enable, clicktimes, create_time, lastedit_time', 'numerical', 'integerOnly'=>true),
			array('title, author, origin, image', 'length', 'max'=>200),
			array('introduction, image_s', 'safe'),
			array('piccategory_id', 'numerical' ,'min'=>1,'tooSmall'=>'请选择图库.'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, piccategory_id, title, author, origin, enable, manager_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'piccategory' => array(self::BELONGS_TO, 'Piccategory', 'piccategory_id'),
			'manager' => array(self::BELONGS_TO, 'Manager', 'manager_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'piccategory_id' => '所属图库',
			'weight' => '排序权重',
			'title' => '标题',
			'author' => '作者',
			'origin' => '链接',
			'image' => '图片地址',
			'introduction' => '图片介绍',
			'width' => '宽',
			'height' => '高',
                        'manager_id' => '管理员ID',
			'enable' => '状态',
			'clicktimes' => '点击数',
			'create_time' => '发布时间',
			'lastedit_time' => '修改时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('weight',$this->weight);
		$criteria->compare('piccategory_id',$this->piccategory_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('author',$this->author,true);
		$criteria->compare('origin',$this->origin,true);
		$criteria->compare('enable',$this->enable);
		$criteria->compare('manager_id',$this->enable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'create_time DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
        
        
        /*
         * addClick 点击数加一
         */
        public function addClick(){
            //Yii::trace('a','cool.a');
            //以下花3.31秒
            //$this->updateByPk($this->id, array('clicktimes'=>$this->clicktimes+1));
            //Yii::trace('a','cool.b');
            
            //以下花5.9秒
            //$this->clicktimes+=1;
            //$this->update();
            //Yii::trace('a','cool.c');
            
            //以下花1.75秒，但@屏蔽不了错误，只能try
            try {
                Yii::app()->db->createCommand("update {{picture}} set clicktimes=clicktimes+1 where id=".$this->id)->query();
            } catch (Exception $exc) {
            }
            //Yii::trace('a','cool.d');
        }
        
        public function beforeSave() {
            if(parent::beforeSave()){
                if($this->isNewRecord){
                    $this->create_time=$this->lastedit_time=time();
                    $this->clicktimes=0;
                }
                else
                    $this->lastedit_time=time();
                $this->manager_id=Yii::app()->user->id;
                return true;
            }
            return false;
        }
        public function afterSave() {
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog ('添加了图片 {'.$this->title.'}');
            else
                CFunc::writeLog ('修改了图片 {'.$this->title.'}');
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog('删除了图片 {'.$this->title.'}');
            if(CFunc::getCustomparam('system_picture_deletefile')=='1'){
                @unlink($this->image);
                @unlink(CFunc::getSmallImg($this->image));
            }
        }
}