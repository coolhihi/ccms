<?php

/**
 * This is the model class for table "{{product}}".
 *
 * The followings are the available columns in table '{{product}}':
 * @property integer $id
 * @property integer $procategory_id
 * @property integer $weight
 * @property string $title
 * @property string $image
 * @property string $flags
 * @property string $content
 * @property string $tags
 * @property integer $clicktimes
 * @property integer $create_time
 * @property integer $lastedit_time
 * @property integer $enable
 * @property string $param1
 * @property string $param2
 *
 * The followings are the available model relations:
 * @property Procategory $procategory
 */
class Product extends CActiveRecord
{
    public $flagsArr=array();
    public $image_s;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{product}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('procategory_id, weight, title, enable', 'required'),
			array('procategory_id, weight, clicktimes, create_time, lastedit_time, enable', 'numerical', 'integerOnly'=>true),
			array('title, image, flags, tags', 'length', 'max'=>200),
			array('procategory_id', 'numerical', 'min'=>1, 'tooSmall'=>'请选择产品分类.'),
			array('flagsArr,content,param1,param2', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, procategory_id, weight, title, flags, content, tags, enable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'procategory' => array(self::BELONGS_TO, 'Procategory', 'procategory_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'procategory_id' => '所属分类',
			'weight' => '排序权重',
			'title' => '产品名称',
			'image' => '产品图片',
			'flags' => '产品特性',
			'flagsArr' => '产品特性',
			'content' => '产品介绍',
			'tags' => '标签',
			'clicktimes' => '点击数',
			'create_time' => '发布时间',
			'lastedit_time' => '修改时间',
			'enable' => '可用',
			'param1' => '参数A',
			'param2' => '参数B',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('procategory_id',$this->procategory_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('flags',$this->flags,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('enable',$this->enable);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'create_time DESC', //设置默认排序是create_time倒序
                        ),
		));
	}
        
        /*
         * addClick 点击数加一
         */
        public function addClick(){
            //Yii::trace('a','cool.a');
            //以下花3.31秒
            //$this->updateByPk($this->id, array('clicktimes'=>$this->clicktimes+1));
            //Yii::trace('a','cool.b');
            
            //以下花5.9秒
            //$this->clicktimes+=1;
            //$this->update();
            //Yii::trace('a','cool.c');
            
            //以下花1.75秒，但@屏蔽不了错误，只能try
            try {
                Yii::app()->db->createCommand("update {{product}} set clicktimes=clicktimes+1 where id=".$this->id)->query();
            } catch (Exception $exc) {
            }
            //Yii::trace('a','cool.d');
        }
        
        /*
         * getFlags 返回特性的中文意义
         */
        public function getFlags(){
            $tempStr=$this->flags;
            $tempStr=str_replace('a', '头条', $tempStr);
            $tempStr=str_replace('b', '幻灯', $tempStr);
            $tempStr=str_replace('c', '推荐', $tempStr);
            return $tempStr;
        }
        
        public function beforeSave() {
		if(parent::beforeSave())
		{
			if($this->isNewRecord){
				$this->create_time=$this->lastedit_time=time();
                                $this->clicktimes=0;
                        }
                        else
                                $this->lastedit_time=time();
			return true;
		}
		else
			return false;
        }
        public function afterSave(){
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了产品 {".($this->title)."}");
            else
                CFunc::writeLog("修改了产品 {".($this->title)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了产品 {".($this->title)."}");
            return true;
        }
}