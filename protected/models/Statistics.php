<?php

/**
 * This is the model class for table "{{statistics}}".
 *
 * The followings are the available columns in table '{{statistics}}':
 * @property integer $id
 * @property string $ipaddress
 * @property string $url
 * @property string $fromurl
 * @property integer $isuv
 * @property integer $visit_time
 *
 * The followings are the available model relations:
 * @property Manager $manager
 */
class Statistics extends CBaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Managelog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{statistics}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ipaddress,url', 'required'),
			array('visit_time', 'numerical', 'integerOnly'=>true),
			array('ipaddress,url,fromurl', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('ipaddress, isuv, visit_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ipaddress' => 'IP地址',
			'url' => '访问页面',
			'fromurl' => '来源页面',
			'isuv' => '独立用户',
			'visit_time' => '访问时间',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
		$criteria->compare('id',$this->id);
		$criteria->compare('ipaddress',$this->manager_id);
		$criteria->compare('isuv',$this->content,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                         'pagination'=>array(
                             'pageSize'=>20,
                         ),
                        'sort'=>array(
                            'defaultOrder'=>'visit_time DESC', //设置默认排序是visit_time倒序
                        ),
		));
	}
}