<?php

/**
 * This is the model class for table "{{template}}".
 *
 * The followings are the available columns in table '{{template}}':
 * @property integer $id
 * @property string $title
 * @property string $categoryfile
 * @property string $contentfile
 * @property integer $perpage
 *
 */
class Template extends CBaseModel
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Content the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{template}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('perpage', 'numerical', 'integerOnly'=>true),
			array('title, categoryfile, contentfile', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, categoryfile, contentfile', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => '模板名称',
			'categoryfile' => '列表模板',
			'contentfile' => '详情模板',
			'perpage' => '每页容量',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('categoryfile',$this->categoryfile,true);
		$criteria->compare('contentfile',$this->contentfile,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>array(
                            'pagesize'=>Yii::app()->user->getState('numPerPage'),
                            'currentPage'=>Yii::app()->user->getState('pageNum')-1,
                        ),
                        'sort'=>array(
                            'defaultOrder'=>'id DESC', //设置默认排序是id倒序
                        ),
		));
	}
        public function afterSave(){
            parent::afterSave();
            if($this->isNewRecord)
                CFunc::writeLog("添加了资讯模板 {".($this->title)."}");
            else
                CFunc::writeLog("修改了资讯模板 {".($this->title)."}");
            return true;
        }
        public function afterDelete() {
            parent::afterDelete();
            CFunc::writeLog("删除了资讯模板 {".($this->title)."}");
            return true;
        }
}