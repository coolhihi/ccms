<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-CN" lang="zh-CN">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh-CN" />
	<script type="text/javascript" src="/inc/syntaxhighlighter/scripts/shCore.js"></script>
	<script type="text/javascript" src="/inc/syntaxhighlighter/scripts/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="/inc/syntaxhighlighter/styles/shCoreDefault.css"/>
	<script type="text/javascript">
            SyntaxHighlighter.defaults['auto-links'] = false;
            SyntaxHighlighter.defaults['toolbar'] = false;
            SyntaxHighlighter.defaults['gutter'] = true;
            SyntaxHighlighter.defaults['class-name'] = 'highlight_ex';
            SyntaxHighlighter.all();
        </script>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<!--[if lt IE 10]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main_ie.css" media="screen, projection" />
	<![endif]-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    <div id="header">
                    //共用header开始-demo
    <?php echo CFunc::getCustomparam('custom_website_name'); ?>
    <br />
                    <?php
                    /*
                     * 导航菜单设置，读出“导航显示”值为1的栏目并按权重排序
                     */
                        $menuInit=array(
                            'items'=>array(
                                    array('label'=>'首页', 'url'=>array('/site/index')),
                            ),
                        );
                        $categoryShow=Category::model()->findAll(array('condition'=>'joinnav=1','order'=>'weight desc,id asc'));
                        foreach ($categoryShow as $model){
                            $menuInit['items'][]=array('label'=>$model->categoryname, 'url'=>array('category/view','id'=>$model->id));
                        }

                        $this->widget('zii.widgets.CMenu',$menuInit);
                    ?>
                    <br />
                    <br />
                    //共用header结束-demo
                    <br />
                    <br />
    </div>
                    <?php echo $content; ?>
    <div id="footer">
                    <br />
                    <br />
                    //共用footer开始-demo
                    <br />
                    <br />
                    Copyright &copy; <?php echo date('Y'); ?> <?php echo CHtml::link(CFunc::getCustomparam('custom_website_copyright'),'#'); ?>. 
                    All Rights Reserved. <?php echo CFunc::getCustomparam('custom_website_recordnumber'); ?><br/>
                    Designed by <?php echo CHtml::link(CFunc::getCustomparam('system_company_name'), CFunc::getCustomparam('system_company_website'),array('target'=>'_blank')).'. '; ?>
                    <br />
                    <?php echo CHtml::link('Manage', '/admin.php'); ?>
                    <br />
                    <br />
                    //页面结束-demo
    </div>
    <?php
    $param_id=(isset($_GET['id']))?$_GET['id']:'0';
    $httpreferer=(isset($_SERVER['HTTP_REFERER']))?CFunc::srcUrlEncode($_SERVER['HTTP_REFERER']):'';
    $paramArray=array('surl'=>CFunc::srcUrlEncode($_SERVER['REQUEST_URI']),'sfrom'=>$httpreferer,'controllerid'=>Yii::app()->getController()->id,'actionid'=>Yii::app()->getController()->getAction()->id,'id'=>$param_id);
    ?>
    <script type="text/javascript" src="<?php echo $this->createUrl('stat/index',$paramArray); ?>"></script>
</body>
</html>