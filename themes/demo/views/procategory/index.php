 <div class="map">
 	<p>
<?php
    echo CHtml::link('首页',array('site/index')).' &gt; 产品中心';
?>
</p>
</div>
<div class="search01">
    <form action="<?php echo $this->createUrl('site/search');?>" method="post">
        <input name="Product[keyword]" type="text" class="text01"  />
        <input name="submit" type="submit" value="搜索" class="submit01" />
    </form>
</div>
<div class="menu">
	<ul>
		<?php
			
			$proCategoryList=Procategory::model()->findAll(array('condition'=>'parent_id=0','order'=>'weight desc,id asc'));
			foreach ($proCategoryList as $proCategoryItem)
			{
				echo '<li>'.CHtml::link($proCategoryItem->categoryname,array('site/procategory','id'=>$proCategoryItem->id)).'</li>';
			}
		?>
	</ul>
</div>
<br />
<div class="con">
<?php
/*
 * 展示所有产品
 * <div class="product_item">
 *     <div class="product_item_image"><a><img/></a></div>
 *     <div class="product_item_text"><a>产品名称</a></div>
 * </div>
 */

$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view_product',
    'pager'=>array(
	)
));
?>