<div class="map">
 	<p>
<?php
    echo CHtml::link('产品中心',array('site/products'));
    $tempP = ' &gt; '.$model->categoryname;
    $parentC=$model;
    while($parentC->parent_id!='0'){
        $parentC=Procategory::model()->findByPk($parentC->parent_id);
        $tempP = ' &gt; '.CHtml::link($parentC->categoryname,array('site/procategory','id'=>$parentC->id)).$tempP;
    }
    echo $tempP;
?>
</p>
</div>

<div class="search01">
    <form action="<?php echo $this->createUrl('site/search');?>" method="post">
        <input name="Product[keyword]" type="text" class="text01"  />
        <input name="submit" type="submit" value="搜索" class="submit01" />
    </form>
</div>

<div class="menu">
<ul>
    <?php
        /*
         * 以<li><a>分类名称</a></li>的形式展示产品分类
         * 栏目展示图片可在后台管理
         */
        $proCategoryList=Procategory::model()->findAll(array('condition'=>'parent_id='.$model->id,'order'=>'weight desc,id asc'));
        if(count($proCategoryList)==0)
            $proCategoryList=Procategory::model()->findAll(array('condition'=>'parent_id='.$model->parent_id,'order'=>'weight desc,id asc'));
        
        foreach ($proCategoryList as $proCategoryItem){
            echo '<li>'.CHtml::link($proCategoryItem->categoryname,array('site/procategory','id'=>$proCategoryItem->id)).'</li>';
        }
    ?>
</ul>
</div>

<div class="con">

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider'=>$dataProvider,
    'itemView'=>'_view_product',
    'pager'=>array(
	)
));
?>
</div>