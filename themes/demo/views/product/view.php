<div class="map">
 	<p>
<?php
    echo CHtml::link('产品中心',array('site/products'));
    $tempP = ' &gt; '.$model->procategory->categoryname;
    $parentC=$model->procategory;
    while($parentC->parent_id!='0'){
        $parentC=Procategory::model()->findByPk($parentC->parent_id);
        $tempP = ' &gt; '.CHtml::link($parentC->categoryname,array('site/procategory','id'=>$parentC->id)).$tempP;
    }
    echo $tempP;
?>
</p>
</div>

<div class="search01">
    <form action="<?php echo $this->createUrl('site/search');?>" method="post">
        <input name="Product[keyword]" type="text" class="text01"  />
        <input name="submit" type="submit" value="搜索" class="submit01" />
    </form>
</div>

<div class="menu">
<ul>
    <?php
        /*
         * 以<li><a>分类名称</a></li>的形式展示产品分类
         * 栏目展示图片可在后台管理
         */
        $proCategoryList=Procategory::model()->findAll(array('condition'=>'parent_id='.$model->procategory->parent_id,'order'=>'weight desc,id asc'));
        
        foreach ($proCategoryList as $proCategoryItem){
            echo '<li>'.CHtml::link($proCategoryItem->categoryname,array('site/procategory','id'=>$proCategoryItem->id)).'</li>';
        }
    ?>
</ul>
</div>
<div class="con">

<?php
    /*
     * 样式：
     * <div class="product_detail_title">产品名称</div>
     * <div class="product_detail_content">产品内容</div>
     */
    echo '<div class="product_detail_image">'.CHtml::image($model->image,$model->title).'</div>';
    echo '<div class="product_detail_title">'.$model->title.'</div>';
    echo '<div class="product_detail_content">'.$model->content.'</div>';
    echo '<span id="CkTimes">'.$model->clicktimes.'</span>';
?>
</div>
<div class="tit01"><p>参数一</p></div>
<?php    echo '<div class="product_detail_param">'.$model->param1.'</div>';?>
<div class="tit01"><p>参数二</p></div>
<?php    echo '<div class="product_detail_param">'.$model->param2.'</div>';?>
<div class="tit01"><p>相关产品</p></div>
<div class="con">
<?php
    /*
     * 同系列的其他产品列表
     * 
     * 样式同其他列表：
     * <div class="product_item">
     *     <div class="product_item_img">
     *          <a><img/></a>
     *     </div>
     *     <div class="product_item_text">
     *          <a>产品名称</a>
     *     </div>
     * </div>
     */

    $relationProductList = Product::model()->findAll(array('condition'=>'procategory_id='.$model->procategory_id.' and id<>'.$model->id.' and enable=1','order'=>'weight desc,id desc'));
    foreach ($relationProductList as $relationProductItem){
        echo '<div class="product_item">';
        echo '<div class="product_item_img">';
        echo CHtml::link(CHtml::image($relationProductItem->image,$relationProductItem->title),array('site/product','id'=>$relationProductItem->id));
        echo '</div>';
        echo '<div class="product_item_text">';
        echo CHtml::link($relationProductItem->title,array('site/product','id'=>$relationProductItem->id));
        echo '</div>';
        echo '</div>';
    }
?>
</div>